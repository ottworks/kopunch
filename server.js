var path = require('path')
var bodyParser = require('body-parser')
var express = require('express')
var webpack = require('webpack')
var https = require('follow-redirects').https
var http = require('follow-redirects').http
var fs = require('fs')
const LRU = require('lru-cache')
var pump = require('pump')

try {
  var key = fs.readFileSync('/etc/letsencrypt/live/ott.works/privkey.pem')
  var cert = fs.readFileSync('/etc/letsencrypt/live/ott.works/fullchain.pem')
} catch (e) {
  var nossl = true
}

if (nossl) {
  var config = require('./webpack.config.dev.js')
} else {
  var config = require('./webpack.config.js')
}

var app = express()
app.set('trust proxy', '127.0.0.1')
//app.enable('trust proxy')

app.use('/api/*', (client_req, client_res, next) => {
  let path = client_req.originalUrl.substring('/api'.length)
  console.log("api req", client_req.method, path, client_req.ip, client_req.ips)
  let cookies = client_req.headers.cookie?.split(';').map(v => v.split('=')).reduce((a, v) => {
    a[decodeURIComponent(v[0].trim())] = decodeURIComponent(v[1].trim())
    return a
  }, {}) ?? {}

  let sep = path.includes('?') ? '&' : '?'
  path = path + sep + "key=" + process.env.KNOCKOUT_API_KEY

  let options = {
    hostname: 'api.knockout.chat',
    path,
    method: client_req.method,
    headers: client_req.headers,
  }
  options.headers.host = 'api.knockout.chat'
  options.headers['x-forwarded-for'] = client_req.ip
  if (cookies.knockoutJwt) {
    options.headers.authorization = cookies.knockoutJwt
  }
  let proxy = https.request(options, (res) => {
    client_res.writeHead(res.statusCode, res.headers)
    pump(res, client_res, (err) => {
      if (err) {
        console.log("api pipe res client_res", err)
      }
    })
  })
  pump(client_req, proxy, (err) => {
    if (err) {
      console.log("api pipe client_req proxy", err)
    }
  })
})

var compiler = webpack(config)

// @Warning: body-parser will read a stream and prevent you from piping it if it has a body!
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

app.use('/static', express.static('static'))
app.use('/favicon.ico', express.static('static/favicon.ico'))

app.get('/thread*', (req, res)=>{
  let url = '/showthread.php'
  let thread = parseInt(req.url.substring("/thread/".length))
  if (!isNaN(thread)) {
    url += "?t=" + thread
    let page = parseInt(req.url.substring(("/thread/" + thread + "/").length))
    if (!isNaN(page)) {
      url += "&page=" + page
    }
  }
  res.redirect(url)
})

app.use('/auth', (client_req, client_res, next) => {
  let path = client_req.originalUrl.substring('/auth'.length)
  let data = JSON.stringify({ authorization: client_req.query.authorization })
  let options = {
    hostname: 'api.knockout.chat',
    path: '/auth/request-token?key=' + process.env.KNOCKOUT_API_KEY,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data),
    }
  }
  let proxy = https.request(options, (res) => {
    res.setEncoding('utf8')
    let chunks = []
    res.on('data', function(chunk) {
      chunks.push(chunk)
    })
    res.on('end', () => {
      let joined = chunks.join('')
      try {
        let result = JSON.parse(joined)
        client_res.cookie('knockoutJwt', result.token, {
          maxAge: 1000 * 60 * 60 * 24 * 7,
          httpOnly: true,
        })
        client_res.write("<!DOCTYPE html><head><script>window.opener.opener.postMessage('reload'); window.opener.close(); window.close()</script></head><body>Success</body></html>")
        client_res.end()
      } catch (e) {
        console.log("auth err", joined, e)
        client_res.end()
      }
    })
  })
  proxy.write(data)
  proxy.on('error', (a) => {
    console.log("Auth error", a)
  })
  proxy.end(() => {
    console.log("Auth connection closed")
  })
  //next()
})

const yt_cache = new LRU({
  maxSize: 500,
  sizeCalculation: (value, key) => {
    return 1
  },
  ttl: 1000 * 60 * 60 * 24,
})

let total_fetched = 0
let total_cached = 0

app.get('/youtube/*', function(client_req, client_res) {
  let id = client_req.url.substring('/youtube/'.length)
  console.log("Yotube request", id, client_req.headers.referer)

  if (yt_cache.has(id)) {
    let obj = yt_cache.get(id)
    client_res.writeHead(200, obj.headers)
    client_res.write(obj.body)
    client_res.end()
    ++total_cached
    console.log("cached", id, (total_fetched / total_cached) * 100 + "%")
    return
  }

  let corsURL = require('url').parse("https://www.googleapis.com/youtube/v3/videos?id=" + id + "&key=" + process.env.YOUTUBE_API_KEY + "&part=snippet,contentDetails&fields=items(id,snippet(title),contentDetails(duration))")
  //console.log('serve: ' + corsURL.href);

  var options = {
    hostname: corsURL.hostname,
    path: corsURL.path,
    method: client_req.method,
    headers: client_req.headers,
  };

  options.headers.host = corsURL.host

  //console.log("origin:", client_req.headers.origin, "x-requested-with:", client_req.headers["x-requested-with"])
  //console.log("request headers:", client_req.headers)

  var proxy = https.request(options, function (res) {
    console.log("Youtube response", id, res.statusCode)
    //console.log("response code:", res.statusCode, "response headers:", res.headers)
    //client_res.writeHead(res.statusCode, res.headers)
    let chunks = []
    res.on('data', (chunk)=>{
      chunks.push(chunk)
    })
    res.on('end', () => {
      let body = Buffer.concat(chunks)
      if (res.statusCode === 200) {
        let obj = {
          headers: res.headers,
          body: body,
        }
        yt_cache.set(id, obj)
        ++total_fetched
        console.log("cach set", id, (total_fetched / total_cached) * 100 + "%")
      }
    })
    client_res.writeHead(res.statusCode, res.headers)
    pump(res, client_res, (err)=>{
      if (err) {
        console.log("youtube response", err)
      }
    })
  })
  proxy.on('error', (a)=>{
    console.log("YouTube error", id, a)
  })
  proxy.end(()=>{
    console.log("YouTube connection closed", id)
  })

  //console.log(proxy)

  pump(client_req, proxy, (err)=>{
    if (err) {
      console.log("Youtube sent", err)
    }
  })
})

/*
app.get('/title/*', function(client_req, client_res) {
  let url = client_req.url.substring('/title/'.length)

  if (!url.startsWith("https://") && !url.startsWith("http://")) {
    url = url.replace("/", "//")
  }

  let corsURL = require('url').parse(url)
  //console.log('serve: ' + corsURL.href);

  var options = {
    hostname: corsURL.hostname,
    path: corsURL.path,
    method: 'GET',
    headers: {
      'Accept': 'text/html'
    }
  };

  options.headers.host = corsURL.host

  //console.log("origin:", client_req.headers.origin, "x-requested-with:", client_req.headers["x-requested-with"])
  //console.log("request headers:", client_req.headers)

  var proxy = https.request(options, function (res) {
    //console.log("response code:", res.statusCode, "response headers:", res.headers)
    let body = ''
    res.setEncoding('utf8')
    res.on('data', function(chunk) {
      body += chunk
    })
    res.on('end', function() {
      //console.log("res", res)
      let titlestart = body.indexOf("<title")
      let title = ''
      if (titlestart !== -1) {
        titlestart = body.indexOf(">", titlestart) + 1
        if (titlestart !== 0) {
          let titleend = body.indexOf("</title>", titlestart)
          if (titleend !== -1) {
            title = body.substring(titlestart, titleend)
          }
        }
      }

      client_res.send(title.trim())
    })
  }).on('error', ()=>{
    console.log("Error parsing title", client_req.url)
    client_res.end()
  })

  //console.log(proxy)

  pump(client_req, proxy, (err)=>{
    console.log("Title req", err)
  })
})
*/

app.get('/hotlink/*', function(client_req, client_res) {
  let url = client_req.url.substring('/hotlink/'.length)
  if (url.startsWith("https:/knockout.chat") || url.startsWith("https:/cdn.knockout.chat")) {
    url = "https://" + url.substring("https:/".length)
  }
  let parsed = require('url').parse(url)

  if (parsed.hostname !== "knockout.chat" && parsed.hostname !== "cdn.knockout.chat") {
    //console.log("Rejected hotlink:", url)
    return
  }

  var options = {
    hostname: parsed.hostname,
    path: parsed.path,
    method: client_req.method,
    headers: client_req.headers,
  }

  options.headers.host = parsed.host
  options.headers.referer = url

  //console.log("origin:", client_req.headers.origin, "x-requested-with:", client_req.headers["x-requested-with"])
  //console.log("request headers:", client_req.headers)

  var proxy = https.request(options, function(res) {
    //console.log("response code:", res.statusCode, "response headers:", res.headers)
    console.log("Hotlink response", url)
    client_res.writeHead(res.statusCode, res.headers)
    pump(res, client_res, (err)=>{
      if (err) {
        console.log("Hotlink res", err)
      }
    })
  })
  proxy.on('error', (e)=>{
    console.log("Error hotlinking", url)
  })
  proxy.end(()=>{
    console.log("Hotlink connection closed")
  })

  pump(client_req, proxy, (err)=>{
    if (err) {
      console.log("Hotlink req", err)
    }
  })
})

app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, 'index.html'))
})

if (nossl) {
  var httpServer = http.createServer(app)

  httpServer.listen(8080)

  console.log("Server listening on port 8080")
} else {
  var httpsServer = https.createServer({
    key: key,
    cert: cert,
  }, app)

  httpsServer.listen(8444)

  console.log("Server listening on port 8444")
}


