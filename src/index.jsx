import React, { Suspense, lazy } from 'react'
import ReactDom from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import {Helmet} from 'react-helmet'
import Snow from './components/Snow'
import Forum from './components/views/forum'
import ForumDisplay from './components/views/forumdisplay'
import ShowThread from './components/views/showthread'
import Events from './components/views/fp_events'
import Popular from './components/views/fp_popular'
import Read from './components/views/fp_read'
import Ticker from './components/views/fp_ticker'
import NewReply from './components/views/newreply'
import NewThread from './components/views/newthread'
import Images from './components/views/fp_images'
import Member from './components/views/member'
import Search from './components/views/search'
import UserCP from './components/views/usercp'
import Calendar from './components/views/calendar'
import Misc from './components/views/misc'
import DesignElements from './components/views/design-elements'
import AppStateContext from './components/AppStateContext'
/*
const Snow = lazy(()=>import('./components/Snow'))
const Forum = lazy(()=>import('./components/views/forum'))
const ForumDisplay = lazy(()=>import('./components/views/forumdisplay'))
const ShowThread = lazy(()=>import('./components/views/showthread'))
const Events = lazy(()=>import('./components/views/fp_events'))
const Popular = lazy(()=>import('./components/views/fp_popular'))
const Read = lazy(()=>import('./components/views/fp_read'))
const NewReply = lazy(()=>import('./components/views/newreply'))
const NewThread = lazy(()=>import('./components/views/newthread'))
const Images = lazy(()=>import('./components/views/fp_images'))
const Member = lazy(()=>import('./components/views/member'))
const Search = lazy(()=>import('./components/views/search'))
const UserCP = lazy(()=>import('./components/views/usercp'))
*/

require('./stylesheets/base.css')
const queryString = require('query-string')

export const userOptions = {
  "layout": {
    name: "Post Layout",
    type: "radio",
    options: [
      ["newpunch", "Newpunch"],
      ["oldpunch", "Oldpunch"],
      ["bluepunch", "Small Avatars"],
      ["goldpunch", "Big Avatars"],
    ],
    default: "oldpunch",
  },
  "width": {
    name: "Forum Width",
    type: "dropdown",
    options: [
      ["960px", "960px"],
      ["1280px", "1280px"],
      ["1920px", "1920px"],
      ["2560px", "2560px"],
    ],
    default: "1920px",
    onChange: (oldval, newval)=>{
      document.body.style.maxWidth = newval
    },
  },
  "theme": {
    name: "Theme",
    type: "dropdown",
    options: [
      ["default", "Default"],
      ["light", "Light"],
      ["dark", "Dark"],
    ],
    default: "default",
    onChange: (oldval, newval)=>{
      document.body.classList.remove("theme-" + oldval)
      document.body.classList.add("theme-" + newval)
    }
  },
  "lineheight": {
    name: "Line height",
    type: "dropdown",
    options: [
      [1, "Condensed"],
      [1.3, "Medium"],
      [1.6, "Spaced"],
      [2, "Double Spaced"],
    ],
    default: 1,
  },
  "qa": {
    name: "Use QA site",
    type: "toggle",
    options: ["no", "yes"],
    default: false,
    hidden: true,
  },
  "flagdog": {
    name: "Flagdog",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: true,
  },
  "singlecolumn": {
    name: "Single Column Layout",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
  "supersecret": {
    name: "Secret Feature",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: true,
  },
  "nsfw": {
    name: "Display NSFW Threads",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
  "autosub": {
    name: "Auto-Subscribe",
    type: "dropdown",
    options: [
      ["never", "Never"],
      ["reply", "On reply"],
      ["read", "On read"],
    ],
    default: "reply",
  },
  "convertlinks": {
    name: "Convert Knockout Links",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
  "subload": {
    name: "Load All Unread Threads",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: true,
  },
  "datasaver": {
    name: "Click to load embeds",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
  "noavatar": {
    name: "Hide avatars and backgrounds",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
  "nosocial": {
    name: "Hide social icons",
    type: "toggle",
    options: ["Disabled", "Enabled"],
    default: false,
  },
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      authenticated: undefined,
      api: "",
      userid: 0,
      userbanned: false,
      username: "",
      banmessage: "",
      notifications: [],
      ignorelist: {},
      motdlist: [],
      taglist: [],
      readthreads: [],
      mouse: {x: 0, y: 0},
      dark: false,
    }
    this.storageListener = this.storageListener.bind(this)
    console.log("new app constructed")
  }
  componentWillMount() {
    let qa = localStorage.getItem("kopunch_qa") == "true" ? true : false
    console.log("qa: ", qa)
    let api = qa ? "https://archive.kopunch.club" : "/api"
    this.setState({api: api})
    window.fetch(api + "/user/syncData", {credentials: 'include'}).then(res => res.status != 401 && res.json()).then((r)=>{
      console.log("zzzzz", r)
      if (r && !(r.error || r.errors)) {
        console.log("authenticated!")
        this.setState({authenticated: true, user: r, userid: r.id, username: r.username, userbanned: r.isBanned})
        if (r.isBanned) {
          this.setState({banmessage: r.banInfo.banMessage, banthread: r.banInfo.threadId})
        }
      } else {
        window.fetch(api + "/user", {credentials: 'include'}).then(res => res.status != 401 && res.json()).then((r)=>{
          if (r && !(r.error || r.errors)) {
            console.log("authenticated (on second attempt)!")
            this.setState({authenticated: true, user: r.user, userid: r.user.id, username: r.user.username, userbanned: r.user.isBanned})
            if (r.user.isBanned) {
              this.setState({banmessage: r.user.banMessage, banthread: r.user.threadId})
            }
          } else {
            this.setState({authenticated: false})
          }
        })
      }
      this.setState({api: api})
    })

    window.fetch(api + "/tag/list", {credentials: 'include'}).then(r=>r.json()).then(r=>{
      let taglist = []
      r.map(obj=>{
        taglist[obj.id] = obj.name
      })
      this.setState({taglist: taglist})
    })

    this.setState({ignorelist: JSON.parse(localStorage.getItem("kopunch_ignorelist")) || {}})
    this.setState({motdlist: JSON.parse(localStorage.getItem("kopunch_motdlist")) || []})
    this.setState({readthreads: JSON.parse(localStorage.getItem("kopunch_readthreads")) || []})
  }
  componentDidMount() {
    for (const [option, data] of Object.entries(userOptions)) {
      let value = localStorage.getItem("kopunch_" + option) || data.default
      if (data.type == "toggle")
        value = value === true || value === "true"
      this.setState({[option]: value})
      if (data.onChange) {
        data.onChange(undefined, value)
      }
    }
    window.addEventListener("storage", this.storageListener)
  }
  storageListener(e) {
    console.log("Storage changed", e.key, e.newValue)
    if (e.key.startsWith("kopunch_")) {
      let option = e.key.substring("kopunch_".length)
      let data = userOptions[option]
      if (data) {
        let value = e.newValue
        if (data.type == "toggle") {
          value = value === true || value === "true"
        }
        this.setState({[option]: value})
      }
    }
    if (e.key == "kopunch_ignorelist") {
      this.setState({ignorelist: JSON.parse(e.newValue)})
    }
    if (e.key == "kopunch_motdlist") {
      this.setState({motdlist: JSON.parse(e.newValue)})
    }
    if (e.key == "kopunch_readthreads") {
      this.setState({readthreads: JSON.parse(e.newValue)})
    }
  }
  componentWillUnmount() {
    window.removeEventListener("storage", this.storageListener)
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    for (const [option, data] of Object.entries(userOptions)) {
      if (this.state[option] != prevState[option]) {
        if (data.onChange) {
          data.onChange(prevState[option], this.state[option])
        }
        localStorage.setItem("kopunch_" + option, this.state[option])
      }
    }

    if (this.state.ignorelist != prevState.ignorelist) {
      localStorage.setItem("kopunch_ignorelist", JSON.stringify(this.state.ignorelist))
    }
    if (this.state.motdlist != prevState.motdlist) {
      localStorage.setItem("kopunch_motdlist", JSON.stringify(this.state.motdlist))
    }
    if (this.state.readthreads != prevState.readthreads) {
      localStorage.setItem("kopunch_readthreads", JSON.stringify(this.state.readthreads))
    }
    let dark = window.getComputedStyle(document.body).content === '"dark"'
    if (dark != this.state.dark) {
      this.setState({dark: dark})
    }
  }
  render() {
    //const View = this.props.view
    console.log("app render")
    if (this.state.api) {
      let dark = window.getComputedStyle(document.body).content === '"dark"'
      return <AppStateContext.Provider value={[this.state, this.setState]}>
        <BrowserRouter>
          <Helmet>
            <meta name="twitter:dnt" content="on"/>
            <meta name="color-scheme" content="light dark"/>
            <meta name="twitter:widgets:theme" content={dark ? "dark" : "light"}/>
          </Helmet>
          <Suspense fallback={<div>Loading...</div>}>
            <div 
              className="app-container" 
              onMouseMove={this.state.supersecret ? (e)=>{
                this.state.mouse.x = e.clientX
                this.state.mouse.y = e.clientY
              } : undefined}
              style={{
                lineHeight: this.state.lineheight,
              }}
            >
              <Switch>
                <Route path='/' exact render={(p)=><Forum {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/forum.php' render={(p)=><Forum {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/forumdisplay.php' render={(p)=><ForumDisplay {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/showthread.php' render={(p)=><ShowThread {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/fp_events.php' render={(p)=><Events {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/fp_popular.php' render={(p)=><Popular {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/fp_read.php' render={(p)=><Read {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/fp_ticker.php' render={(p)=><Ticker {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/newreply.php' render={(p)=><NewReply {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/newthread.php' render={(p)=><NewThread {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/fp_images.php' render={(p)=><Images {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/member.php' render={(p)=><Member {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/search.php' render={(p)=><Search {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/usercp.php' render={(p)=><UserCP {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/misc.php' render={(p)=><Misc {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
                <Route path='/design.php' render={(p) => <DesignElements {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s) => this.setState(s)} />} />
                <Route path='/calendar.php' render={(p)=><Calendar {...p} key={p.location.key} query={queryString.parse(p.location.search)} appState={this.state} appSetState={(s)=>this.setState(s)}/>}/>
              </Switch>
              {this.state.supersecret && <Snow mouse={this.state.mouse} dark={this.state.dark}/>}
            </div>
          </Suspense>
        </BrowserRouter>
      </AppStateContext.Provider>
    }
    return null
    //return <div>{this.props.children}</div>
  }
}

ReactDom.render(
  <App/>,
  document.querySelector('#app')
);
