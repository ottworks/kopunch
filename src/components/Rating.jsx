import React from "react"
import LinkWrapper from "./LinkWrapper";

export const ratings = {
  agree: {
    name: "Agree",
    url: "/static/silkicons/tick.png",
    subforum: ~0,
  },
  disagree: {
    name: "Disagree",
    url: "/static/silkicons/cross.png",
    subforum: ~0,
  },
  funny: {
    name: "Funny",
    url: "/static/funny.png",
    subforum: ~0,
  },
  winner: {
    name: "Winner",
    url: "/static/silkicons/award_star_gold_1.png",
    subforum: ~0,
  },
  zing: {
    name: 'Zing',
    url: '/static/silkicons/lightning.png',
    subforum: ~0,
  },
  informative: {
    name: "Informative",
    url: "/static/silkicons/information.png",
    subforum: ~0,
  },
  confusing: {
    name: "Confusing",
    url: "/static/silkicons/help.png",
    subforum: ~0,
  },
  friendly: {
    name: "Friendly",
    url: "/static/silkicons/heart.png",
    subforum: ~0,
  },
  rude: {
    name: "Rude",
    url: "/static/silkicons/exclamation.png",
    subforum: ~0,
  },
  kawaii: {
    name: "Cute",
    url: "/static/silkicons/bell.png",
    subforum: ~0,
  },
  optimistic: {
    name: 'Optimistic',
    url: '/static/silkicons/rainbow.png',
    subforum: ~0,
  },
  sad: {
    name: "Sad",
    url: "/static/silkicons/emoticon_unhappy.png",
    subforum: ~0,
  },
  artistic: {
    name: "Artistic",
    url: "/static/silkicons/palette.png",
    subforum: ~0,
  },
  idea: {
    name: "Idea",
    url: "/static/silkicons/lightbulb.png",
    subforum: ~0,
  },
  glasses: {
    name: "Bad Reading",
    url: "/static/silkicons/book_error.png",
    subforum: ~0,
  },
  late: {
    name: "Late",
    url: "/static/silkicons/clock.png",
    subforum: ~0,
  },
  dumb: {
    name: "Dumb",
    url: "/static/silkicons/box.png",
    subforum: ~0,
  },
  citation: {
    name: 'Citation Needed',
    url: '/static/silkicons/tag.png',
    subforum: 0,
  },
  yeet: {
    name: 'Yeet',
    url: '/static/silkicons/arrow_turn_right.png',
    subforum: (1 << 16),
  },
  scary: {
    name: "Scary",
    url: '/static/silkicons/error.png',
    subforum: ~0,
  }
}

export default function Rating({ rating, count, showName, showCount, doRating, postId}) {
  let r = ratings[rating] ?? {
    url: "/static/silkicons/error.png",
    name: rating,
  }
  return <span key={rating} className="rating">
    <LinkWrapper href="" onClick={(e) => { e.preventDefault(); doRating(postId, rating) }}>
      <img className="icon" src={r.url} title={r.name} />
    </LinkWrapper>
    {showCount && <>
      &nbsp;{(showName ? r.name : "") + " x "}
      <b>{count}</b>
    </>}
  </span>
}