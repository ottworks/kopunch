import React, { Component } from "react";
import LinkWrapper from './LinkWrapper'

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-links">
          <LinkWrapper href={"/forumdisplay.php?f=" + this.props.subforumid}>
            {this.props.subforum || "Moderators"}
          </LinkWrapper>
          {' - '}
          <LinkWrapper href="#" onClick={(e)=>{
            e.preventDefault()
            window.scrollTo(0, 0)
          }}>Top</LinkWrapper>
        </div>
        <div className="big">
          <LinkWrapper href="/forum.php">Knockout</LinkWrapper>
        </div>
        <div className="small">
          <LinkWrapper href="https://knockout.chat/rules">The Rules</LinkWrapper> | <LinkWrapper href="" onClick={(e)=>{e.preventDefault()}}>Archive</LinkWrapper>
          <br/>
          Icons by <LinkWrapper href="http://www.famfamfam.com/">famfamfam</LinkWrapper> and <LinkWrapper href="https://icons8.com/">Icons8</LinkWrapper> | kopunch by <LinkWrapper href="showthread.php?t=929">Ott</LinkWrapper>
        </div>
      </div>
    );
  }
}