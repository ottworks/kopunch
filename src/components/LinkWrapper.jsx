import React, { Component } from "react";
import { Link } from "react-router-dom"
import he from "he"

export default class LinkWrapper extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    if (this.props.smart && !this.state.title) {
      this.setState({title: this.props.href})
      return
      fetch("/title/" + this.props.href).then(r=>r.text()).then(r=>{
        console.log("got title", r)
        this.setState({title: he.decode(r)})
      }).catch(e=>{

      })
    }
  }
  render() {
    let {external, className, smart, ...rest} = this.props
    if (external || !this.props.href || typeof this.props.href == "string" && this.props.href.includes("://")) {
      if (typeof this.props.href == "string" && this.props.href.startsWith("steam://")) {
        return <a className={"link-external " + (className || "")} {...rest}>{this.state.title || this.props.children}</a>
      }
      return <a className={"link-external " + (className || "")} target="_blank" rel="noopener ugc" {...rest}>{this.state.title || this.props.children}</a>
    } else {
      return <Link className={"link-internal " + (className || "")} {...rest} to={this.props.href}>{this.state.title || this.props.children}</Link>
    }
  }
}
