import React, { Component } from "react"
import {parse, toSeconds} from 'iso8601-duration'
import LinkWrapper from './LinkWrapper'
const queryString = require('query-string')
const jsonp = require('jsonp')
const url = require('url')

export default class YouTube extends Component {
  constructor(props) {
    super(props)
    let src = props.src
    console.log("yt constructor", src)
    this.state = {}
    if (typeof src != "string")
      return "BAD YOUTUBE EMBED"

    let vars = queryString.parse(url.parse(src).search)
    vars.autoplay = 0
    vars.rel = 0
    let id = vars.v
    delete vars.v

    if (!id) {
      try {
        id = src.match(/youtu.be\/([\w-_]+)/)[1]
      } catch (e) {
        return src
      }
    }

    let t = vars.t
    if (t) {
      if (isNaN(t)) {
        t = t.toLowerCase()
        let s = 0
        let n = 0
        for (let i = 0; i < t.length; i++) {
          if (!isNaN(t[i])) {
            n *= 10
            n += Number(t[i])
          } else {
            if (t[i] == 'h')
              s += n * 60 * 60
            else if (t[i] == 'm')
              s += n * 60
            else if (t[i] == 's')
              s += n
            n = 0
          }
        }
        if (n)
          s += n
        t = s
      }
      vars.start = t
      delete vars.t
    }

    src = "https://www.youtube.com/embed/" + id
    if (vars)
      src += '?' + Object.keys(vars).map(k => k + '=' + vars[k]).join('&')
    console.log("youtube id", id, "start", vars.start)

    this.state = {
      src: src,
      id: id,
      start: vars.start,
      active: false,
      title: props.src,
    }
  }
  componentDidMount() {
    window.fetch("/youtube/" + this.state.id, {
      mode: "cors"
    }).then(r=>r.json()).then((response)=>{
      console.log("r", response)
      if (!response.items)
        return
      let title = response.items[0].snippet.title

      let start = this.state.start || 0
      let starts = ("" + start % 60).padStart(2, '0')
      start = Math.floor(start / 60)
      if (start) {
        starts = ("" + start % 60).padStart(2, '0') + ":" + starts
        start = Math.floor(start / 60)
        if (start) {
          starts = start + ":" + starts
        }
        if (starts)
          starts = starts.replace(/^0+/, '')
      } else {
        starts = "0:" + starts
      }

      let length = toSeconds(parse(response.items[0].contentDetails.duration))
      let lengths = ("" + length % 60).padStart(2, '0')
      length = Math.floor(length / 60)
      if (length) {
        lengths = ("" + length % 60).padStart(2, '0') + ":" + lengths
        length = Math.floor(length / 60)
        if (length) {
          lengths = length + ":" + lengths
        }
        if (lengths)
          lengths = lengths.replace(/^0+/, '')
      } else {
        lengths = "0:" + lengths
      }

      let time = " (" + (this.state.start ? (starts + "/") : "") + lengths + ")"
      this.setState({title: title, time: time})
    })
  }
  render() {
    console.log("yt render")
    if (this.state.active) {
      return <div className="youtube">
        <iframe src={this.state.src} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/>
      </div>
    }
    else {
      return <div className="youtube" onClick={(e)=>{
        console.log("youtube click", e, e.target.className, e.target.className.includes("title-link"))
        if (!e.target.className.includes("title-link"))
        {
          e.preventDefault()
          this.setState({active: true})
        }
      }}>
        <div className="title">
          <LinkWrapper className="title-link" href={this.props.src}>{this.state.title}</LinkWrapper>
          <span className="time">{this.state.time}</span>
        </div>
        <object data={"https://img.youtube.com/vi/" + this.state.id + "/maxresdefault.jpg"} type="image/png">
          <img src={"https://img.youtube.com/vi/" + this.state.id + "/0.jpg"}/>
        </object>
      </div>
    }
  }
}
// await fetch("http://youtube.com/get_video_info?video_id=awJq_VChxMM").then(r=>r.text()).then(r=>decodeURIComponent(r)).then(r=>r.match(/title":"([^,]+)",/))