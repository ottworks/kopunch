import React, { Component } from "react";
import LinkWrapper from './LinkWrapper'

const icons = [{
    id: 0,
    url: 'https://img.icons8.com/color/64/default.png',
    description: 'Default Icon',
    restricted: !0
  }, {
    id: 1,
    url: 'https://img.icons8.com/color/64/drama.png',
    description: 'Drama'
  }, {
    id: 2,
    url: 'https://img.icons8.com/color/64/games.png',
    description: 'Games'
  }, {
    id: 3,
    url: 'https://img.icons8.com/color/64/life.png',
    description: 'Life Stuff'
  }, {
    id: 4,
    url: 'https://img.icons8.com/color/64/money.png',
    description: 'Chart'
  },  {
    id: 5,
    url: 'https://img.icons8.com/color/64/announcement.png',
    description: 'Announcement',
    restricted: !0
  }, {
    url: 'https://img.icons8.com/color/64/arcade-cabinet.png',
    description: 'Arcade',
    id: 6
  }, {
    url: 'https://img.icons8.com/color/64/blog.png',
    description: 'Blog',
    id: 7
  }, {
    url: 'https://img.icons8.com/color/64/bomb.png',
    description: 'Bomb',
    id: 8
  }, {
    url: 'https://img.icons8.com/color/64/bug.png',
    description: 'Bug',
    id: 9
  }, {
    url: 'https://img.icons8.com/color/64/code.png',
    description: 'Programming',
    id: 10
  }, {
    url: 'https://img.icons8.com/color/64/combo-chart.png',
    description: 'Chart',
    id: 11
  }, {
    url: 'https://img.icons8.com/color/64/cooker.png',
    description: 'Cooking',
    id: 12
  }, {
    url: 'https://img.icons8.com/color/64/crying-baby.png',
    description: 'Baby',
    id: 13
  }, {
    url: 'https://img.icons8.com/color/64/dota.png',
    description: 'Dota 2',
    id: 14
  }, {
    url: 'https://img.icons8.com/color/64/europe-news.png',
    description: 'EU News',
    id: 15
  }, {
    url: 'https://img.icons8.com/color/64/gallery.png',
    description: 'Photos',
    id: 16
  }, {
    url: 'https://img.icons8.com/color/64/grocery-bag.png',
    description: 'Grocery',
    id: 17
  }, {
    url: 'https://img.icons8.com/color/64/gun.png',
    description: 'Gun',
    id: 18
  }, {
    url: 'https://img.icons8.com/color/64/heart-with-pulse.png',
    description: 'Health',
    id: 19
  }, {
    url: 'https://img.icons8.com/color/64/help.png',
    description: 'Help',
    id: 20
  }, {
    url: 'https://img.icons8.com/color/64/hot-article.png',
    description: 'Hot Topic',
    restricted: !0,
    id: 21
  }, {
    url: 'https://img.icons8.com/color/64/news.png',
    description: 'News',
    id: 22
  }, {
    url: 'https://img.icons8.com/color/64/nintendo-entertainment-system.png',
    description: 'Nintendo Entertainment System',
    id: 23
  }, {
    url: 'https://img.icons8.com/color/64/pin.png',
    description: 'Pin',
    restricted: !0,
    id: 24
  }, {
    url: 'https://img.icons8.com/color/64/planner.png',
    description: 'Calendar',
    id: 25
  }, {
    url: 'https://img.icons8.com/color/64/tv-show.png',
    description: 'Modern TV',
    id: 26
  }, {
    url: 'https://img.icons8.com/color/64/uk-news.png',
    description: 'UK News',
    id: 27
  }, {
    url: 'https://img.icons8.com/color/64/us-music.png',
    description: 'Music',
    id: 28
  }, {
    url: 'https://img.icons8.com/color/64/us-news.png',
    description: 'US News',
    id: 29
  }, {
    description: 'Old TV',
    url: 'https://img.icons8.com/color/64/retro-tv.png',
    id: 30
  }, {
    description: 'Smile',
    url: 'https://img.icons8.com/color/64/smiling.png',
    id: 31
  }, {
    description: 'Vomit',
    url: 'https://img.icons8.com/color/64/vomited.png',
    id: 32
  }, {
    description: 'Science',
    url: 'https://img.icons8.com/color/64/test-tube.png',
    id: 33
  }, {
    description: 'Art',
    url: 'https://img.icons8.com/color/64/easel.png',
    id: 34
  }, {
    description: 'Banana',
    url: 'https://img.icons8.com/color/64/banana.png',
    id: 35
  }, {
    description: 'Love',
    url: 'https://img.icons8.com/color/64/heart-with-arrow.png',
    id: 36
  }, {
    description: 'Radioactive',
    url: 'https://img.icons8.com/color/64/radio-active.png',
    id: 37
  }, {
    description: 'Poison',
    url: 'https://img.icons8.com/color/64/poison.png',
    id: 38
  }, {
    description: 'Eye',
    url: 'https://img.icons8.com/color/64/invisible.png',
    id: 39
  }, {
    description: 'Eye',
    url: 'https://img.icons8.com/color/64/visible.png',
    id: 40
  }, {
    description: 'Gamepad',
    url: 'https://img.icons8.com/color/64/controller.png',
    id: 41
  }, {
    description: 'Boredom',
    url: 'https://img.icons8.com/color/64/boring.png',
    id: 42
  }, {
    description: 'Anger',
    url: 'https://img.icons8.com/color/64/swearing-male.png',
    id: 43
  }, {
    description: 'Idea',
    url: 'https://img.icons8.com/color/64/idea.png',
    id: 44
  }, {
    description: 'Announcement',
    url: 'https://img.icons8.com/color/64/megaphone.png',
    id: 45
  }, {
    description: 'Money',
    url: 'https://img.icons8.com/color/64/banknotes.png',
    id: 46
  }, {
    description: 'Robot',
    url: 'https://img.icons8.com/color/64/robot-3.png',
    id: 47
  }, {
    description: 'Ringing Phone',
    url: 'https://img.icons8.com/color/64/phonelink-ring.png',
    id: 48
  }, {
    description: 'Skull',
    url: 'https://img.icons8.com/color/64/thriller.png',
    id: 49
  }, {
    description: 'Hamburguer',
    url: 'https://img.icons8.com/color/64/hamburger.png',
    id: 50
  }, {
    description: 'Cherry Blossom',
    url: 'https://img.icons8.com/color/64/sakura.png',
    id: 51
  }, {
    description: 'Sushi',
    url: 'https://img.icons8.com/color/64/sushi.png',
    id: 52
  }, {
    description: 'Life Buoy',
    url: 'https://img.icons8.com/color/64/lifebuoy.png',
    id: 53
  }, {
    description: 'Handshake',
    url: 'https://img.icons8.com/color/64/handshake.png',
    id: 54
  }, {
    description: 'Fist',
    url: 'https://img.icons8.com/color/64/clenched-fist.png',
    id: 55
  }, {
    description: 'Mask',
    url: 'https://img.icons8.com/color/64/anonymous-mask.png',
    id: 56
  }, {
    description: 'Rocket',
    url: 'https://img.icons8.com/color/64/rocket.png',
    id: 57
  }, {
    description: 'Vodka',
    url: 'https://img.icons8.com/color/64/vodka.png',
    id: 58
  }, {
    description: 'Poop',
    url: 'https://img.icons8.com/color/64/poo.png',
    id: 59
  }, {
    description: 'Car',
    url: 'https://img.icons8.com/color/64/sedan.png',
    id: 60
  }, {
    description: 'Futuristic Car',
    url: 'https://img.icons8.com/color/64/tesla-model-x.png',
    id: 61
  }, {
    description: 'RAM',
    url: 'https://img.icons8.com/color/64/memory-slot.png',
    id: 62
  }, {
    description: 'CPU',
    url: 'https://img.icons8.com/color/64/processor.png',
    id: 63
  }, {
    description: 'Fire',
    url: 'https://img.icons8.com/color/64/fire-element.png',
    id: 64
  }, {
    description: 'Firework',
    url: 'https://img.icons8.com/color/64/firework.png',
    id: 65
  }, {
    description: 'Siren',
    url: 'https://img.icons8.com/color/64/siren.png',
    id: 66
  }, {
    description: 'Confetti',
    url: 'https://img.icons8.com/color/64/confetti.png',
    id: 67
  }, {
    description: 'Campfire',
    url: 'https://img.icons8.com/color/64/campfire.png',
    id: 68
  }, {
    description: 'Panties',
    url: 'https://img.icons8.com/color/64/panties.png',
    id: 69
  }, {
    description: 'Nuke',
    url: 'https://img.icons8.com/color/64/mushroom-cloud.png',
    id: 70
  }, {
    description: 'Bomb',
    url: 'https://img.icons8.com/color/64/bomb-with-timer.png',
    id: 71
  }, {
    description: 'ADHD',
    url: 'https://img.icons8.com/color/64/adhd.png',
    id: 72
  }, {
    description: 'Monster',
    url: 'https://img.icons8.com/color/64/cute-monster.png',
    id: 73
  }, {
    description: 'Japan',
    url: 'https://img.icons8.com/color/64/japan-circular.png',
    id: 74
  }, {
    description: 'Sad Sun',
    url: 'https://img.icons8.com/color/64/sad-sun.png',
    id: 75
  }, {
    description: 'Anime',
    url: 'https://img.icons8.com/color/64/anime-emoji.png',
    id: 76
  }, {
    description: 'Daruma',
    url: 'https://img.icons8.com/color/64/daruma-doll.png',
    id: 77
  }, {
    description: 'Jellyfish',
    url: 'https://img.icons8.com/color/64/jellyfish.png',
    id: 78
  }, {
    description: 'Sailor Moon',
    url: 'https://img.icons8.com/color/64/sailor-moon.png',
    id: 79
  }, {
    description: 'Argentina',
    url: 'https://img.icons8.com/color/64/argentina-circular.png',
    id: 80
  }, {
    description: 'Dice',
    url: 'https://img.icons8.com/color/64/dice.png',
    id: 81
  }, {
    description: 'Landslide',
    url: 'https://img.icons8.com/color/64/landslide.png',
    id: 82
  }, {
    description: 'Security Guard',
    url: 'https://img.icons8.com/color/64/security-guard.png',
    id: 83
  }, {
    description: 'Australia',
    url: 'https://img.icons8.com/color/64/australia-circular.png',
    id: 84
  }, {
    description: 'Dog Pee',
    url: 'https://img.icons8.com/color/64/dog-pee.png',
    id: 85
  }, {
    description: 'Language',
    url: 'https://img.icons8.com/color/64/language-skill.png',
    id: 86
  }, {
    description: 'Skull Heart',
    url: 'https://img.icons8.com/color/64/skull-heart--v2.png',
    id: 87
  }, {
    description: 'Bad Piggies',
    url: 'https://img.icons8.com/color/64/bad-piggies.png',
    id: 88
  }, {
    description: 'Dog Pooping',
    url: 'https://img.icons8.com/color/64/dog-pooping.png',
    id: 89
  }, {
    description: 'Lasagna',
    url: 'https://img.icons8.com/color/64/lasagna.png',
    id: 90
  }, {
    description: 'Soil',
    url: 'https://img.icons8.com/color/64/soil.png',
    id: 91
  }, {
    description: 'Batman',
    url: 'https://img.icons8.com/color/64/batman-emoji.png',
    id: 92
  }, {
    description: 'Exchange',
    url: 'https://img.icons8.com/color/64/euro-exchange.png',
    id: 93
  }, {
    description: 'Lawn',
    url: 'https://img.icons8.com/color/64/lawn-care.png',
    id: 94
  }, {
    description: 'South Korea',
    url: 'https://img.icons8.com/color/64/south-korea-circular.png',
    id: 95
  }, {
    description: 'Belgium',
    url: 'https://img.icons8.com/color/64/belgium-circular.png',
    id: 96
  }, {
    description: 'Finland',
    url: 'https://img.icons8.com/color/64/finland-circular.png',
    id: 97
  }, {
    description: 'Lobster',
    url: 'https://img.icons8.com/color/64/lobster.png',
    id: 98
  }, {
    description: 'Spain',
    url: 'https://img.icons8.com/color/64/spain2-circular.png',
    id: 99
  }, {
    description: 'Boiling',
    url: 'https://img.icons8.com/color/64/boiling.png',
    id: 100
  }, {
    description: 'Fortnite',
    url: 'https://img.icons8.com/color/64/fortnite.png',
    id: 101
  }, {
    description: 'Weed',
    url: 'https://img.icons8.com/color/64/marijuana-leaf.png',
    id: 102
  }, {
    description: 'Statue Of Christ The Redeemer',
    url: 'https://img.icons8.com/color/64/statue-of-christ-the-redeemer.png',
    id: 103
  }, {
    description: 'Collie',
    url: 'https://img.icons8.com/color/64/border-collie.png',
    id: 104
  }, {
    description: 'Fry',
    url: 'https://img.icons8.com/color/64/fry.png',
    id: 105
  }, {
    description: 'MDMA',
    url: 'https://img.icons8.com/color/64/mdma.png',
    id: 106
  }, {
    description: 'Roots',
    url: 'https://img.icons8.com/color/64/stump-with-roots.png',
    id: 107
  }, {
    description: 'Brazil',
    url: 'https://img.icons8.com/color/64/brazil-circular.png',
    id: 108
  }, {
    description: 'Full Of Shit',
    url: 'https://img.icons8.com/color/64/full-of-shit.png',
    id: 109
  }, {
    description: 'Meowth',
    url: 'https://img.icons8.com/color/64/meowth.png',
    id: 110
  }, {
    description: 'Sweden',
    url: 'https://img.icons8.com/color/64/sweden-circular.png',
    id: 111
  }, {
    description: 'Brigadeiro',
    url: 'https://img.icons8.com/color/64/brigadeiro.png',
    id: 112
  }, {
    description: 'Gasoline',
    url: 'https://img.icons8.com/color/64/gasoline-refill.png',
    id: 113
  }, {
    description: 'Mexico',
    url: 'https://img.icons8.com/color/64/mexico-circular.png',
    id: 114
  }, {
    description: 'Switzerland',
    url: 'https://img.icons8.com/color/64/switzerland-circular.png',
    id: 115
  }, {
    description: 'Buddha',
    url: 'https://img.icons8.com/color/64/buddha.png',
    id: 116
  }, {
    description: 'Germany',
    url: 'https://img.icons8.com/color/64/germany-circular.png',
    id: 117
  }, {
    description: 'Band',
    url: 'https://img.icons8.com/color/64/music-band.png',
    id: 118
  }, {
    description: 'Tapir',
    url: 'https://img.icons8.com/color/64/tapir.png',
    id: 119
  }, {
    description: 'Call Me',
    url: 'https://img.icons8.com/color/64/call-me.png',
    id: 120
  }, {
    description: 'Park',
    url: 'https://img.icons8.com/color/64/national-park.png',
    id: 121
  }, {
    description: 'The Sims',
    url: 'https://img.icons8.com/color/64/the-sims.png',
    id: 122
  }, {
    description: 'Canada',
    url: 'https://img.icons8.com/color/64/canada-circular.png',
    id: 123
  }, {
    description: 'Great Britain',
    url: 'https://img.icons8.com/color/64/great-britain-circular.png',
    id: 124
  }, {
    description: 'Nightmare',
    url: 'https://img.icons8.com/color/64/nightmare.png',
    id: 125
  }, {
    description: 'Trust',
    url: 'https://img.icons8.com/color/64/trust.png',
    id: 126
  }, {
    description: 'Car Fire',
    url: 'https://img.icons8.com/color/64/car-fire.png',
    id: 127
  }, {
    description: 'Animals',
    url: 'https://img.icons8.com/color/64/group-of-animals.png',
    id: 128
  }, {
    description: 'Gamecube',
    url: 'https://img.icons8.com/color/64/nintendo-gamecube-controller.png',
    id: 129
  }, {
    description: 'Computer',
    url: 'https://img.icons8.com/color/64/under-computer.png',
    id: 130
  }, {
    description: 'Caveman',
    url: 'https://img.icons8.com/color/64/caveman.png',
    id: 131
  }, {
    description: 'Headache',
    url: 'https://img.icons8.com/color/64/headache.png',
    id: 132
  }, {
    description: 'No Drugs',
    url: 'https://img.icons8.com/color/64/no-drugs.png',
    id: 133
  }, {
    description: 'Undertale',
    url: 'https://img.icons8.com/color/64/undertale.png',
    id: 134
  }, {
    description: 'Chad',
    url: 'https://img.icons8.com/color/64/chad-circular.png',
    id: 135
  }, {
    description: 'Hills',
    url: 'https://img.icons8.com/color/64/hills.png',
    id: 136
  }, {
    description: 'Norway',
    url: 'https://img.icons8.com/color/64/norway-circular.png',
    id: 137
  }, {
    description: 'Usa',
    url: 'https://img.icons8.com/color/64/usa-circular.png',
    id: 138
  }, {
    description: 'Chickenpox',
    url: 'https://img.icons8.com/color/64/chickenpox.png',
    id: 139
  }, {
    description: 'Hitler',
    url: 'https://img.icons8.com/color/64/hitler.png',
    id: 140
  }, {
    description: 'Nuke',
    url: 'https://img.icons8.com/color/64/nuke.png',
    id: 141
  }, {
    description: 'USSR',
    url: 'https://img.icons8.com/color/64/ussr-circular.png',
    id: 142
  }, {
    description: 'China',
    url: 'https://img.icons8.com/color/64/china-circular.png',
    id: 143
  }, {
    description: 'Holy Bible',
    url: 'https://img.icons8.com/color/64/holy-bible.png',
    id: 144
  }, {
    description: 'Overwatch',
    url: 'https://img.icons8.com/color/64/overwatch.png',
    id: 145
  }, {
    description: 'Wake Up',
    url: 'https://img.icons8.com/color/64/wake-up.png',
    id: 146
  }, {
    description: 'Choir',
    url: 'https://img.icons8.com/color/64/choir--v2.png',
    id: 147
  }, {
    description: 'Improvement',
    url: 'https://img.icons8.com/color/64/improvement.png',
    id: 148
  }, {
    description: 'Peace',
    url: 'https://img.icons8.com/color/64/peace-pigeon.png',
    id: 149
  }, {
    description: 'Wildfire',
    url: 'https://img.icons8.com/color/64/wildfire.png',
    id: 150
  }, {
    description: 'City',
    url: 'https://img.icons8.com/color/64/city-buildings.png',
    id: 151
  }, {
    description: 'India',
    url: 'https://img.icons8.com/color/64/india-circular.png',
    id: 152
  }, {
    description: 'Plasma',
    url: 'https://img.icons8.com/color/64/plasma-ball.png',
    id: 153
  }, {
    description: 'Winner',
    url: 'https://img.icons8.com/color/64/winner.png',
    id: 154
  }, {
    description: 'Complaint',
    url: 'https://img.icons8.com/color/64/complaint.png',
    id: 155
  }, {
    description: 'Insects',
    url: 'https://img.icons8.com/color/64/insects.png',
    id: 156
  }, {
    description: 'Pocket',
    url: 'https://img.icons8.com/color/64/pocket.png',
    id: 157
  }, {
    description: 'Wring',
    url: 'https://img.icons8.com/color/64/wring.png',
    id: 158
  }, {
    description: 'Country',
    url: 'https://img.icons8.com/color/64/country.png',
    id: 159
  }, {
    description: 'Internship',
    url: 'https://img.icons8.com/color/64/internship.png',
    id: 160
  }, {
    description: 'Pokeball',
    url: 'https://img.icons8.com/color/64/pokeball-2.png',
    id: 161
  }, {
    description: 'Cross',
    url: 'https://img.icons8.com/color/64/cross.png',
    id: 162
  }, {
    description: 'Ireland',
    url: 'https://img.icons8.com/color/64/ireland-circular.png',
    id: 163
  }, {
    description: 'Cu',
    url: 'https://img.icons8.com/color/64/cup-with-straw.png',
    id: 164
  }, {
    description: 'Israel',
    url: 'https://img.icons8.com/color/64/israel-circular.png',
    id: 165
  }, {
    description: 'Pray',
    url: 'https://img.icons8.com/color/64/pray.png',
    id: 166
  }, {
    description: 'Virtual Reality',
    url: 'https://img.icons8.com/color/64/virtual-reality.png',
    category: 'Games',
    id: 167
  }, {
    description: 'Aftereffects',
    url: '/static/threadicons/aftereffects.svg',
    category: 'Draw Hard',
    id: 168,
    custom: true,
  }, {
    description: 'Anime',
    url: '/static/threadicons/anime.svg',
    category: 'Draw Hard',
    id: 169,
    custom: true,
  }, {
    description: 'Art',
    url: '/static/threadicons/art.svg',
    category: 'Draw Hard',
    id: 170,
    custom: true,
  }, {
    description: 'Chat',
    url: '/static/threadicons/chat.svg',
    category: 'Draw Hard',
    id: 171,
    custom: true,
  }, {
    description: 'Computers',
    url: '/static/threadicons/computers.svg',
    category: 'Draw Hard',
    id: 172,
    custom: true,
  }, {
    description: 'Cpp',
    url: '/static/threadicons/cpp.svg',
    category: 'Draw Hard',
    id: 173,
    custom: true,
  }, {
    description: 'Drama',
    url: '/static/threadicons/drama.svg',
    category: 'Draw Hard',
    id: 174,
    custom: true,
  }, {
    description: 'Everything/nothing',
    url: '/static/threadicons/everything-nothing.svg',
    category: 'Draw Hard',
    id: 175,
    custom: true,
  }, {
    description: 'Games',
    url: '/static/threadicons/games.svg',
    category: 'Draw Hard',
    id: 176,
    custom: true,
  }, {
    description: 'Gross',
    url: '/static/threadicons/gross.svg',
    category: 'Draw Hard',
    id: 177,
    custom: true,
  }, {
    description: 'Heart',
    url: '/static/threadicons/heart.svg',
    category: 'Draw Hard',
    id: 178,
    custom: true,
  }, {
    description: 'Heartbreak',
    url: '/static/threadicons/heartbreak.svg',
    category: 'Draw Hard',
    id: 179,
    custom: true,
  }, {
    description: 'Help',
    url: '/static/threadicons/help.svg',
    category: 'Draw Hard',
    id: 180,
    custom: true,
  }, {
    description: 'Html',
    url: '/static/threadicons/html.svg',
    category: 'Draw Hard',
    id: 181,
    custom: true,
  }, {
    description: 'Humor',
    url: '/static/threadicons/humor.svg',
    category: 'Draw Hard',
    id: 182,
    custom: true,
  }, {
    description: 'Js',
    url: '/static/threadicons/js.svg',
    category: 'Draw Hard',
    id: 183,
    custom: true,
  }, {
    description: 'Life',
    url: '/static/threadicons/life.svg',
    category: 'Draw Hard',
    id: 184,
    custom: true,
  }, {
    description: 'Link',
    url: '/static/threadicons/link.svg',
    category: 'Draw Hard',
    id: 185,
    custom: true,
  }, {
    description: 'Lua',
    url: '/static/threadicons/lua.svg',
    category: 'Draw Hard',
    id: 186,
    custom: true,
  }, {
    description: 'Map',
    url: '/static/threadicons/map.svg',
    category: 'Draw Hard',
    id: 187,
    custom: true,
  }, {
    description: 'Microsoft',
    url: '/static/threadicons/microsoft.svg',
    category: 'Draw Hard',
    id: 188,
    custom: true,
  }, {
    description: 'Money',
    url: '/static/threadicons/money.svg',
    category: 'Draw Hard',
    id: 189,
    custom: true,
  }, {
    description: 'Movies',
    url: '/static/threadicons/movies.svg',
    category: 'Draw Hard',
    id: 190,
    custom: true,
  }, {
    description: 'Music',
    url: '/static/threadicons/music.svg',
    category: 'Draw Hard',
    id: 191,
    custom: true,
  }, {
    description: 'News',
    url: '/static/threadicons/news.svg',
    category: 'Draw Hard',
    id: 192,
    custom: true,
  }, {
    description: 'Nintendo',
    url: '/static/threadicons/nintendo.svg',
    category: 'Draw Hard',
    id: 193,
    custom: true,
  }, {
    description: 'Nsfw',
    url: '/static/threadicons/nsfw.svg',
    category: 'Draw Hard',
    id: 194,
    custom: true,
  }, {
    description: 'Pc',
    url: '/static/threadicons/pc.svg',
    category: 'Draw Hard',
    id: 195,
    custom: true,
  }, {
    description: 'Pets',
    url: '/static/threadicons/pets.svg',
    category: 'Draw Hard',
    id: 196,
    custom: true,
  }, {
    description: 'Photos',
    url: '/static/threadicons/photos.svg',
    category: 'Draw Hard',
    id: 197,
    custom: true,
  }, {
    description: 'Photoshop',
    url: '/static/threadicons/photoshop.svg',
    category: 'Draw Hard',
    id: 198,
    custom: true,
  }, {
    description: 'Php',
    url: '/static/threadicons/php.svg',
    category: 'Draw Hard',
    id: 199,
    custom: true,
  }, {
    description: 'Politics',
    url: '/static/threadicons/politics.svg',
    category: 'Draw Hard',
    id: 200,
    custom: true,
  }, {
    description: 'Poll',
    url: '/static/threadicons/poll.svg',
    category: 'Draw Hard',
    id: 201,
    custom: true,
  }, {
    description: 'Postyour',
    url: '/static/threadicons/postyour.svg',
    category: 'Draw Hard',
    id: 202,
    custom: true,
  }, {
    description: 'Programming',
    url: '/static/threadicons/programming.svg',
    category: 'Draw Hard',
    id: 203,
    custom: true,
  }, {
    description: 'Question',
    url: '/static/threadicons/question.svg',
    category: 'Draw Hard',
    id: 204,
    custom: true,
  }, {
    description: 'Rant',
    url: '/static/threadicons/rant.svg',
    category: 'Draw Hard',
    id: 205,
    custom: true,
  }, {
    description: 'Release',
    url: '/static/threadicons/release.svg',
    category: 'Draw Hard',
    id: 206,
    custom: true,
  }, {
    description: 'Repeat',
    url: '/static/threadicons/repeat.svg',
    category: 'Draw Hard',
    id: 207,
    custom: true,
  }, {
    description: 'Request',
    url: '/static/threadicons/request.svg',
    category: 'Draw Hard',
    id: 208,
    custom: true,
  }, {
    description: 'School',
    url: '/static/threadicons/school.svg',
    category: 'Draw Hard',
    id: 209,
    custom: true,
  }, {
    description: 'Science',
    url: '/static/threadicons/science.svg',
    category: 'Draw Hard',
    id: 210,
    custom: true,
  }, {
    description: 'Shitpost',
    url: '/static/threadicons/shitpost.svg',
    category: 'Draw Hard',
    id: 211,
    custom: true,
  }, {
    description: 'Sony',
    url: '/static/threadicons/sony.svg',
    category: 'Draw Hard',
    id: 212,
    custom: true,
  }, {
    description: 'Stupid',
    url: '/static/threadicons/stupid.svg',
    category: 'Draw Hard',
    id: 213,
    custom: true,
  }, {
    description: 'Tutorial',
    url: '/static/threadicons/tutorial.svg',
    category: 'Draw Hard',
    id: 214,
    custom: true,
  }, {
    description: 'Tv',
    url: '/static/threadicons/tv.svg',
    category: 'Draw Hard',
    id: 215,
    custom: true,
  }, {
    description: 'Valve',
    url: '/static/threadicons/valve.svg',
    category: 'Draw Hard',
    id: 216,
    custom: true,
  }, {
    description: 'Wip',
    url: '/static/threadicons/wip.svg',
    category: 'Draw Hard',
    id: 217,
    custom: true,
  }
]
icons.sort((a, b)=>{
  if (a.description.toLowerCase() < b.description.toLowerCase()) {
    return -1
  }
  if (a.description.toLowerCase() > b.description.toLowerCase()) {
    return 1
  }
  return 0
})
export { icons }

const customIcons = {
  0: {
    description: "Missing",
    url: "not a url",
    notCustom: true,
  },
  1: {
    description: "Drama",
    url: "/static/threadicons/drama.svg",
  },
  2: {
    description: "Gaems",
    url: "/static/threadicons/games.svg",
  },
  3: {
    description: "Life Stuff",
    url: "/static/threadicons/life.svg",
  },
  5: {
    description: "Announcement",
    url: "https://i.imgur.com/v0gCL8d.png",
  }
}

const funFonts = [
  'bold 15px "Arial"',
  'bold 15px "Helvetica"',
  'bold 15px "Times New Roman"',
  'bold 15px "Times"',
  'bold 15px "Courier New"',
  'bold 15px "Courier"',
  'bold 15px "Verdana"',
  'bold 15px "Georgia"',
  'bold 15px "Palatino"',
  'bold 15px "Garamond"',
  'bold 15px "Bookman"',
  'bold 15px "Comic Sans MS"',
  'bold 15px "Trebuchet MS"',
  'bold 15px "Arial Black"',
  '15px "Impact"',
]

export default class ThreadIcon extends Component {
  constructor(props) {
    super(props)
    this.ref = React.createRef()
    this.state = {
      scale: 1,
      scaleY: 1,
    }
  }
  componentDidMount() {
    if (this.ref.current) {
      let width = this.ref.current.offsetWidth
      let parentWidth = this.ref.current.parentNode.offsetWidth
      let height = this.ref.current.offsetHeight
      let parentHeight = this.ref.current.parentNode.offsetHeight
      this.setState({scale: parentWidth / width, scaleY: parentHeight / height})
    }
  }
  render() {
    let icon = icons.find(a => a.id == this.props.id)
    if (!icon) {
      icon = icons[0]
    }
    let custom = false
    if (customIcons[this.props.id]) {
      icon = customIcons[this.props.id]
      if (!icon.notCustom)
        custom = true
    }
    if (icon.custom) {
      custom = true
    }
    if (custom) {
      return <div className="thread-icon-custom" title={icon.description} style={{backgroundImage: 'url(' + icon.url + ')'}}>

      </div>
    } else {
      return <div className="thread-icon" title={icon.description} style={{backgroundImage: 'url(' + icon.url + ')'}}>
        <img src={icon.url} width="20px"/>
        <div className="text-container">
          <div className="text-bit" style={{transform: 'scale(' + this.state.scale + ', ' + this.state.scaleY + ')', font: funFonts[this.props.id % funFonts.length]}} ref={this.ref}>
            {icon.description}
          </div>
        </div>
      </div>
    }
  }
}
