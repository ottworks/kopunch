import React, { Component } from "react"
export default class Snow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      enabled: false,
      width: 800,
      height: 600,
      mouseX: 400,
      mouseY: 300,
    }
    this.paint = this.paint.bind(this)
    this.updateWidth = this.updateWidth.bind(this)

    if ((new Date()).getMonth() == 11)
      this.state.enabled = true

    this.flakes = []
    for (let i = 0; i < 100; ++i) {
      let depth = Math.random() * 0.8 + 0.2
      this.flakes[i] = {
        x: Math.random() * this.state.width / depth,
        y: Math.random() * this.state.height / depth,
        depth: depth,
      }
    }
  }
  componentDidMount() {
    window.requestAnimationFrame(this.paint)
    this.updateWidth()
    window.addEventListener('resize', this.updateWidth)
  }
  updateWidth() {
    if (this.state.width != window.innerWidth) {
      for (let flake of this.flakes) {
        flake.x *= window.innerWidth / this.state.width;
      }
    }
    if (this.state.height != window.innerHeight) {
      for (let flake of this.flakes) {
        flake.x *= window.innerHeight / this.state.height;
      }
    }

    this.setState({width: window.innerWidth, height: window.innerHeight})
  }
  paint(time) {
    if (!this.canvas)
      return

    
    this.state.mouseX = (this.props.mouse.x - this.state.mouseX) * 0.1 + this.state.mouseX
    this.state.mouseY = (this.props.mouse.y - this.state.mouseY) * 0.1 + this.state.mouseY

    if (isNaN(this.state.mouseX))
      this.state.mouseX = 0
    if (isNaN(this.state.mouseY))
      this.state.mouseY = 0

    let ctx = this.canvas.getContext("2d")

    ctx.clearRect(0, 0, this.state.width, this.state.height)

    ctx.globalCompositeOperation = 'source-over'
    if (this.props.dark) {
      let grad = ctx.createRadialGradient(this.state.mouseX, this.state.mouseY, 1, this.state.mouseX, this.state.mouseY, Math.min(this.state.width, this.state.height) / 3)
      grad.addColorStop(0, 'white')
      grad.addColorStop(1, '#0000')

      ctx.fillStyle = grad 
      ctx.fillRect(0, 0, this.state.width, this.state.height)
      ctx.globalCompositeOperation = 'source-in'
    }

    ctx.fillStyle = 'rgba(255, 255, 255, 0.6)'
    ctx.save()

    ctx.beginPath()

    for (const flake of this.flakes) {
      flake.y += 3
      flake.x += Math.sin(time / 10000 + flake.depth * 2) * 2
      while (flake.y > this.state.height / flake.depth)
        flake.y -= this.state.height / flake.depth
      while (flake.x > this.state.width / flake.depth)
        flake.x -= this.state.width / flake.depth
      while (flake.x < 0)
        flake.x += this.state.width / flake.depth

      
      ctx.arc(flake.x, flake.y, 6 * flake.depth, 0, 2 * Math.PI)
      ctx.closePath()
    }

    ctx.fill()

    ctx.restore()

    

    window.requestAnimationFrame(this.paint)
  }
  render() {
    if (!this.state.enabled)
      return null
    return <canvas className="super-secret" ref={(e)=>{this.canvas = e}} width={this.state.width} height={this.state.height}/>
  }
}