import React, { Component } from "react";

const minute = 60
const hour = minute * 60
const day = hour * 24
const week = day * 7
const year = day * 365.2422
const month = year / 12

export function elapsedString(date, date2) {
  if (typeof date === "undefined") {
    return "Undefined"
  }
  if (!date.getDate) {
    date = new Date(date)
  }
  let nodate2 = false
  if (typeof date2 === "undefined") {
    nodate2 = true
    date2 = new Date()
  }
  let units = date2.getTime() - (new Date(date)).getTime()
  units = Math.floor(units / 1000)
  let unit = "Second"

  if (nodate2 && units > month) {
    let day = date.getDate()
    let year = date.getFullYear()
    let month = date.toLocaleDateString(undefined, {month: "long"})
    let suffix = "th "
    let lastdigit = day % 10
    if (day < 11 || day > 13) {
      if (lastdigit === 1) {
        suffix = "st "
      } else if (lastdigit === 2) {
        suffix = "nd "
      } else if (lastdigit === 3) {
        suffix = "rd "
      }
    }
    return day + suffix + month + " " + year
  }

  if (units > year) {
    unit = "Year"
    units = Math.floor(units / year)
  } else if (units > month) {
    unit = "Month"
    units = Math.floor(units / month)
  } else if (units > week) {
    unit = "Week"
    units = Math.floor(units / week)
  } else if (units > day) {
    unit = "Day"
    units = Math.floor(units / day)
  } else if (units > hour) {
    unit = "Hour"
    units = Math.floor(units / hour)
  } else if (units > minute) {
    unit = "Minute"
    units = Math.floor(units / minute)
  }
  return units + " " + unit + (units == 1 ? "" : "s") + (nodate2 ? " Ago" : "")
}

export function TimeDuration(props) {
  return <span title={new Date(props.date).toString()}>{elapsedString(new Date(props.from), new Date(props.to))}</span>
}

export default function TimeElapsed(props) {
  return <span title={new Date(props.date).toString()}>{elapsedString(new Date(props.date))}</span>
}

