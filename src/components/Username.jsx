import React, { Component, useDebugValue } from "react";
import { getUser } from "./api/user";
import LinkWrapper from './LinkWrapper'
import './Username.css'
import AppStateContext from "./AppStateContext";

export function getUserGroupName(author) {
  let group = "Memeber"
  switch (author.role.code) {
    case 'limited-user': group = "User"; break;
    case 'basic-user': group = "Member"; break;
    case 'gold-user': group = "Gold Member"; break;
    case 'paid-gold-user': group = "Gold Member"; break;
    case 'moderator': group = "Moderator"; break;
    case 4: group = "Cat Enthusiast"; break;
    case 'admin': group = "Admin"; break;
    case 'moderator-in-training': group = "Moderator In Training"; break;
    case 'banned-user': group = "Banned"; break;
  }
  if (author.isBanned)
    group = "Banned"
  return group
}

export function getUserTitleOrGroupName(author) {
  let group = author.role && getUserGroupName(author)
  if (group !== "Banned" && author.title) {
    return author.title
  }
  return group
}

export default function Username(props) {
  let [appState, appSetState] = React.useContext(AppStateContext)
  let [user, setUser] = React.useState(props.user)
  React.useEffect(() => {
    if (props.id) {
      getUser(appState.api, props.id, setUser)
    }
  }, [props.id])
  if (user === undefined) return "no user"
  let group = user?.role && getUserGroupName(user)
  return <LinkWrapper title={group} onClick={props.onClick} href={props.href || ("/member.php?u=" + user.id)} className={"user-name user-role-" + user.role?.code + (user.isBanned ? " banned" : "")}>
    <span className={"user-name-text" + (props.shadow ? " shadow" : "")}>{props.at ? '@' : ''}{user.username}</span>
  </LinkWrapper>
}
