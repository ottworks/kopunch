import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import LinkWrapper from './../LinkWrapper'
import { emotes } from '../Post'
import '../../stylesheets/misc.css'

const tabs = ["Profile", "Settings"]

export default class Misc extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.state = {
      loaded: true,
    }
  }
  componentDidMount() {

  }
  componentDidUpdate(prevprops, prevstate, snapshot) {

  }
  render() {
    return <div id="forum">
      <Header title="Smile List" appState={this.props.appState} appSetState={this.props.appSetState}/>
      {this.state.loaded && <div className="content-inner">
        <div className="misc">
          <h1 className="page-title">Smilies</h1>
          <div className="block">
            <h2 className="blockhead">Explanation</h2>
            <div className="blockbody">
              <p>
                'Smilies' are small graphical images that can be used to convey an emotion or feeling. If you have used email or internet chat, 
                you are likely familiar with the smilie concept. Certain standard strings are automatically converted into smilies. Try twisting 
                your head on one side if you do not 'get' smilies; using a bit of imagination should reveal a face of some description.
              </p>
              <br/>
              <p>
                If you want to disable smilies in a post that you make, you can select the 'Disable Smilies' option when posting. This is 
                particularly useful if you are posting program code and you do not want <b>;)</b> converted to a smilie face!
              </p>
            </div>
            <h2 className="blockhead">Smile List</h2>
            <table className="smilies">
              <thead>
                <tr>
                  <td>What to Type</td>
                  <td>Resulting Graphic</td>
                  <td>Meaning</td>
                </tr>
              </thead>
              <tbody>
                {emotes.map(emote=>{
                  return <tr>
                    <td>{emote.Code}</td>
                    <td><img src={emote.Url}/></td>
                    <td>{emote.Name}</td>
                  </tr>
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>}
      <Footer/>
    </div>
  }
}
