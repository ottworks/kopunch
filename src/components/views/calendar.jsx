import React, { Component } from "react"
import Header from '../Header'
import Footer from '../Footer'
import { elapsedString } from '../TimeElapsed'
import '../../stylesheets/calendar.css'
import LinkWrapper from "../LinkWrapper"


function Calendar({firstdate, start, end, lastdate, data, onSelectEvent, onSelectDay, selectedDay, selectedEvent}) {
  let now = new Date()
  let numdays = Math.round((lastdate - firstdate) / 1000 / 60 / 60 / 24)
  console.log("numdays", numdays, firstdate, start, end, lastdate)
  let days = []
  for (let i = 0; i <= numdays; ++i) {
    let curday = new Date(firstdate.getFullYear(), firstdate.getMonth(), firstdate.getDate() + i)
    let curevents = data.filter(e => new Date(e.startsAt) <= new Date(curday.getFullYear(), curday.getMonth(), curday.getDate() + 1) && new Date(e.endsAt) >= curday)

    days.push(<div className={`calendarday ${
      curday.getMonth() !== start.getMonth() ? "extra" : ""
    } ${
      curday.getMonth() === now.getMonth() && curday.getDate() === now.getDate() && curday.getFullYear() === now.getFullYear() ? "today" : ""
    } ${
      curevents.length > 0 ? "active" : ""
    }`}>
      <div className="day" tabIndex="0" onClick={()=>onSelectDay(curday)}>
        {curday.toLocaleDateString(undefined, {
          weekday: 'long', 
        })} {curday.toLocaleDateString(undefined, {
          day: 'numeric',
        })}
      </div>
      {(selectedDay - curday) === 0 && selectedEvent === undefined && <div className="popover panel">
        <h5>Schedule:</h5>
        {curevents.slice().sort((a, b)=>(new Date(a.startsAt) < new Date(b.startsAt) ? 1 : (a.startsAt === b.startsAt ? 0 : -1))).map(e => {
          return <p>
            {new Date(e.startsAt).toLocaleTimeString(undefined, { timeZoneName: 'short' })} - {new Date(e.endsAt).toLocaleTimeString(undefined, { timeZoneName: 'short' })}:{" "}
            <LinkWrapper href="#" onClick={(v)=>{v.preventDefault(); onSelectEvent(curday, e.id)}}>{e.title}</LinkWrapper>
          </p>
        })}
      </div>}
      {curevents.map(e => {
        return <div key={curday.toString()} className="calendarevent">
          <div className="title" tabIndex="0" role="button" onClick={() => onSelectEvent(curday, e.id)}>{e.title}</div>
          {(selectedDay - curday) === 0 && selectedEvent == e.id && <div className="popover panel">
            <h5>Event Title:</h5>
            <p>{e.title}</p>
            <h5>Event Description:</h5>
            <p>{e.description}</p>
            <h5>Event Thread:</h5>
            <p><LinkWrapper href={`/showthread.php?t=${e.thread.id}`}>{e.thread.title}</LinkWrapper></p>
            <h5>Event Time:</h5>
            <p>{new Date(e.startsAt).toLocaleTimeString(undefined, { timeZoneName: 'short' })} - {new Date(e.endsAt).toLocaleTimeString(undefined, { timeZoneName: 'short'  })}</p>
            <h5>Event Date:</h5>
            <p>{new Date(e.startsAt).toLocaleDateString()} - {new Date(e.endsAt).toLocaleDateString()}</p>
          </div>}
        </div>
      })}
    </div>)
  }
  return <div className="calendar">
    {days}
  </div>
}

export default class CalendarPage extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      loaded: false,
      monthdate: this.props.query.date ? new Date(this.props.query.date) : new Date(),
    }
    this.lastTime = ""
    this.fetchCalendar = this.fetchCalendar.bind(this)
  }
  componentDidMount() {
    this.fetchCalendar()
  }
  componentDidUpdate(prevprops, prevstate) {
    if (prevstate.monthdate - this.state.monthdate !== 0) {
      //this.fetchCalendar();
    }
  }
  fetchCalendar() {
    let [firstdate, start, end, lastdate] = this.getMonthRange(this.state.monthdate.getFullYear(), this.state.monthdate.getMonth())
    if (Math.round((lastdate - firstdate) / 1000 / 60 / 60 / 24) > 40) {
      lastdate = new Date(firstdate.getFullYear(), firstdate.getMonth(), firstdate.getDate() + 40)
    }
    window.fetch(`${this.props.appState.api}/v2/calendarEvents/?startDate="${firstdate}&endDate=${lastdate}"`, { 
      credentials: 'include',
    }).then(res => res.json()).then((result) => {
      console.log(result)
      this.setState({ loaded: true, data: result })
    })
  }
  getMonthRange(year, month) {
    let start = new Date(year, month)
    let startday = start.getDay()
    let firstdate = new Date(year, month, -startday + 1)
    let end = new Date(year, month + 1, 0)
    let endday = end.getDay()
    let lastdate = new Date(year, month + 1, 6 - endday)
    return [firstdate, start, end, lastdate]
  }
  render() {
    let [firstdate, start, end, lastdate] = this.getMonthRange(this.state.monthdate.getFullYear(), this.state.monthdate.getMonth())
    return (
      <div id="forum">
        <Header title="Calendar" appState={this.props.appState} appSetState={this.props.appSetState} />
        <div className="content-inner">
          <table>
            <tbody>
              <tr>
                <td>
                  <div className="calendarhead">
                    <LinkWrapper href={`/calendar.php?date=${new Date(this.state.monthdate.getFullYear(), this.state.monthdate.getMonth() - 1).toLocaleDateString(undefined, { dateStyle: 'short' })}`}>
                      &lt; {new Date(start.getFullYear(), start.getMonth() - 1).toLocaleDateString(undefined, {
                        month: 'long',
                      })}
                    </LinkWrapper>
                    <span className="cur">
                      {start.toLocaleDateString(undefined, {
                        month: 'long',
                        year: 'numeric',
                      })}
                    </span>
                    <LinkWrapper href={`/calendar.php?date=${new Date(this.state.monthdate.getFullYear(), this.state.monthdate.getMonth() + 1).toLocaleDateString(undefined, { dateStyle: 'short' })}`}>
                      {new Date(start.getFullYear(), start.getMonth() + 1).toLocaleDateString(undefined, {
                        month: 'long',
                      })} &gt;
                    </LinkWrapper>
                  </div>
                  {this.state.loaded && <Calendar
                    data={this.state.data}
                    firstdate={firstdate}
                    start={start}
                    end={end}
                    lastdate={lastdate}
                    selectedDay={this.state.selectedDay}
                    selectedEvent={this.state.selectedEvent}
                    onSelectEvent={(curday, id) => {
                      console.log("onSelectEvent", curday, id)
                      if (this.state.selectedDay - curday === 0 && this.state.selectedEvent === id) {
                        this.setState({ selectedDay: undefined, selectedEvent: undefined })
                      } else {
                        this.setState({ selectedDay: curday, selectedEvent: id })
                      }
                    }}
                    onSelectDay={(curday) => {
                      console.log("onSelectDay", curday)
                      if (this.state.selectedDay - curday === 0 && this.state.selectedEvent === undefined) {
                        this.setState({ selectedDay: undefined, selectedEvent: undefined })
                      } else {
                        this.setState({ selectedDay: curday, selectedEvent: undefined })
                      }
                    }}
                  />}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <Footer />
      </div>
    );
  }
}