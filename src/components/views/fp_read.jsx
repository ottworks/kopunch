import React, { Component } from "react"
import Header from '../Header'
import Footer from '../Footer'
import { ThreadEntry } from './forumdisplay'
import LinkWrapper from './../LinkWrapper'

const ALERTS_PER_PAGE = 20

export default class Read extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.loadRead = this.loadRead.bind(this)
    this.loadLatest = this.loadLatest.bind(this)
    this.pruneSubscriptions = this.pruneSubscriptions.bind(this)
    this.alertSort = this.alertSort.bind(this)
    this.setTagFilter = this.setTagFilter.bind(this)
    this.state = {
      loaded: false,
      loadingMore: false,
      loaded2: false,
      tagFilter: "",
    }
    this.data = props.appState.readthreads
    this.fetchedThreadIds = []
    this.lastTime = ""
  }
  loadRead(page) {
    if (page === undefined) {
      page = 1
    }
    window.fetch(this.props.appState.api + "/alerts/" + page, {
      method: 'GET', 
      credentials: 'include',
      headers: {
        "content-format-version": "1",
      }
    }).then(res => res.json()).then((result)=>{
      console.log("read result", result)
      if (result.error || result.message) {
        this.setState({error: result.error || result.message})
      } else {
        let readthreads = this.data.slice()
        if (page === 1) {
          for (const a of readthreads) {
            a.new = false
          }
        }
        for (const a of result.alerts) {
          let index = readthreads.findIndex(({ threadId }) => threadId === a.threadId)
          if (index === -1) {
            index = readthreads.length
            a.new = true
          } else if (readthreads[index].unreadPosts !== a.unreadPosts) {
            a.new = true
          } else {
            a.new = false
          }
          readthreads[index] = a
          this.fetchedThreadIds.push(a.threadId)
        }
        readthreads.sort(this.alertSort);
        this.data = readthreads
        this.setState({ totalAlerts: result.totalAlerts, totalStaleAlerts: result.totalAlerts, alertPage: page, loaded: true, loadingMore: false})
        if (this.props.appState.subload) {
          if (result.alerts[result.alerts.length - 1].firstUnreadId !== -1) {
            console.log("loading more")
            this.setState({ loadingMore: true })
            this.loadRead(page + 1)
          } else {
            console.log("done", this.props.appState.readthreads)
            for (let i = 0; i < this.data.length; ++i) {
              if (!this.fetchedThreadIds.includes(this.data[i].threadId)) {
                this.data.splice(i, 1)
                console.log("removed", i)
                --i
              }
            }
            this.forceUpdate()
          }
        }
      }
    })
  }
  alertSort(a, b) {
    let aid = a.firstUnreadId
    let bid = b.firstUnreadId
    if (aid === -1) {
      if (bid !== -1) {
        return 1
      }
      aid = a.lastPost.id
    }
    if (bid === -1) {
      if (aid !== -1) {
        return -1
      }
      bid = b.lastPost.id
    }

    if (aid > bid) return -1;
    if (aid < bid) return 1;
    return 0;
  }
  pruneSubscriptions() {
    let page = Math.floor((this.state.totalStaleAlerts - 1) / ALERTS_PER_PAGE) + 1
    console.log("totalStaleAlerts", this.state.totalStaleAlerts, "page", page)
    window.fetch(this.props.appState.api + "/alerts/" + page, {
      method: 'GET', 
      credentials: 'include',
      headers: {
        "content-format-version": "1",
      }
    }).then(res => res.json()).then((result)=>{
      console.log("prune read result", result)
      let alerts = result.alerts
      let hadOne = false

      const pickOne = () => {
        let thread = alerts.shift()
        if (thread === undefined) {
          if (this.state.totalStaleAlerts > 0) {
            this.pruneSubscriptions()
          } else {
            this.setState({ loadingMore: false, pruningSubscriptions: false })
          }
          return
        }
        console.log(this.state.totalStaleAlerts, thread.threadId, thread.threadTitle)
        if ((new Date() - new Date(thread.threadUpdatedAt)) / (1000 * 60 * 60 * 24) < 14) { // TODO: eventually threadUpdatedAt will change to updatedAt
          console.log("Thread skipped - thread was not older than two weeks")
          this.setState((state) => {
            return { totalStaleAlerts: state.totalStaleAlerts - 1 }
          })
          pickOne()
          return
        }
        if (thread.threadUser == this.props.appState.user.id) {
          this.setState((state) => {
            return { totalStaleAlerts: state.totalStaleAlerts - 1 }
          })
          console.log("Thread skipped - OP was you!")
          pickOne()
          return
        }
        hadOne = true
        window.fetch(this.props.appState.api + "/alerts", {
          method: 'DELETE',
          credentials: 'include',
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            "content-format-version": "1",
          },
          body: JSON.stringify({
            threadId: thread.threadId
          }),
          mode: "cors"
        }).then(r => {
          if (r.status !== 200) {
            this.setState({ loadingMore: false, pruningSubscriptions: false })
            console.log("Aborting thread prune - unsubscribe returned status", r.status)
            return
          }
          console.log("unsubbed")
          this.setState((state) => {
            return { totalAlerts: state.totalAlerts - 1, totalStaleAlerts: state.totalStaleAlerts - 1 }
          })
          pickOne()
        })
      }
      pickOne()
    })
  }
  loadLatest() {
    window.fetch(this.props.appState.api + "/v2/threads/latest", {
      credentials: 'include',
      headers: {
        "content-format-version": "1",
      }
    }).then(res => res.json()).then((result)=>{
      console.log(result)
      if (result.error) {
        this.setState({error2: result.error})
      } else if (!result.message) {
        this.data2 = result
        this.setState({loaded2: true})
      }
    })
  }
  componentDidMount() {
    console.log("componentDidMount")
    this.data = this.props.appState.readthreads
    this.loadRead()
    this.loadLatest()
  }
  componentWillUnmount() {
    console.log("componentWillUnmount")
    this.props.appSetState({readthreads: this.data})
  }
  componentDidUpdate(prevprops) {
  }
  setTagFilter(tag) {
    this.setState({ tagFilter: tag })
  }
  render() {
    return (
      <div id="forum">
        <Header title="Read Threads" appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table>
            <thead>
              <tr className="category-title">
                <td></td>
                <td><b>{this.props.appState.autosub === "read" ? "Read" : "Subscribed"} Threads With New Posts</b></td>
                <td>Last Post By</td>
                <td>Replies</td>
                <td>Viewers</td>
              </tr>
            </thead>
            <tbody>
              {this.state.loaded && this.data.map((data, index)=>{
                if (data?.unreadPosts)
                  return <ThreadEntry key={data.threadId} withSubforum data={data} read="true" subscribed="true" tagFilter={this.state.tagFilter} setTagFilter={this.setTagFilter} appState={this.props.appState} appSetState={this.props.appSetState}/>
              })}
              {this.state.error && <tr className="error-row">
                <td/>
                <td>{this.state.error} <LinkWrapper href="" className="link-button" onClick={(e)=>{
                  e.preventDefault()
                  this.setState({error: false})
                  this.loadRead()
                }}>Retry?</LinkWrapper></td>
              </tr>}
            </tbody>
            <thead>
              <tr className="category-title">
                <td></td>
                <td><b>Subscriptions</b></td>
                <td>Last Post By</td>
                <td>Replies</td>
                <td>Viewers</td>
              </tr>
            </thead>
            <tbody>
              {this.state.loaded && this.data.map((data, index)=>{
                if (data && !data.unreadPosts)
                  return <ThreadEntry key={data.threadId} withSubforum data={data} subscribed="true" tagFilter={this.state.tagFilter} setTagFilter={this.setTagFilter} appState={this.props.appState} appSetState={this.props.appSetState}/>
              })}
              {this.state.loaded && this.data.length < this.state.totalAlerts && <tr className="thread-entry load-more">
                <td/>
                <td>
                  {!this.state.pruningSubscriptions && <>
                    {this.state.totalAlerts - this.data.length} threads not shown.&nbsp;
                  </>}
                  {this.state.pruningSubscriptions && <>
                    {this.state.totalStaleAlerts} threads left to process.&nbsp;
                  </>}
                  <LinkWrapper 
                    className={"link-button" + (this.state.loadingMore ? " disabled" : "")} 
                    href="#" 
                    onClick={(e)=>{
                      e.preventDefault()
                      if (!this.state.loadingMore) {
                        this.setState({loadingMore: true})
                        this.loadRead(this.state.alertPage + 1)
                      }
                    }}
                  >Load more</LinkWrapper>&nbsp;
                  <LinkWrapper 
                    className={"link-button" + (this.state.loadingMore ? " disabled" : "")} 
                    href="#" 
                    title="Unsubscribes to threads older than two weeks and not made by you." 
                    onClick={(e)=>{
                      e.preventDefault()
                      if (!this.state.loadingMore) {
                        this.setState({loadingMore: true, pruningSubscriptions: true})
                        try {
                          this.pruneSubscriptions()
                        } catch (e) {
                          this.setState({ pruneError: e.message, loadingMore: false, pruningSubscriptions: false})
                        }
                      }
                    }}
                  >Prune subscriptions</LinkWrapper>
                  {this.state.pruneError && <div className="error">{this.state.pruneError}</div>}
                </td>
              </tr>}
            </tbody>
            <thead>
              <tr className="category-title">
                <td></td>
                <td><b>Latest Created Threads</b></td>
                <td>Last Post By</td>
                <td>Replies</td>
                <td>Viewers</td>
              </tr>
            </thead>
            <tbody>
              {this.state.loaded2 && this.data2.map((data, index)=>{
                return <ThreadEntry key={data.id} withSubforum tagFilter={this.state.tagFilter} setTagFilter={this.setTagFilter} data={data} appState={this.props.appState} appSetState={this.props.appSetState}/>
              })}
              {this.state.error2 && <tr className="error-row">
                <td/>
                <td>{this.state.error2} <LinkWrapper href="" className="link-button" onClick={(e)=>{
                  e.preventDefault()
                  this.setState({error2: false})
                  this.loadLatest()
                }}>Retry?</LinkWrapper></td>
              </tr>}
            </tbody>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}