import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed, { elapsedString } from '../TimeElapsed'
import '../../stylesheets/newreply.css'
import '../../stylesheets/showthread.css'
import PostEditor from '../PostEditor'
import { vegitate, getbbc } from '../PostEditor'
import LinkWrapper from './../LinkWrapper'
import { POSTS_PER_PAGE } from './showthread'
/*
await fetch("https://forums.stylepunch.club:3000/post", {
    "credentials": "include",
    "headers": {
        "Content-Type": "application/json;charset=utf-8"
    },
    "referrer": "https://forums.stylepunch.club/thread/78/1",
    "body": "{\"content\":\"{\\\"object\\\":\\\"value\\\",\\\"document\\\":{\\\"object\\\":\\\"document\\\",\\\"data\\\":{},\\\"nodes\\\":[{\\\"object\\\":\\\"block\\\",\\\"type\\\":\\\"paragraph\\\",\\\"data\\\":{},\\\"nodes\\\":[{\\\"object\\\":\\\"text\\\",\\\"leaves\\\":[{\\\"object\\\":\\\"leaf\\\",\\\"text\\\":\\\"Ohh﻿ edit\\\",\\\"marks\\\":[]}]}]}]}}\",\"id\":119,\"threadId\":78}",
    "method": "PUT",
    "mode": "cors"
});
*/

export function stripQuotes(content) {
  let cursor = 0
  let depth = 0
  let quotestart = 0
  while (cursor < content.length)
  {
    open = content.indexOf("[quote", cursor)
    close = content.indexOf("[/quote]", cursor)
    if (open == -1 && close == -1)
    {
      break
    }
    else if (open != -1 && (open < close || close == -1))
    {
      depth++
      cursor = open + 1;
      if (depth == 1)
      {
        quotestart = open
      }
    }
    else if (close != -1 && (close < open || open == -1))
    {
      depth--
      cursor = close + 1;
      if (depth == 0)
      {
        content = content.substring(0, quotestart) + content.substring(close + "[/quote]".length)
        cursor = quotestart
      }
    }
    else
    {
      break
    }
  }
  return content
}

export default class NewReply extends Component {
  constructor(props) {
    super(props)
    this.query = this.props.query
    this.state = {
      loaded: false,
      text: localStorage.getItem("kopunch_thread_reply_" + this.query.t) || "",
    }
    this.lastTime = ""
    this.onTextChange = this.onTextChange.bind(this)
  }
  componentDidMount() {
    let getpost = this.props.query.p || this.props.query.edit || undefined
    if (getpost) {
      window.fetch(this.props.appState.api + "/v2/posts/" + getpost, {credentials: 'include'}).then(res => res.json()).then((result)=>{
        console.log(result)
        let quote = result.content
        if (this.props.query.p) {
          let content = stripQuotes(quote)
          this.setState({text: this.state.text + '[quote mentionsUser="' + result.user.id + '" username="' + result.user.username + '" threadId="' + this.props.query.t + '" threadPage="' + this.props.query.page + '" postId="' + result.id + '"]\n' + content.trim() + '\n[/quote]\n\n'})
        } else {
          this.setState({text: this.state.text + quote})
        }
      })
    }
    window.fetch(this.props.appState.api + "/thread/" + Number(this.props.query.t)).then(r=>r.json()).then((r)=>{
      this.setState({subforum: r.subforum.name, subforumid: r.subforum.id, thread: r.title, threadid: r.id})
    })
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    
  }
  onTextChange(text) {
    this.setState({text: text})
    localStorage.setItem("kopunch_thread_reply_" + this.query.t, text)
  }
  render() {
    return (
      <div id="forum">
        <Header title="Post New Reply" subforum={this.state.subforum} subforumid={this.state.subforumid} thread={this.state.thread} threadid={this.state.threadid} appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table>
            <thead>
              <tr className="category-title">
                <td><b>Your Message</b></td>
              </tr>
            </thead>
            <div className="table-content">
              <PostEditor text={this.state.text} onChange={this.onTextChange} thread={this.props.query.t} page={this.props.query.page} edit={this.props.query.edit} onCancel={()=>{
                this.onTextChange("")
                window.history.back()
              }} doingReply={this.state.doingReply} appState={this.props.appState} appSetState={this.props.appSetState}/>
            </div>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}
