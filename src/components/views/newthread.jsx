import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed, { elapsedString } from '../TimeElapsed'
import { postPlugin } from '../Post'
import '../../stylesheets/newthread.css'
import '../../stylesheets/showthread.css'
import PostEditor from '../PostEditor'
import { vegitate, getbbc } from '../PostEditor'
import ThreadIcon, { icons } from '../ThreadIcon'
import LinkWrapper from './../LinkWrapper'

export default class NewThread extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      loaded: false,
      text: "",
      title: "",
      icon: 0,
      error: "",
      tagIds: [],
      subforum: "",
    }
    this.lastTime = ""
    this.doThread = this.doThread.bind(this)


  }
  componentDidMount() {
    window.fetch(this.props.appState.api + "/subforum/" + Number(this.props.query.f), {
      credentials: 'include',
    }).then(r=>r.json()).then((r)=>{
      this.setState({subforum: r.name})
    })
  }
  doThread(text) {
    this.setState({error: ""})
    let content = text
    console.log(JSON.stringify({
        content: content,
        thread_id: this.props.query.t,
      }))
    window.fetch(this.props.appState.api + "/v2/threads", {
      method: 'POST', 
      credentials: 'include',
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "content-format-version": "1",
      },
      body: JSON.stringify({
        content: content,
        subforum_id: Number(this.props.query.f),
        icon_id: Number(this.state.icon),
        title: this.state.title,
        displayCountryInfo: this.props.appState.flagdog,
        appName: "kopunch",
        tag_ids: this.state.tagIds,
      }),
    }).then(r=>r.text()).then((r)=>{
      try {
        r = JSON.parse(r)
        if ((r.error || r.errors)) {
          this.setState({error: (r.error || r.errors)})
        } else {
          window.fetch(this.props.appState.api + "/alerts", {
            method: 'POST', 
            credentials: 'include',
            headers: {
              "Content-Type": "application/json;charset=utf-8",
              "content-format-version": "1",
            },
            body: JSON.stringify({
              threadId: r.id,
              thread_id: r.id,
              lastPostNumber: 1,
              last_post_number: 1,
            }),
          }).then(r=>r.json()).then((r2)=>{
            console.log(r2)
            window.location.href = "/showthread.php?t=" + r.id
          })
        }
      } catch (e) {
        this.setState({error: r})
      }
    })
  }
  render() {
    return (
      <div id="forum">
        <Header title="Post New Thread" subforumid={Number(this.props.query.f)} subforum={this.state.subforum} appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table>
            <thead>
              <tr className="category-title">
                <td><b>Your Message</b></td>
              </tr>
            </thead>
            <div className="table-content">
              <b>Title:</b><br/>
              <input className="title-input" type="text" value={this.state.title} onChange={(e)=>{this.setState({title: e.target.value})}}/>
              <hr/>
              <PostEditor onSubmit={this.doThread} appState={this.props.appState} appSetState={this.props.appSetState}/>
              {this.state.error && <span className="error">{this.state.error}</span>}
              <hr/>
              <b>Tags: </b>
              {this.state.tagIds.map(id=>this.props.appState.taglist[id] + ", ")}
              <select name="tags" id="tag-select" value="" onChange={(e)=>{
                let ids = this.state.tagIds.slice()
                let index = ids.indexOf(e.target.value)
                if (index == -1) {
                  ids.push(Number(e.target.value))
                } else {
                  ids.splice(index, 1)
                }
                this.setState({tagIds: ids})
              }}>
                <option value="">-</option>
                {Object.keys(this.props.appState.taglist).map(key=>{
                  return <option value={key} key={key}>{(this.state.tagIds.includes(Number(key)) ? "✓ " : "✗ ") + this.props.appState.taglist[key]}</option>
                })}
              </select>
              <hr/>
              <b>Post Icons:</b>
              <form className="thread-icons" onChange={(e)=>{this.setState({icon: Number(e.target.value)})}}>
              	{icons.map((v,k)=>{
              	  return <div key={v.id}>
              	  	<input type="radio" name="threadicon" checked={this.state.icon == v.id} id={v.id} value={v.id}/>
              	  	<label htmlFor={v.id}><ThreadIcon id={v.id}/></label>
              	  </div>
              	})}
              </form>
            </div>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}
