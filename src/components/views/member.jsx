import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed, { elapsedString, TimeDuration } from '../TimeElapsed'
import Username, {getUserTitleOrGroupName} from '../Username'
import LinkWrapper from './../LinkWrapper'
import Post, { SocialIcons } from '../Post'
import PostEditor from '../PostEditor'
import Pagination from '../Pagination'
import { ratings } from '../Rating'
import '../../stylesheets/member.css'
import { markNotificationsAsRead } from "../api/notifications"

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const tabs = ["Visitor Messages", "About Me", "Events"]

export var BanEntry = (props)=>{
  let src = "/static/silkicons/world_delete.png"

  return <div key={props.data.id} className="event-entry">
    {props.showTime && 
      <p className="event-time small">
        <TimeElapsed date={props.data.createdAt}/>
      </p>
    }
    <p className="event">
      <img src={src} className="icon"/>
      <LinkWrapper href={"/member.php?u=" + props.data.bannedBy.id}>{props.data.bannedBy.username}</LinkWrapper> banned <LinkWrapper href={"/member.php?u=" + props.data.user.id}>{props.data.user.username}</LinkWrapper> for <b><TimeDuration from={props.data.createdAt} to={props.data.expiresAt}/></b> {props.data.thread && ["in ", <b><LinkWrapper href={"/showthread.php?t=" + props.data.thread.id + "&page=" + props.data.post.page + "&p=" + props.data.post.id}>{props.data.thread.title}</LinkWrapper></b>]} with the reason “<b>{props.data.banReason}</b>”
    </p>
  </div>
}

export default class Member extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.clickBg = this.clickBg.bind(this)
    this.ignore = this.ignore.bind(this)
    this.state = {
      loaded: false,
      ratingsLoaded: false,
      profileLoaded: false,
      commentsLoaded: false,
      previewBackground: false,
      tab: props.query.page !== undefined ? 0 : 1,
    }
    this.onTextChange = this.onTextChange.bind(this)
  }
  clickBg(e) {
    if (e.target.id == "forum" || e.target.parentNode.id == "forum" || e.originalTarget == document.body || e.originalTarget == document.body.parentNode) {
      this.setState({previewBackground: !this.state.previewBackground})
    } else if (this.state.previewBackground) {
      this.setState({previewBackground: false})
    }
  }
  componentWillUnmount() {
    document.body.style.backgroundImage = ""
    document.body.classList.remove("custom-bg")
    document.removeEventListener('click', this.clickBg)
  }
  componentDidMount() {
    document.addEventListener('click', this.clickBg)
    this.setState({text: localStorage.getItem("kopunch_visitor_message_" + this.props.query.u) || "", commentsLoaded: false})
    window.fetch(this.props.appState.api + "/user/" + this.query.u, {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.data = result
      this.joindate = new Date(this.data.createdAt)
      this.setState({loaded: true})
    })
    window.fetch(this.props.appState.api + "/user/" + this.query.u + "/topRatings", {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.ratings = result;
      this.setState({ratingsLoaded: true});
    })
    window.fetch(`${this.props.appState.api}/v2/users/${this.query.u}/profile`, {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.profile = result;

      if (this.profile.background.url)
      {
        let bgurl = ""
        try {
          bgurl = new URL(this.profile.background.url).href
        } catch (e) {
          try {
            bgurl = new URL("https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/" + this.profile.background.url).href
          } catch (f) {

          }
        }
        console.log("bgurl: ", bgurl)
        document.body.style.backgroundImage = bgurl ? `url("${bgurl}")` : ""
        if (this.profile.backgroundType == "tiled")
          document.body.classList.add("custom-tiled")
        if (bgurl) 
          document.body.classList.add("custom-bg")
      }

      this.setState({profileLoaded: true})
    })
    window.fetch(`${this.props.appState.api}/v2/users/${this.query.u}/profile/comments?page=${this.query.page ?? 1}`, {credentials: 'include'}).then(res => res.json()).then(r=>{
      this.comments = r.comments
      this.setState({commentsLoaded: true, totalComments: r.totalComments, commentPage: r.page})
    })
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    if (prevstate.tab != this.state.tab && this.state.tab == 2 && !this.state.bansLoaded) {
      window.fetch(this.props.appState.api + "/user/" + this.query.u + "/bans", {credentials: 'include'}).then(res => res.json()).then((result)=>{
        console.log(result)
        this.bans = result
        this.setState({bansLoaded: true})
      })
    }
    if (Number(this.query.u) == this.props.appState.userid && this.state.commentsLoaded && this.state.tab == 0 && (!prevstate.commentLoaded || prevstate.tab !== 0)) {
      console.log("componentDidUpdate checking notifications")
      let ids = []
      for (const n of this.props.appState.notifications) {
        if (!n.read && n.type === "PROFILE_COMMENT") {
          for (const m of this.comments) {
            if (n.data.id === m.id) {
              ids.push(n.id)
            }
          }
        }
      }
      console.log("notification ids", ids)
      if (ids.length > 0) {
        markNotificationsAsRead(this.props, ids)
      }
    }
  }
  ignore(e) {
    e.preventDefault()
    let list = Object.assign({}, this.props.appState.ignorelist)
    list[this.data.id] = !list[this.data.id]
    this.props.appSetState({ignorelist: list})
  }
  onTextChange(text) {
    console.log("onTextChange")
    localStorage.setItem("kopunch_visitor_message_" + this.props.query.u, text)
    this.setState({text: text})
  }
  render() {
    let lastTime
    
    return (
      <div id="forum">
        <Header title={this.state.loaded && this.data.username} appState={this.props.appState} appSetState={this.props.appSetState}/>
        {this.state.loaded && <div className="content-inner" style={{opacity: this.state.previewBackground ? 0.5 : 1}}>
          <div className="profile-header">
            <img src={this.state.profileLoaded && this.profile.header ? `https://cdn.knockout.chat/image/${this.profile.header}` : "https://knockout.chat/static/profile_header_dark.png"}/>
          </div>
          <div className="member-summary small">
            <Username user={this.data}/>
            <br/>
            {getUserTitleOrGroupName(this.data)}
            <div className="member-avatar">
              {!this.data.isBanned &&
                <picture className="user-avatar" alt={this.data.username + "'s avatar"} title={this.data.username + "'s avatar"}>
                  <source srcSet={"https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/" + this.data.avatarUrl} type="image/webp"/>
                  <img srcSet="/static/defaultavatar.png"/>
                </picture>
              }
              {this.data.isBanned && 
                <div className="user-avatar banned">
                  BAN
                </div>
              }
            </div>
            <SocialIcons api={this.props.appState.api} id={this.data.id}/>
            <ul className="member-menu">
              {this.props.appState.authenticated && <li><LinkWrapper href={"/usercp.php?do=newmessage&userid=" + this.data.id}><img className="icon sendpm" src="/static/silkicons/email.png"/>Send Private Message</LinkWrapper></li>}
              {this.state.profileLoaded && this.profile.social.website && <li><LinkWrapper href={this.profile.social.website}><img className="icon homepage" src="/static/silkicons/cursor.png"/>Home Page</LinkWrapper></li>}
              <li><LinkWrapper href={"/search.php?do=finduser&userid=" + this.data.id + "&contenttype=post"}><img className="icon findposts" src="/static/silkicons/comments.png"/>Find latest posts</LinkWrapper></li>
              <li><LinkWrapper href={"/search.php?do=finduser&userid=" + this.data.id + "&contenttype=thread"}><img className="icon findthreads" src="/static/silkicons/comments.png"/>Find latest started threads</LinkWrapper></li>
              <li><LinkWrapper href={"/fp_images.php?u=" + this.data.id}><img className="icon findimages" src="/static/silkicons/pictures.png"/>View Images Posted</LinkWrapper></li>
              {this.props.appState.authenticated && !this.props.appState.ignorelist[this.data.id] && <li><LinkWrapper href="#" onClick={this.ignore}><img className="icon ignore" src="/static/silkicons/cross.png"/>Add user to ignore list</LinkWrapper></li>}
              {this.props.appState.authenticated && this.props.appState.ignorelist[this.data.id] && <li><LinkWrapper href="#" onClick={this.ignore}><img className="icon ignore" src="/static/silkicons/tick.png"/>Remove user from ignore list</LinkWrapper></li>}
            </ul>
            <hr/>
            <div className="member-stats">
              <dl>
                <dt>Join Date:</dt>
                <dd>{this.joindate.getDate() + " " + months[this.joindate.getMonth()] + " " + this.joindate.getFullYear()}</dd>
              </dl>
              <dl>
                <dt>Last Activity:</dt>
                <dd>???</dd>
              </dl>
              <dl>
                <dt>Total Posts:</dt>
                <dd>{this.data.posts}</dd>
              </dl>
            </div>
            <br/>
            <div className="member-ratings panel">
              {this.state.ratingsLoaded && this.ratings.sort((a, b)=>b.count - a.count).map((o)=>{
                return <div className="rating">
                  <img className="icon" src={ratings[o.name].url} title={ratings[o.name].name}/>
                  &nbsp;{ratings[o.name].name + " x "}
                  <b>{o.count}</b>
                </div>
              })}
            </div>
          </div>
          <div className="member-tabs">
            <div className="tabs">
              {tabs.map((v, k)=>{
                return <div className={"tab" + (this.state.tab == k ? " selected" : "")} key={v}>
                  <LinkWrapper href="" onClick={(e)=>{
                    e.preventDefault()
                    this.setState({tab: k})
                  }}>{v}</LinkWrapper>
                </div>
              })}
            </div>
            {this.state.tab == 0 && 
              <div className="member-content messages">
                <Pagination page={this.props.query.page ?? 1} query={this.props.query} location={this.props.location} totalItems={this.state.totalComments} itemsPerPage={10}/>
                {this.state.commentsLoaded && this.comments.map((m, i)=>{
                  console.log("profile comment", m)
                  
                  return <Post
                    key={m.id}
                    data={m}
                    number={m.id}
                    threadRead={true}
                    noReport
                    noRatings
                    noButtons
                    customButtons={Number(this.query.u) === this.props.appState.userid ? [{
                      src: "/static/xbutton.png",
                      title: "Delete Comment",
                      onClick: (e)=>{
                        e.preventDefault()
                        if (window.confirm("Are you sure you want to delete this visitor message?")) {
                          window.fetch(`${this.props.appState.api}/v2/users/${this.query.u}/profile/comments/${m.id}`, {
                            credentials: 'include',
                            method: 'DELETE',
                          }).then(r=>{
                            if (r.message) {
                              console.log(r.message)
                            } else {
                              this.props.history.push(`/member.php?u=${this.query.u}&page=${this.query.page ?? 1}`)
                            }
                          })
                        }
                      }
                    }] : undefined}
                    permalink={`member.php?u=${m.userProfile}#post-${m.id}`}
                    appState={this.props.appState} 
                    appSetState={this.props.appSetState}
                  />
                })}
                <div className="quick-reply">
                  <PostEditor 
                    key="ConversationReplyEditor"
                    text={this.state.text}
                    onChange={this.onTextChange}
                    appState={this.props.appState}
                    appSetStattEditor
                    e={this.props.appSetState}
                    doingReply={this.state.doingReply}
                    error={this.state.postError}
                    onSubmit={text=>{
                      this.setState({doingReply: true})
                      window.fetch(`${this.props.appState.api}/v2/users/${this.query.u}/profile/comments`, {
                        "credentials": "include",
                        "headers": {
                          "content-format-version": "1",
                          "Content-Type": "application/json;charset=utf-8",
                        },
                        "body": JSON.stringify({
                          content: text,
                          appName: "kopunch",
                        }),
                        "method": "POST",
                      }).then(r=>r.json()).then(r=>{
                        if ((r.error || r.errors)) {
                          this.setState({postError: (r.error || r.errors), doingReply: false})
                        } else {
                          this.setState({doingReply: false})
                          this.onTextChange("")
                          this.props.history.push(`member.php?u=${this.props.query.u}#post-${r.id}`)
                        }
                      })
                    }}
                  />
                </div>
                <Pagination page={this.props.query.page ?? 1} query={this.props.query} location={this.props.location} totalItems={this.state.totalComments} itemsPerPage={10} />
              </div>
            }
            {this.state.tab == 1 && 
              <div className="member-content about">
                <h4 className="blue-header">Basic Information</h4>
                <h5>About {this.data.username}</h5>
                {this.state.profileLoaded && <div className="about-me">
                  {this.profile.headingText}
                  {this.profile.bio}
                </div>}
                <h4 className="blue-header">Statistics</h4>
                <h5>Total Posts</h5>
                <dl>
                  <dt>Total Posts:</dt>
                  <dd>{this.data.posts}</dd>
                </dl>
                <dl>
                  <dt>Posts Per Day:</dt>
                  <dd>{(this.data.posts / ((new Date() - this.joindate) / (1000 * 60 * 60 * 24))).toFixed(2)}</dd>
                </dl>
                <h5>Visitor Messages</h5>
                <dl>
                  <dt>Total Messages:</dt>
                  <dd>{this.state.totalComments}</dd>
                </dl>
                <dl>
                  <dt>Most Recent Message:</dt>
                  <dd>{this.state.commentsLoaded ? <TimeElapsed date={this.comments[0] && this.comments[0].createdAt}/> : "Loading..."}</dd>
                </dl>
                <dl>
                  <LinkWrapper href="" onClick={(e) => { e.preventDefault(); this.setState({ tab: 0 }) }} style={{ marginLeft: "14px" }}>Visitor Messages for {this.data.username}</LinkWrapper>
                </dl>
                <h5>General Information</h5>
                <dl>
                  <dt>Last Activity:</dt>
                  <dd>???</dd>
                </dl>
                <dl>
                  <dt>Join Date:</dt>
                  <dd>{this.joindate.getDate() + " " + months[this.joindate.getMonth()] + " " + this.joindate.getFullYear()}</dd>
                </dl>
              </div>
            }
            {this.state.tab == 2 && 
              <div className="member-content events">
                {!this.state.bansLoaded && "Loading..."}
                {this.state.bansLoaded && this.bans.length &&
                  this.bans.map((v, k)=>{
                    let changed = lastTime != elapsedString(v.created_at || v.createdAt)
                    if (changed)
                      lastTime = elapsedString(v.created_at || v.createdAt)
                    return <BanEntry key={v.id} data={v} username={this.data.username} showTime={changed}/>
                  }) || "Nothing to show here!"
                }
              </div>
            }
          </div>
        </div>}
        <Footer/>
      </div>
    );
  }
}
