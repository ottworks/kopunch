import React, { Component, PureComponent } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed from '../TimeElapsed'
import ThreadIcon from '../ThreadIcon'
import Pagination from '../Pagination'
import LinkWrapper from './../LinkWrapper'
import '../../stylesheets/forumdisplay.css'
import { ratings } from '../Rating'
import { getUser } from "../api/user"
import { getSubforum } from "../api/subforum"

const THREADS_PER_PAGE = 40
const POSTS_PER_PAGE = 20

export class ThreadEntry extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      hidden: false,
      hasError: undefined,
      user: undefined,
      subforum: undefined,
    }
  }
  static getDerivedStateFromError(err) {
    return {hasError: err}
  }
  componentDidMount() {
    if (typeof this.props.data?.user === "number") {
      getUser(this.props.appState.api, this.props.data?.user, (r)=>this.setState({user: r}))
    }
    console.log("ThreadEntry did mount", this.props.data, this.props.data.subforumId)
    if (this.props.withSubforum && this.props.data.subforumId) {
      getSubforum(this.props.appState.api, this.props.data.subforumId, (r)=>{console.log("subforum", r); this.setState({subforum: r})})
    }
  }
  render() {
    if (this.state.hidden) {
      return null
    }

    if (this.state.hasError) {
      return <tr className={"thread-entry" + (this.props.data.locked ? " locked" : "") + (this.props.data.pinned ? " pinned" : "") + (read ? " read" : "") + (this.props.data.new ? " new" : "")} id={"thread-" + id}>
        <td className="thread-icon">
          <ThreadIcon id={0}/>
        </td>
        <td className="thread-info">
          <LinkWrapper className="thread-title">{this.state.hasError}</LinkWrapper>
          <LinkWrapper className="images-link" href={"/fp_images.php?t=" + id}>
            <img className="icon" src="/static/silkicons/bullet_picture.png"/>
          </LinkWrapper>
          <div className="thread-username small">
          </div>
        </td>
        <td className="thread-lastpost small">
        </td>
        <td className="thread-replies">
        </td>
        <td className="thread-views">
          ????
        </td>
      </tr>
    }

    let pages = Math.floor(((this.props.data.postCount || this.props.data.threadPostCount) - 1) / POSTS_PER_PAGE) + 1
    let pagelinks = []
    if (pages <= 8) {
      for (let i = 2; i <= pages; i++) {
        pagelinks.push(i)
      }
    } else {
      pagelinks = [2, 3, 4, 5, ".."]
      for (let i = pages - 3; i <= pages; i++)
      {
        pagelinks.push(i)
      }
    }

    let user = this.state.user || this.props.data.user
    let unreadposts = this.props.data.readThreadUnreadPosts || this.props.data.unreadPosts || this.props.data.unreadPostCount || undefined
    let username = this.props.username || this.props.data.username || this.props.data.threadUsername || (user && user.username) || undefined
    let userid = this.props.userid || this.props.data.threadUser || (user && user.id) || undefined
    let updated = (this.props.data.lastPost && this.props.data.lastPost.createdAt) || this.props.data.threadUpdatedAt || this.props.data.updatedAt || this.props.data.updated_at || undefined
    let id = this.props.data.thread_id || this.props.data.threadId || this.props.data.id || undefined
    let posts = this.props.data.postCount || this.props.data.threadPostCount
    let title = this.props.data.title || this.props.data.threadTitle
    let unreadpage = Math.floor((posts - unreadposts) / POSTS_PER_PAGE) + 1
    let subscribed = this.props.subscribed
    let read = this.props.data.hasRead || this.props.data.hasSeenNoNewPosts || (this.props.data.unreadPosts === 0) || undefined


    let bottomline = [<LinkWrapper href={"/member.php?u=" + userid}>{username}</LinkWrapper>]

    if (this.props.withSubforum && this.state.subforum) {
      bottomline.push(<span className="dot"> • </span>)
      bottomline.push(<span className="viewers"><LinkWrapper href={"/forumdisplay.php?f=" + this.state.subforum.id}>{this.state.subforum.name}</LinkWrapper></span>)
    }

    if (subscribed) {
      bottomline.push(<span className="dot"> • </span>)
      bottomline.push(<LinkWrapper href="" onClick={(e)=>{
        e.preventDefault()
        window.fetch(this.props.appState.api + "/alerts", {
          method: 'DELETE', 
          credentials: 'include',
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            "content-format-version": "1",
          },
          body: JSON.stringify({
            threadId: id,
          }),
        }).then(r=>r.json()).then((r)=>{
          console.log(r)
          this.setState({hidden: true})
        })
      }}>Unsubscribe</LinkWrapper>)
    } else if (read || unreadposts) {
      bottomline.push(<span className="dot"> • </span>)
      bottomline.push(<LinkWrapper href="" onClick={(e)=>{
        e.preventDefault()
        window.fetch(this.props.appState.api + "/v2/read-threads/" + id, {
          method: 'DELETE',
          credentials: 'include',
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            "content-format-version": "1",
          },
          mode: "cors"
        }).then(r=>r.json()).then((r)=>{
          console.log(r)
          this.setState({hidden: true})
        })
      }}>Mark Unread</LinkWrapper>)
    }

    if (this.props.data.recentPostCount) {
      bottomline.push(<span className="dot"> • </span>)
      bottomline.push(<span className="viewers">{this.props.data.recentPostCount} recent posts</span>)
    }

    let tags = []
    if (this.props.data.tags) {
      let had_tag = this.props.tagFilter ? false : true
      let is_nsfw = false
      //console.log("tagfilter: ", this.props.tagFilter)
      this.props.data.tags.map(obj=>Object.keys(obj).map(key=>{
        if (obj[key] == this.props.tagFilter) {
          had_tag = true
        }
        if (obj[key] == "NSFW") {
          is_nsfw = true
        }
        tags.push([<span className="dot" key={obj[key] + id + "dot"}> • </span>, <span className={"thread-tag" + (obj[key] == this.props.tagFilter ? " active" : "")} key={obj[key] + id} onClick={()=>{
          if (this.props.tagFilter == obj[key]) {
            this.props.setTagFilter("")
          } else {
            this.props.setTagFilter(obj[key])
          }
        }}>{obj[key]}</span>])
      }))
      if (!had_tag) {
        return null
      }
      if (is_nsfw && !this.props.appState.nsfw) {
        return null
      }
    } else if (this.props.tagFilter) {
      return null
    }

    bottomline = bottomline.concat(tags)

    return <tr className={"thread-entry" + (this.props.data.locked ? " locked" : "") + (this.props.data.pinned ? " pinned" : "") + (read ? " read" : "") + (this.props.data.new ? " new" : "")} id={"thread-" + id}>
      {this.props.number &&
        <td className="number">
          {this.props.number}
        </td>
      }
      <td className="thread-icon">
        <ThreadIcon id={this.props.data.icon_id || this.props.data.iconId || this.props.data.threadIcon || 0}/>
      </td>
      <td className="thread-info">
        {this.props.data.firstPostTopRating && <div className="thread-rating rating">
          <LinkWrapper href="" title="Rate this post" onClick={(e)=>{
            e.preventDefault()
            window.fetch(this.props.appState.api + "/thread/" + id, {
              credentials: 'include',
            }).then(r=>r.json()).then(r=>{
              let post = r.posts[0].id
              window.fetch(this.props.appState.api + "/v2/posts/" + post + "/ratings", {
                method: 'PUT', 
                credentials: 'include',
                headers: {
                  "Content-Type": "application/json;charset=utf-8",
                  "content-format-version": "1",
                },
                body: JSON.stringify({
                  rating: this.props.data.firstPostTopRating.rating,
                }),
              }).then((result)=>{
                if (result.ok) {
                  window.fetch(this.props.appState.api + "/v2/posts/" + post, {
                    method: 'GET',
                    credentials: 'include',
                  }).then(r=>r.json()).then((r)=>{
                    //this.setState({ratings: r.ratings})
                    for (const rating of r.ratings) {
                      if (rating.rating === this.props.data.firstPostTopRating.rating) {
                        this.props.data.firstPostTopRating.count = rating.count
                        this.forceUpdate()
                        break;
                      }
                    }
                  })
                }
              })
            })
          }}>
            <img src={ratings[this.props.data.firstPostTopRating.rating] && ratings[this.props.data.firstPostTopRating.rating].url} className="icon" title={ratings[this.props.data.firstPostTopRating.rating] && ratings[this.props.data.firstPostTopRating.rating].name}/> x <strong>{this.props.data.firstPostTopRating.count}</strong>
          </LinkWrapper>
        </div>}
        <LinkWrapper className="thread-title" href={"/showthread.php?t=" + id} title="insert post preview here">{title}</LinkWrapper>
        {pages > 1 && <span className="thread-pagelinks small"> ({
          pagelinks.map((n)=>{
            return <LinkWrapper key={n} href={n == ".." ? null : "/showthread.php?t=" + id + "&page=" + n}>{n != pages ? n : n + " Last"}</LinkWrapper>
          })
        })</span>}
        <LinkWrapper className="images-link" href={"/fp_images.php?t=" + id}>
          <img className="icon" src="/static/silkicons/bullet_picture.png"/>
        </LinkWrapper>
        {unreadposts && <span className="unread">
          <LinkWrapper href={"/showthread.php?t=" + id + "&page=" + unreadpage + "&p=" + this.props.data.firstUnreadId}>
            <img className="icon" src="/static/silkicons/accept.png"/>
            {unreadposts} new post{unreadposts == 1 ? "" : "s"}
          </LinkWrapper>
        </span>}
        <div className="thread-username small">
          {bottomline}
        </div>
      </td>
      <td className="thread-lastpost small">
        <div>
          <TimeElapsed date={updated}/>
          <br/>
          by <LinkWrapper href={"/member.php?u=" + ((this.props.data.lastPost && this.props.data.lastPost.user) ? this.props.data.lastPost.user.id : 0)}>{(this.props.data.lastPost && this.props.data.lastPost.user) ? this.props.data.lastPost.user.username : "Unknown"}</LinkWrapper>&nbsp;
          <LinkWrapper className="goto-post-arrow" href={"/showthread.php?t=" + id + "&page=" + (this.props.data.lastPost && this.props.data.lastPost.page || 1) + "&p=" + (this.props.data.lastPost && this.props.data.lastPost.id)} title="Go to last post">&#9658;</LinkWrapper>
        </div>
      </td>
      <td className="thread-replies">
        {posts}
      </td>
      <td className="thread-views">
        {this.props.data.viewers ? ((this.props.data.viewers.memberCount || 0) + (this.props.data.viewers.guestCount || 0)) : this.props.data.recentPostCount}
      </td>
    </tr>
  }
}


export default class ForumDisplay extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.loadSubforum = this.loadSubforum.bind(this)
    this.state = {
      loaded: false,
      tagFilter: "",
    }
  }
  loadSubforum() {
    let page = this.query.page
    if (!page)
      page = ""
    window.fetch(this.props.appState.api + "/subforum/" + this.query.f + "/" + page, {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.setState({data: result})
      this.setState({loaded: true})
    })
  }
  componentDidMount() {
    this.loadSubforum()
  }
  componentDidUpdate(prevprops) {
  }
  render() {
    return (
      <div id="forum">
        <Header title={this.state.loaded && this.state.data.name} appState={this.props.appState} appSetState={this.props.appSetState}/>
        {this.state.loaded && <div className="content-inner">
          <div className="forum-above">
            <LinkWrapper className="new-thread" href={"/newthread.php?f=" + this.state.data.id}>New Thread</LinkWrapper>
            <Pagination page={this.state.data.currentPage} totalItems={this.state.data.totalThreads} itemsPerPage={THREADS_PER_PAGE} location={this.props.location} query={this.props.query}/>
          </div>
          <table>
            <thead>
              <tr className="small">
                <td></td>
                <td>Title / Thread Starter</td>
                <td>Last Post By&#9660;</td>
                <td>Replies</td>
                <td>Viewers</td>
              </tr>
            </thead>
            <tbody>
              {this.state.data.threads.map((data, index)=>{return <ThreadEntry key={data.id} data={data} tagFilter={this.state.tagFilter} setTagFilter={(tag)=>{this.setState({tagFilter: tag})}} appState={this.props.appState} appSetState={this.props.appSetState}/>})}
            </tbody>
          </table>
          <Pagination page={this.state.data.currentPage} totalItems={this.state.data.totalThreads} itemsPerPage={THREADS_PER_PAGE} location={this.props.location} query={this.props.query}/>
        </div>}
        <Footer/>
      </div>
    );
  }
}
