import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed from '../TimeElapsed'
import Pagination from '../Pagination'
import LinkWrapper from './../LinkWrapper'
import ImgWrapper from './../ImgWrapper'
import '../../stylesheets/fp_images.css'

const POSTS_PER_PAGE = 20

const matchimg = /\[img[^\]]*?\][^\[]+/g

export default class Images extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.state = {
      loaded: false,
    }
    console.log("Images constructor")
  }
  componentDidMount() {
    console.log("Images mount")
    let page = this.query.page
    if (!page)
      page = 1
    let endpoint = "/thread/" + this.query.t + "/"
    if (this.query.u)
      endpoint = "/user/" + this.query.u + "/posts/"
    window.fetch(this.props.appState.api + endpoint + page, {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.data = result
      console.log("posts", this.data.posts)
      this.matches = []
      this.threadids = []
      for (let i = 0; i < this.data.posts.length; i++) {
        this.matches[i] = this.data.posts[i].content.match(matchimg)
        if (!this.matches[i])
          continue
        for (let j = 0; j < this.matches[i].length; j++) {
          this.matches[i][j] = this.matches[i][j].substring(this.matches[i][j].indexOf("]") + 1)
        }
      }
      this.setState({loaded: true})
      console.log("MATCHES", this.matches)
    })
  }
  render() {
    let page = this.query.page
    if (!page)
      page = 1
    return (
      <div id="forum">
        <Header title="Posted Images" subforum={this.state.loaded && this.data.subforumName} subforumid={this.state.loaded && this.data.subforumId} appState={this.props.appState} appSetState={this.props.appSetState}/>
        {this.state.loaded && <div className="content-inner">
          <Pagination page={this.data.currentPage} totalItems={this.data.totalPosts} itemsPerPage={this.query.u ? 40 : POSTS_PER_PAGE} location={this.props.location} query={this.props.query}/>
          {this.matches && this.matches.map((m, i)=>{
            console.log("match", m)
            return m && m.map((img, j)=>{
              let thread 
              return <div className="image-entry">
                <LinkWrapper href={"/showthread.php?t=" + (this.query.t || this.data.posts[i].thread.id) + "&page=" + (this.data.posts[i].page || 1) + "&p=" + this.data.posts[i].id}>
                  <ImgWrapper src={img}/>
                </LinkWrapper>
              </div>
            })
          })}
          <Pagination page={this.data.currentPage} totalItems={this.data.totalPosts} itemsPerPage={this.query.u ? 40 : POSTS_PER_PAGE} location={this.props.location} query={this.props.query}/>
        </div>}
        <Footer/>
      </div>
    );
  }
}
