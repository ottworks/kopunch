import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed from '../TimeElapsed'
import Pagination from '../Pagination'
import Post from '../Post'
import LinkWrapper from './../LinkWrapper'
import { ThreadEntry } from './forumdisplay'
import '../../stylesheets/search.css'

const linkmatch = /knockout.chat\/thread\/(\d+)\/(\d+)/g

const POSTS_PER_PAGE = 40

export default class Search extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.state = {
      loaded: false,
      tagFilter: "",
      customSearch: false,
      username: "User",
    }
    this.ref = React.createRef()
    this.didchange = 0
    console.log("Search constructor")
  }
  clickBg(e) {
    if (e.target.id == "forum" || e.originalTarget == document.body || e.originalTarget == document.body.parentNode) {
      this.setState({previewBackground: !this.state.previewBackground})
    } else if (this.state.previewBackground) {
      this.setState({previewBackground: false})
    }
  }
  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect()
    }
  }
  componentDidMount() {
    console.log("Search mount")

    let page = this.query.page
    if (!page)
      page = 1

    if (this.query.userid) {
      let contenttype = "posts"
      if (this.query.contenttype == "thread")
        contenttype = "threads"
      window.fetch(this.props.appState.api + "/user/" + this.query.userid, {credentials: 'include'}).then(r=>r.json()).then(r=>{
        this.setState({username: r.username})
      })
      window.fetch(this.props.appState.api + "/user/" + this.query.userid + "/" + contenttype + "/" + page, {credentials: 'include'}).then(res => res.json()).then((result)=>{
        console.log(result)
        this.data = result

        this.setState({loaded: true})
        console.log("posts", this.data.posts)
      })
    } else {
      this.data = {}
      this.setState({loaded: true, customSearch: true})
      if (this.ref && this.ref.current) {
        console.log("adding observer to", this.ref.current)
        if (this.observer) {
          this.observer.disconnect()
        }
        this.observer = new MutationObserver(()=>{
          if (this.didchange === 0) {
            console.log("doing find and replace")
            let plusone = this.didchange + 1
            this.didchange = plusone
            //this.ref.current.innerHTML = this.ref.current.innerHTML.replace(linkmatch, 'kopunch.club/showthread.php?t=$1&page=$2')

            window.setTimeout(()=>{
              if (this.didchange === plusone) {
                this.didchange = 0
                console.log("cleared")
                let walker = document.createTreeWalker(this.ref.current, NodeFilter.SHOW_ALL, {acceptNode: (node)=>{
                  if (node.attributes) {
                    for (let i = 0; i < node.attributes.length; ++i) {
                      if (node.attributes[i].value.includes("knockout.chat")) {
                        return NodeFilter.FILTER_ACCEPT
                      }
                    }
                  }
                  if (node.data && node.data.includes && node.data.includes("knockout.chat")) {
                    return NodeFilter.FILTER_ACCEPT
                  }
                  return NodeFilter.FILTER_SKIP
                }})
                let current = walker.currentNode

                while (current) {
                  //console.log("current:", current)
                  //current.outerHTML = current.outerHTML.replace(linkmatch, 'kopunch.club/showthread.php?t=$1&page=$2')

                  if (current.attributes) {
                    for (let i = 0; i < current.attributes.length; ++i) {
                      if (current.attributes[i].value.includes("knockout.chat")) {
                        current.attributes[i].value = current.attributes[i].value.replace(linkmatch, 'kopunch.club/showthread.php?t=$1&page=$2')
                      }
                    }
                  }
                  if (current.data && current.data.includes && current.data.includes("knockout.chat")) {
                    current.data = current.data.replace(linkmatch, 'kopunch.club/showthread.php?t=$1&page=$2')
                  }
                  current = walker.nextNode()
                }
              }
            }, 0)
          }
        })
        this.observer.observe(this.ref.current, {childList: true, subtree: true})
      }
    }
    
    if (!window.twttr) {
      window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));
    }
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {

  }
  render() {
    let page = this.query.page
    if (!page)
      page = 1
    let own = this.state.loaded && this.props.appState.userid == this.data.userId
    return (
      <div id="forum">
        <Header title={"Search"} subforum={this.state.loaded && this.data.subforumName} subforumid={this.state.loaded && this.data.subforumId} appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner" style={{opacity: this.state.previewBackground ? 0.5 : 1}}>
          {this.state.loaded && <div>
            {!this.state.customSearch && <Pagination page={this.data.currentPage} totalItems={this.data.totalPosts || this.data.totalThreads} itemsPerPage={POSTS_PER_PAGE} location={this.props.location} query={this.props.query}/>}
            {this.data.posts && 
              <ol className="post-list" start={(page - 1) * POSTS_PER_PAGE + 1}>
                {this.data.posts.map((data, index)=>{return <Post key={data.id} data={data} number={(page - 1) * POSTS_PER_PAGE + 1 + index} id={data.id} thread={data.thread.id} page={data.page} appState={this.props.appState} appSetState={this.props.appSetState}/>})}
              </ol>
            }
            {this.data.threads && 
              <table>
                <tbody>
                  {this.state.loaded && this.data.threads.map((data, index)=>{
                    return <ThreadEntry key={data.id} data={data} username={this.state.username} userid={this.query.userid} tagFilter={this.state.tagFilter} setTagFilter={(tag)=>{this.setState({tagFilter: tag})}} appState={this.props.appState} appSetState={this.props.appSetState}/>
                  })}
                </tbody>
              </table>
            }
            {this.state.customSearch && <div dangerouslySetInnerHTML={{__html: `<img src onerror="var script = document.createElement('script');script.src = 'https://cse.google.com/cse.js?cx=005675489929610452028:k9jkao3p-fy&q=comic+con&sort=date-sdate';document.body.appendChild(script);"/>`}}/>}
            {!this.state.customSearch && <Pagination page={this.data.currentPage} totalItems={this.data.totalPosts || this.data.totalThreads} itemsPerPage={POSTS_PER_PAGE} location={this.props.location} query={this.props.query}/>}
          </div>}
          <div ref={this.ref}><div className="gcse-search"/></div>
        </div>
        <Footer/>
      </div>
    );
  }
}