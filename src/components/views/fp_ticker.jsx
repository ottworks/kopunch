import React, { Component } from "react"
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed, { elapsedString } from '../TimeElapsed'
import '../../stylesheets/fp_events.css'
import { socketClient } from "../ViewerCount"
import EventEntry from "../EventEntry"

export default class Ticker extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      loaded: false,
      events: [],
    }
    this.lastTime = ""
  }
  componentDidMount() {
    socketClient.emit('events:join')
    socketClient.on('events:new', (event) => {
      console.log("got event", event)
      let events = this.state.events.slice()
      event.alt = !events[0].alt
      events.unshift(event)
      if (events.length > 300) {
        events.length = 300
      }
      this.setState({events: events})
    })
    window.fetch(this.props.appState.api + "/v2/events", {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      if (result.length) {
        for (let i = 0; i < result.length; i += 2) {
          result[i].alt = true
        }
      }
      
      this.setState({loaded: true, events: result})
    })
  }
  componentWillUnmount() {
    socketClient.emit('events:leave')
    socketClient.off('events:new')
  }
  render() {
    return (
      <div id="forum">
        <Header title="The Ticker" appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table className="ticker-table">
            <tbody>
              {this.state.loaded && (this.state.events.message || this.state.events.map((data, index) => {
                return <tr key={data.id} className={data.alt ? "alt event-entry" : "event-entry"}>
                  <td className="ticker-time">
                    {new Date(data.createdAt).toLocaleTimeString([], { hour: 'numeric', minute: '2-digit' }).replace(' ', '')}
                  </td>
                  <EventEntry key={data.id} data={data} showTime={false} td />
                </tr>
              }))}
            </tbody>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}