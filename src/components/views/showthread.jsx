import React, { Component } from "react"
import { Helmet } from "react-helmet"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed from '../TimeElapsed'
import Pagination from '../Pagination'
import Post, { emotify } from '../Post'
import PostEditor, { bbc } from '../PostEditor'
import { vegitate, getbbc } from '../PostEditor'
import { stripQuotes } from './newreply'
import LinkWrapper from './../LinkWrapper'
import ViewerCount from './../ViewerCount'
import '../../stylesheets/showthread.css'
import '../../stylesheets/newreply.css'
import { markNotificationsAsRead } from "../api/notifications"

export const POSTS_PER_PAGE = 20

export default class ShowThread extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.query = this.props.query
    this.pruneMentions = this.pruneMentions.bind(this)
    this.clickBg = this.clickBg.bind(this)
    this.loadThread = this.loadThread.bind(this)
    this.onTextChange = this.onTextChange.bind(this)
    this.state = {
      loaded: false,
      text: localStorage.getItem("kopunch_thread_reply_" + this.query.t) || "",
      renameMenu: false,
      renameText: "",
      background: "",
      error: "",
      previewBackground: false,
      data: undefined,
      editorIndex: -1,
    }
    console.log("showthread constructor")
  }
  clickBg(e) {
    if (e.target.id == "forum" || e.originalTarget == document.body || e.originalTarget == document.body.parentNode) {
      this.setState({previewBackground: !this.state.previewBackground})
    } else if (this.state.previewBackground) {
      this.setState({previewBackground: false})
    }
  }
  componentWillUnmount() {
    document.body.style.backgroundImage = ""
    document.body.classList.remove("custom-bg")
    document.removeEventListener('click', this.clickBg)
  }
  loadThread() {
    let page = this.query.page
    if (!page)
      page = 1
    window.fetch(this.props.appState.api + "/v2/threads/" + this.query.t + "/" + page, {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log("LOADTHREAD:", result)
      if (result.name) {
        this.setState({ notfound: result.name })
        return
      }
      this.setState({data: result})

      if (this.props.query.do === "subscribe") {
        this.doSubscribe()
      } else if (this.props.query.do === "unsubscribe") {
        this.doSubscribe(true)
      } else if (this.props.appState.autosub === "read" && !result.subscriptionLastPostNumber) {
        this.doSubscribe()
      }

      let bgurl = ""
      try {
        bgurl = new URL(this.state.data.threadBackgroundUrl).href
      } catch (e) {

      }
      document.body.style.backgroundImage = bgurl ? `url("${bgurl}")` : ""
      if (this.state.data.threadBackgroundType == "tiled")
        document.body.classList.add("custom-tiled")
      if (bgurl) 
        document.body.classList.add("custom-bg")

      if (this.state.data.posts)
        this.setState({loaded: true, renameText: this.state.data.title})
      else {
        this.setState({notfound: true})
        return
      }
      console.log("posts", this.state.data.posts)
      this.pruneMentions()
      let d1 = new Date(result.readThreadLastSeen)
      let d2 = new Date(this.state.data.posts.length != 0 && this.state.data.posts[this.state.data.posts.length - 1].createdAt)
      let d3 = new Date(result.subscriptionLastSeen)
      let lastpost = this.state.data.posts.length !== 0 && this.state.data.posts[this.state.data.posts.length - 1].threadPostNumber
      let sublastpost = this.state.data.subscriptionLastPostNumber
      console.log(d1, d2, d3, d1 < d2)

      let unreadposts = this.state.data.postCount - lastpost
      let readthreads = this.props.appState.readthreads.slice()
      console.log("unreadposts", unreadposts, "rewadthreads", readthreads)
      let index = readthreads.findIndex(({threadId})=>threadId === this.state.data.id)
      if (readthreads[index]) {
        console.log(readthreads[index].unreadPosts, unreadposts)
        readthreads[index].unreadPosts = Math.min(readthreads[index].unreadPosts, unreadposts)
      }
      this.props.appSetState({ readthreads: readthreads })
      if (isNaN(d1.getTime()) || (d2 && d1 < d2) || (d2 && d3 < d2) || sublastpost < lastpost) {
        console.log("reading")
        window.fetch(this.props.appState.api + "/v2/read-threads", {
          credentials: 'include',
          headers: {
            "Content-Type": "application/json;charset=utf-8",
            "content-format-version": "1",
          },
          body: JSON.stringify({
            lastPostNumber: this.state.data.posts.length != 0 && this.state.data.posts[this.state.data.posts.length - 1].threadPostNumber,
            threadId: this.state.data.id,
          }),
          method: "POST",
          mode: "cors"
        })
        if (this.state.data.subscribed) {
          window.fetch(this.props.appState.api + "/alerts", {
            credentials: 'include',
            headers: {
              "Content-Type": "application/json;charset=utf-8",
              "content-format-version": "1",
            },
            body: JSON.stringify({
              lastPostNumber: this.state.data.posts.length != 0 && this.state.data.posts[this.state.data.posts.length - 1].threadPostNumber,
              lastSeen: this.state.data.posts.length != 0 && this.state.data.posts[this.state.data.posts.length - 1].createdAt,
              threadId: this.state.data.id
            }),
            method: "POST",
            mode: "cors"
          })
        }
      }
    })
  }
  componentDidMount() {
    console.log("showthread mount")
    document.addEventListener('click', this.clickBg)
    
    this.loadThread()
    
    if (!window.twttr) {
      window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));
    }
  }
  
  pruneMentions() {
    if (!this.state.data || !this.state.data.posts)
      return
    if (!this.props.appState.notifications || this.props.appState.notifications.message)
      return
    let modified = false
    let notifications = this.props.appState.notifications.slice(0)
    let notificationIds = []
    for (let i = 0; i < this.state.data.posts.length; i++) {
      for (let j = 0; j < notifications.length; j++) {
        let mention = notifications[j]
        if (mention.data.id == this.state.data.posts[i].id) {
          console.log("MENTION CLEAR!", mention)
          notificationIds.push(mention.id)
          //notifications.splice(j, 1)
          //j--
        }
      }
    }
    if (notificationIds.length) {
      markNotificationsAsRead(this.props, notificationIds)
    }
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    if (prevstate.loaded != this.state.loaded && this.state.loaded) {
      let post = document.getElementById("post-" + this.query.p)
      if (post)
        post.scrollIntoView()
    }
    if (prevprops.appState != this.props.appState) {
      this.pruneMentions()
    }
    if (prevstate.text !== this.state.text) {
      //this.onTextChange({target: {value: this.state.text}})
    }
  }
  doSubscribe(unsub) {
    window.fetch(this.props.appState.api + "/alerts", {
      method: unsub ? 'DELETE' : 'POST', 
      credentials: 'include',
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "content-format-version": "1",
      },
      body: JSON.stringify({
        threadId: this.props.query.t,
        lastPostNumber: this.state.data.posts.length != 0 && this.state.data.posts[this.state.data.posts.length - 1].threadPostNumber,
      }),
    }).then(r=>r.json()).then((r)=>{
      console.log("sucessfully " + this.props.query.do + "d!")
    })
  }
  onTextChange(text) {
    //this.state.text = text
    this.setState({text: text})
    localStorage.setItem("kopunch_thread_reply_" + this.query.t, text)
  }
  render() {
    let page = this.query.page
    if (!page)
      page = 1
    let own = this.state.loaded && this.props.appState.userid == this.state.data.user.id
    let subscribed = this.state.data && this.state.data.subscribed
    if (this.props.query.do === "subscribe") {
      subscribed = true
    } else if (this.props.query.do === "unsubscribe") {
      subscribed = false
    } else if (this.props.appState.autosub === "read") {
      subscribed = true
    }
    let posts_per_page = POSTS_PER_PAGE
    try {
      posts_per_page = this.state.data.archived.Page.PerPage
    } catch (e) {}
    return (
      <div id="forum">
        <Helmet>
          <link rel="canonical" href={"https://kopunch.club/showthread.php?t=" + this.props.query.t + (this.props.query.page ? "&page=" + this.props.query.page : "")}/>
        </Helmet>
        <Header
          title={this.state.loaded && this.state.data.title} 
          subforum={this.state.loaded && (this.state.data.subforumName || this.state.data.subforum && this.state.data.subforum.name)} 
          subforumid={this.state.loaded && this.state.data.subforumId} 
          appState={this.props.appState} 
          appSetState={this.props.appSetState}
        />
        {this.state.notfound && <div style={{ textAlign: 'center' }}>There's nothing here. Not anymore, at least. ({this.state.notfound})</div>}
        {this.state.loaded && <div className="content-inner" style={{opacity: this.state.previewBackground ? 0.5 : 1}}>
          <Pagination page={this.state.data.currentPage} totalItems={this.state.data.totalPosts} itemsPerPage={posts_per_page} location={this.props.location} query={this.props.query}/>
          <div className="thread-head">
            {!this.state.data.locked && <LinkWrapper href={"/newreply.php?t=" + this.state.data.id + "&page=" + this.props.query.page} className="reply">Reply</LinkWrapper>}
            {this.state.data.locked && <span className="reply">Closed Thread</span>}
            {!subscribed && <LinkWrapper href={"/showthread.php?t=" + this.query.t + (this.query.page ? "&page=" + this.query.page : "") + "&do=subscribe"} className="subscribe">Subscribe</LinkWrapper>}
            {subscribed && <LinkWrapper href={"/showthread.php?t=" + this.query.t + (this.query.page ? "&page=" + this.query.page : "") + "&do=unsubscribe"} className="subscribe">Unsubscribe</LinkWrapper>}
            {own && <span className="subscribe">&nbsp;/&nbsp;</span>}
            {own && <LinkWrapper href="" className="subscribe" onClick={(e)=>{e.preventDefault(); this.setState({renameMenu: !this.state.renameMenu})}}>Rename</LinkWrapper>}
          </div>
          {this.state.renameMenu && <div className="popover panel rename-menu">
            <form onSubmit={(e)=>{
              e.preventDefault()
              window.fetch(this.props.appState.api + "/v2/threads/" + this.state.data.id, {
                method: 'PUT', 
                credentials: 'include',
                headers: {
                  "Content-Type": "application/json;charset=utf-8",
                  "content-format-version": "1",
                },
                body: JSON.stringify({
                  title: this.state.renameText,
                }),
              }).then(r=>r.json()).then((r)=>{
                window.location.reload()
              })
              this.setState({renameMenu: false})
            }}>
              <input type="text" value={this.state.renameText} onChange={(e)=>{this.setState({renameText: e.target.value})}}/>
            </form>
          </div>}
          <ol className="post-list" start={(page - 1) * posts_per_page + 1}>
            {this.state.data.posts.length != 0 && this.state.data.posts.map((data, index)=>{
              return [
                <Post 
                  key={data.id}
                  data={data}
                  number={(page - 1) * posts_per_page + 1 + index}
                  thread={this.state.data.id}
                  threadRead={this.state.data.readThreadLastSeen}
                  locked={this.state.data.locked}
                  page={page}
                  subforum={this.state.data.subforumId}
                  onReplyButtonClicked={(e)=>{
                    e.preventDefault()
                    let content = stripQuotes(data.content)
                    this.setState({editorIndex: index, text: this.state.text + '[quote mentionsUser="' + data.user.id + '" username="' + data.user.username + '" threadId="' + this.props.query.t + '" threadPage="' + this.props.query.page + '" postId="' + data.id + '"]\n' + content.trim() + '\n[/quote]\n\n'})
                  }}
                  appState={this.props.appState}
                  appSetState={this.props.appSetState}
                />, 
                this.state.editorIndex === index && this.props.appState.authenticated && !this.props.appState.userbanned && !this.state.data.locked && 
                <div className="quick-reply">
                  <PostEditor 
                    text={this.state.text} 
                    goAdvanced={"/newreply.php?t=" + this.state.data.id} 
                    thread={this.props.query.t}
                    page={this.props.query.page || 1}
                    edit={this.props.query.edit} 
                    onCancel={()=>{this.setState({editorIndex: -1, text: ""})}} 
                    onChange={this.onTextChange} 
                    appState={this.props.appState} 
                    appSetState={this.props.appSetState}
                  />
                </div> || null
              ]
            })}
          </ol>
          <div className="thread-head foot">
            {!this.state.data.locked && <LinkWrapper href={"/newreply.php?t=" + this.state.data.id + "&page=" + this.props.query.page} className="reply">Reply</LinkWrapper>}
            {this.state.data.locked && <span className="reply">Closed Thread</span>}
            {!subscribed && <LinkWrapper href={"/showthread.php?t=" + this.query.t + (this.query.page ? "&page=" + this.query.page : "") + "&do=subscribe"} className="subscribe">Subscribe</LinkWrapper>}
            {subscribed && <LinkWrapper href={"/showthread.php?t=" + this.query.t + (this.query.page ? "&page=" + this.query.page : "") + "&do=unsubscribe"} className="subscribe">Unsubscribe</LinkWrapper>}
          </div>
          <Pagination page={this.state.data.currentPage} totalItems={this.state.data.totalPosts} itemsPerPage={posts_per_page} location={this.props.location} query={this.props.query}/>
          {this.state.editorIndex === -1 && this.props.appState.authenticated && !this.props.appState.userbanned && !this.state.data.locked && <div className="quick-reply">
            <PostEditor 
              text={this.state.text} 
              onChange={this.onTextChange} 
              goAdvanced={"/newreply.php?t=" + this.state.data.id} 
              thread={this.props.query.t} 
              page={this.props.query.page || 1}
              edit={this.props.query.edit} 
              appState={this.props.appState} 
              appSetState={this.props.appSetState} 
              onCancel={()=>{
                this.onTextChange("")
              }}
            />
          </div>}
          {this.state.loaded && this.state.data.viewers && <ViewerCount id={this.props.query.t} members={this.state.data.viewers.memberCount} guests={this.state.data.viewers.guestCount}/>}
        </div>}
        <Footer subforum={this.state.loaded && (this.state.data.subforumName || this.state.data.subforum && this.state.data.subforum.name)} subforumid={this.state.loaded && this.state.data.subforumId}/>
      </div>
    );
  }
}