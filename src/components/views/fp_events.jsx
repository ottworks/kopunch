import React, { Component } from "react"
import Header from '../Header'
import Footer from '../Footer'
import { elapsedString } from '../TimeElapsed'
import '../../stylesheets/fp_events.css'
import EventEntry from "../EventEntry"

export default class Events extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      loaded: false,
    }
    this.lastTime = ""
  }
  componentDidMount() {
    window.fetch(this.props.appState.api + "/v2/events", {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.data = result
      this.setState({loaded: true})
    })
  }
  render() {
    return (
      <div id="forum">
        <Header title="Event Log" appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table>
            <tbody>
              <tr>
                <td>
                  {this.state.loaded && (this.data.message || this.data.map((data, index)=>{
                    let changed = this.lastTime != elapsedString(data.createdAt || data.created_at)
                    if (changed)
                      this.lastTime = elapsedString(data.createdAt || data.created_at)
                    return <span className="event-entry">
                      <EventEntry key={data.id} data={data} showTime={changed} p/>
                    </span>
                  }))}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}