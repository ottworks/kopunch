import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed, { elapsedString } from '../TimeElapsed'
import ThreadIcon from '../ThreadIcon'
import { ThreadEntry } from './forumdisplay'
import LinkWrapper from './../LinkWrapper'
import '../../stylesheets/fp_popular.css'

const POSTS_PER_PAGE = 20

export default class Popular extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.loadPopular = this.loadPopular.bind(this)
    this.state = {
      loaded: false,
      tagFilter: "",
    }
    this.lastTime = ""
  }
  loadPopular() {
    window.fetch(this.props.appState.api + "/v2/threads/popular", {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.setState({data: result, loaded: true})
    })
  }
  componentDidMount() {
    this.loadPopular()
  }
  componentDidUpdate(prevprops) {
  }
  render() {
    return (
      <div id="forum">
        <Header title="Popular Threads" appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          <table>
            <tbody>
              {this.state.loaded && this.state.data.map && this.state.data.map((data, index)=>{
                return <ThreadEntry key={data.id} withSubforum data={data} number={index + 1} tagFilter={this.state.tagFilter} setTagFilter={(tag)=>{this.setState({tagFilter: tag})}} appState={this.props.appState} appSetState={this.props.appSetState}/>
              })}
              {this.state.loaded && this.state.data.message || null}
            </tbody>
          </table>
        </div>
        <Footer/>
      </div>
    );
  }
}