import React, { Component } from "react"
import { Link } from 'react-router-dom'
import Header from '../Header'
import Footer from '../Footer'
import TimeElapsed from '../TimeElapsed'
import LinkWrapper from '../LinkWrapper'
import ImgWrapper from '../ImgWrapper'
import ThreadAdvert from '../ThreadAdvert'
import '../../stylesheets/forum.css'
import '../../stylesheets/forumdisplay.css'
import { POSTS_PER_PAGE } from './showthread'

var SubforumEntry = (props)=>{
  let icon_url = props.data.icon || ""
  if (icon_url[0] == "/")
    icon_url = "https://knockout.chat" + icon_url
  if (icon_url.includes("knockout.chat")) {
    icon_url = "/hotlink/" + icon_url
  }
  let lastpostpage = Math.floor((props.data.lastPost.thread?.postCount - 1) / POSTS_PER_PAGE) + 1
  let avatar = props.data.lastPost.user?.avatarUrl ?? "none.webp"
  if (avatar.startsWith("none.webp")) {
    avatar = props.data.lastPost.user?.backgroundUrl ?? "none.webp"
  }
  return <tr className="subforum-entry">
    <td className="subforum-info" style={{backgroundImage: 'url(' + icon_url + ')'}}>
      <LinkWrapper className="subforum-title" href={"/forumdisplay.php?f=" + props.data.id} title={props.data.totalThreads + " Threads, " + props.data.totalPosts + " Posts"}>{props.data.name}</LinkWrapper>
      <span className="subforum-viewcount small"> (69 Viewing)</span>
      <div className="subforum-description small">{props.data.description}</div>
    </td>
    {props.data.lastPost &&
      <td className="subforum-lastpost small">
        <div className="avatar-thumb">
          <LinkWrapper href={"/member.php?u=" + props.data.lastPost.user?.id}>
            <object alt={props.data.lastPost.user?.username + "'s avatar"} title={props.data.lastPost.user?.username + "'s avatar"} data={"https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/" + avatar} type="image/webp">
              <source srcSet={"https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/" + avatar} type="image/webp"/>
              <img srcSet="/static/defaultavatar.png"/>
            </object>
          </LinkWrapper>
        </div>
        <p className="thread-link">
          <LinkWrapper href={"/showthread.php?t=" + props.data.lastPost.thread?.id + "&goto=unread"} title={props.data.lastPost.thread?.title}>
            {props.data.lastPost.thread?.title}
          </LinkWrapper>
        </p>
        <p className="thread-lastpost">
          <TimeElapsed date={props.data.lastPost.createdAt}/>
          &nbsp;
          <LinkWrapper className="goto-post-arrow" href={"/showthread.php?t=" + props.data.lastPost.thread?.id + "&page=" + props.data.lastPost.page + "&p=" + props.data.lastPost.id} title="Go to last post">&#9658;</LinkWrapper>
        </p>
      </td>
    }
  </tr>
}

export default class Forum extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      loaded: false,
      single: false,
      statsloaded: false,
    }
    console.log("constructed forum!")
    this.updateWidth = this.updateWidth.bind(this)
    this.loadForum = this.loadForum.bind(this)
  }
  updateWidth() {
    let shouldSingle = this.props.appState.singlecolumn || window.innerWidth < 860
    if (this.state.single != shouldSingle)
      this.setState({single: shouldSingle})
  }
  loadForum() {
    window.fetch(this.props.appState.api + "/subforum?hideNsfw=" + (this.props.appState.nsfw ? 0 : 1), {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.setState({data: result, loaded: true})
    })
    window.fetch(this.props.appState.api + "/stats", {credentials: 'include'}).then(res => res.json()).then((result)=>{
      console.log(result)
      this.setState({stats: result, statsloaded: true})
    })
  }
  componentDidMount() {
    this.updateWidth()
    window.addEventListener('resize', this.updateWidth);
    this.loadForum()
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWidth);
  }
  componentDidUpdate(prevprops) {
  }
  render() {
    console.log("rendered forum!")
    let d = new Date()
    let pm = false
    let time = d.getHours()
    if (time >= 12) {
      pm = true
      time -= 12
    }
    if (time == 0)
      time = 12
    if (time < 10)
      time = "0" + time
    let minutes = d.getMinutes()
    if (minutes < 10) minutes = "0" + minutes

    time += ":" + minutes + (pm ? " PM" : " AM")
    return (
      <div id="forum">
        <Header appState={this.props.appState} appSetState={this.props.appSetState}/>
        <div className="content-inner">
          {!this.state.single &&
            <table className="table-right">
              <thead>
                <tr className="category-title">
                  <td><b>Knockout</b></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.state.loaded && this.state.data.list.map((data, index)=>{return index % 2 == 1 ? <SubforumEntry key={data.id} data={data}/> : null})}
              </tbody>
            </table>
          }
          <table className={this.state.single ? "" : "table-left"}>
            <thead>
              <tr className="category-title">
                <td><b>Knockout</b></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {this.state.loaded && this.state.data.list.map((data, index)=>{return (index % 2 == 0 || this.state.single) ? <SubforumEntry key={data.id} data={data}/> : null})}
            </tbody>
          </table>
          <ThreadAdvert appState={this.props.appState} appSetState={this.props.appSetState}/>
          {this.state.statsloaded && <div className="stats">
            <b>{this.state.stats.threadCount.toLocaleString()}</b> threads, <b>{this.state.stats.postCount.toLocaleString()}</b> posts, <b>{this.state.stats.userCount.toLocaleString()}</b> users, <b>{this.state.stats.ratingsCount.toLocaleString()}</b> ratings.
            <br/><br/>
            The time is now {time}.
          </div>}
        </div>
        <Footer/>
      </div>
    );
  }
}