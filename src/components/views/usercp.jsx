import React, { Component } from "react"
import Header from '../Header'
import Footer from '../Footer'
import Username from '../Username'
import LinkWrapper from './../LinkWrapper'
import { userOptions } from '../../index'
import Post from '../Post'
import PostEditor from '../PostEditor'
import '../../stylesheets/usercp.css'
import { markNotificationsAsRead } from "../api/notifications"
import { searchUser } from "../api/user"

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const tabs = ["Profile", "Settings"]

export default class UserCP extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      loading: true,
      error: undefined,
      recipientText: "",
      text: "",
      recipients: [],
      numFetchedUsers: 0,
    }
    this.fetchContent = this.fetchContent.bind(this)
    this.generateContent = this.generateContent.bind(this)
    this.onTextChange = this.onTextChange.bind(this)
    this.searchForMatchingUsers = this.searchForMatchingUsers.bind(this)
    this.generateSettings = this.generateSettings.bind(this)
    this.generateInbox = this.generateInbox.bind(this)
    this.generateConversation = this.generateConversation.bind(this)
    this.generateNewMessage = this.generateNewMessage.bind(this)
  }
  componentDidMount() {
    console.log("USERCP", this.props.query)
    this.fetchContent()
    if (this.props.query.userid) {
      fetch(this.props.appState.api + "/user/" + this.props.query.userid, {
        "credentials": "include",
        "method": "GET",
      }).then(r=>r.json()).then(u=>{
        console.log("user!", u)
        let r = this.state.recipients.slice()
        r.push(u)
        this.setState({recipients: r})
      })      
    }
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    console.log("update")
    if (prevstate.loaded != this.state.loaded && this.state.loaded && this.props.query.conversation) {
      let post = document.getElementById("post-" + this.props.query.p)
      console.log("post scroll")
      if (post)
        post.scrollIntoView()
    }
    if (prevstate.conversation != this.state.conversation && this.state.conversation && this.props.appState.user) {
      console.log("checking unread")
      let unreadPosts = []
      for (const m of this.state.conversation.messages) {
        if (m.readAt == null && m.user.id !== this.props.appState.user.id) {
          unreadPosts.push(m.id)
        }
      }
      let unreadNotifications = []
      for (const n of this.props.appState.notifications) {
        if (n.type === "MESSAGE" && !n.read) {
          let cid = n?.data?.messages && n?.data?.messages[0]?.conversationId
          if (cid === Number(this.props.query.conversation)) {
            unreadNotifications.push(n.id)
          }
        }
      }
      console.log("unread posts", unreadPosts)
      console.log("unread notifications", unreadNotifications)
      if (unreadNotifications.length !== 0) {
        console.log("Marking notifications as read:", unreadNotifications)
        markNotificationsAsRead(this.props, unreadNotifications)
      }
      if (unreadPosts.length !== 0) {
        console.log("Marking posts as read:", unreadPosts)
        let promise
        let grab_one = ()=>{
          let id = unreadPosts.shift()
          if (id) {
            console.log("Marking post as read:", id)
            fetch(this.props.appState.api + "/messages/" + id, {
              "credentials": "include",
              "headers": {
                "content-format-version": "1",
              },
              "method": "PUT",
            }).then(grab_one)
          }
        }
        grab_one()
      }
    }
  }
  fetchContent() {
    console.log("fetchContent!!!!!!!!!!!!!!!!")
    if (this.props.query.do === "inbox") {
      if (this.props.query.conversation === undefined) {
        this.setState({loaded: true})
        fetch(this.props.appState.api + "/conversations", {
            "credentials": "include",
            "headers": {
                "content-format-version": "1",
            },
            "method": "GET",
        }).then(r=>r.json()).then(r=>{
          console.log("conversations!", r)
          this.setState({loaded: true, loading: false, error: undefined})
          this.props.appSetState({conversations: r})
        }).catch(e=>{
          this.setState({loading: false, error: e})
        })
      } else {
        console.log("Setting text state")
        this.setState({text: localStorage.getItem("kopunch_conversation_reply_" + this.props.query.conversation) || "", loaded: false})
        fetch(this.props.appState.api + "/conversations/" + this.props.query.conversation, {
            "credentials": "include",
            "headers": {
                "content-format-version": "1",
            },
            "method": "GET",
        }).then(r=>r.json()).then(r=>{
          console.log("conversation!", r)
          this.setState({loaded: true, loading: false, error: undefined, conversation: r})
        }).catch(e=>{
          this.setState({loading: false, error: e})
        })
      }
    }
  }
  render() {
    let title = "User Control Panel"
    if (this.props.query.do === "inbox") {
      title = "Inbox"
    } else if (this.props.query.do === "newmessage") {
      title = "Send New Message"
    } else if (this.props.query.do === "subscriptions") {
      title = "Subscriptions"
    } else if (this.props.query.do === "settings") {
      title = "General Settings"
    }
    return (
      <div id="forum">
        <Header title={title} appState={this.props.appState} appSetState={this.props.appSetState}/>
        {<div className="content-inner usercp">
          <div className="row">
            <div className="col" style={{flexBasis: '200px', flexGrow: 0}}>
              <div className="block menu">
                <h2 className="blockhead">My Messages</h2>
                <div className={"menu-item row" + (this.props.query.do === "inbox" ? " active" : "")}>
                  <div className="col" style={{flexGrow: 0, flexBasis: '24px'}}/>
                  <div className="col"><LinkWrapper href="?do=inbox">Inbox</LinkWrapper></div>
                </div>
                <hr/>
                <div className={"menu-item row" + (this.props.query.do === "newmessage" ? " active" : "")}>
                  <div className="col" style={{flexGrow: 0, flexBasis: '24px'}}/>
                  <div className="col"><LinkWrapper href="?do=newmessage">Send New Message</LinkWrapper></div>
                </div>
              </div>
              <div className="block menu">
                <h2 className="blockhead">My Subscriptions</h2>
                <div className={"menu-item row" + (this.props.query.do === "subscriptions" ? " active" : "")}>
                  <div className="col" style={{flexGrow: 0, flexBasis: '24px'}}/>
                  <div className="col"><LinkWrapper href="?do=subscriptions">Subscriptions</LinkWrapper></div>
                </div>
              </div>
              <div className="block menu">
                <h2 className="blockhead">My Settings</h2>
                <div className="menu-item disabled row">
                  <div className="col" style={{flexGrow: 0, flexBasis: '24px'}}/>
                  <div className="col">My Profile</div>
                </div>
                <div className={"menu-item row" + (this.props.query.do === "settings" ? " active" : "")}>
                  <div className="col" style={{flexGrow: 0, flexBasis: '24px'}}/>
                  <div className="col"><LinkWrapper href="?do=settings">General Settings</LinkWrapper></div>
                </div>
              </div>
            </div>
            <div className="col">
              {this.generateContent()}
            </div>
          </div>
        </div>}
        <Footer/>
      </div>
    );
  }
  onTextChange(text) {
    console.log("onTextChange")
    localStorage.setItem("kopunch_conversation_reply_" + this.props.query.conversation, text)
    this.setState({text: text})
  }
  searchForMatchingUsers(text) {
    searchUser(this.props.appState.api, text, (r)=>{
      this.setState({ numFetchedUsers: r.totalUsers, fetchedUsers: r.users })
    })
  }
  generateContent() {
    console.log("generateContent")
    if (this.state.error) {
      console.log("error")
      return <div className="error">
        {this.state.error} <LinkWrapper className={"link-button" + (this.state.loading ? " disabled" : "")} href="#" onClick={()=>{
          this.setState({loading: true})
          this.fetchContent()
        }}>Retry?</LinkWrapper>
      </div>
    }
    if (this.props.query.do === "settings") {
      return this.generateSettings()
    } else if (this.props.query.do === "inbox") {
      if (!this.state.loaded) {
        console.log("inbox notloaded")
        return null
      }
      if (this.props.query.conversation === undefined) {
        return this.generateInbox()
      } else {
        return this.generateConversation()
      }
    } else if (this.props.query.do === "newmessage") {
      return this.generateNewMessage()
    }
  }
  generateSettings() {
    console.log("settings")
    let options = []
    for (const [option, data] of Object.entries(userOptions)) {
      if (data.hidden) continue;
      let elements = []
      if (data.type == "radio") {
        for (let i = 0; i < data.options.length; i++) {
          elements.push(<input type="radio" name={option} id={option + "-" + data.options[i][0]} checked={this.props.appState[option] == data.options[i][0]} onChange={()=>{this.props.appSetState({[option]: data.options[i][0]})}}/>)
          elements.push(<label for={option + "-" + data.options[i][0]}>{data.options[i][1]}</label>)
        }
      } else if (data.type == "dropdown") {
        console.log("dropdown!")
        elements.push(<select id={option} value={this.props.appState[option]} onChange={(e)=>{
          this.props.appSetState({[option]: e.target.value})
        }}>
          {data.options.map((v)=>{
            return <option value={v[0]}>{v[1]}</option>
          })}
        </select>)
      } else if (data.type == "toggle") {
        elements.push(<input type="radio" name={option} id={option + "-" + data.options[0]} checked={this.props.appState[option] == false} onChange={()=>{this.props.appSetState({[option]: false})}}/>)
        elements.push(<label for={option + "-" + data.options[0]}>{data.options[0]}</label>)
        elements.push(<input type="radio" name={option} id={option + "-" + data.options[1]} checked={this.props.appState[option] == true} onChange={()=>{this.props.appSetState({[option]: true})}}/>)
        elements.push(<label for={option + "-" + data.options[1]}>{data.options[1]}</label>)
      }
      options.push(<div>
        <div className="form-label">
          {data.name}
        </div>
        <div className="form-setting">
          {elements}
        </div>
      </div>)
    }
    return <div className="block" key="settings">
      <h2 className="blockhead">User Settings</h2>
      <div className="blockbody">
        {options}
      </div>
    </div>
  }
  generateInbox() {
    console.log("inbox")
    return <div className="block" key="inbox">
      <h2 className="blockhead">Inbox</h2>
      <div className="blockbody inbox">
        {this.props.appState.conversations && this.props.appState.conversations.map && this.props.appState.conversations.map(v=>{
          return <div className="inbox-message" key={v.id}>
            <LinkWrapper className="message-title" href={"?do=inbox&conversation=" + v.id}>
              {v.messages.map(m=>m.user.username + ": " + m.content).join(" ")}
            </LinkWrapper>
            <div className="message-users small">
              {v.users.map((u, i)=>{
                return [
                  <LinkWrapper key={u.id} href={"member.php?u=" + u.id}>{u.username}</LinkWrapper>,
                  i == v.users.length - 1 ? null : ", "
                ]
              })}
            </div>
          </div>
        })}
      </div>
    </div>
  }
  generateConversation() {
    console.log("conversation")
    return <div className="block" key="conversation">
      <h2 className="blockhead">Conversation with {this.state.conversation.users.map((u, i)=>{
        return [
          <LinkWrapper key={u.id} href={"member.php?u=" + u.id}>{u.username}</LinkWrapper>,
          i == this.state.conversation.users.length - 1 ? null : ", "
        ]
      })}</h2>
      <div key="posts" className="posts">
        {this.state.conversation.messages.map((m, i)=>{
          return <Post
            key={m.id}
            data={m}
            number={m.id}
            threadRead={m.readAt != null}
            noReport
            noRatings
            noButtons
            permalink={"usercp.php?do=inbox&conversation=" + this.props.query.conversation + "&p=" + m.id}
            appState={this.props.appState} 
            appSetState={this.props.appSetState}
          />
        }).reverse()}
      </div>
      <div key="quick-reply" className="quick-reply">
        <PostEditor 
          key="ConversationReplyEditor"
          text={this.state.text}
          onChange={this.onTextChange}
          appState={this.props.appState}
          appSetStattEditor
          e={this.props.appSetState}
          doingReply={this.state.doingReply}
          error={this.state.postError}
          onSubmit={text=>{
            this.setState({doingReply: true})
            fetch(this.props.appState.api + "/messages", {
              "credentials": "include",
              "headers": {
                "content-format-version": "1",
                "Content-Type": "application/json;charset=utf-8",
              },
              "body": JSON.stringify({
                receivingUserId: this.state.conversation.users.map(u=>u.id).filter(id=>id !== this.props.appState.user.id)[0],
                content: text,
                conversationId: this.state.conversation.id,
                appName: "kopunch",
                body: JSON.stringify({
                  content: text,
                })
              }),
              "method": "POST",
            }).then(r=>r.json()).then(r=>{
              if ((r.error || r.errors)) {
                this.setState({postError: (r.error || r.errors), doingReply: false})
              } else {
                this.setState({doingReply: false})
                this.onTextChange("")
                this.props.history.push("usercp.php?do=inbox&conversation=" + r.conversationId + "&p=" + r.id)
              }
            })
          }}
        />
      </div>
    </div>
  }
  generateNewMessage() {
    console.log("new message")
    return <div className="block" key="newmessage">
      <h2 className="blockhead">Post New Message</h2>
      <div className="blockbody">
        <b>Recipients:</b><br/>
        {this.state.recipients.map((u, i)=>{
          return [
            <Username user={u} href={"/member.php?u=" + u.id} onClick={(e)=>{
              e.preventDefault()
              let r = this.state.recipients.slice()
              r.splice(i, 1)
              this.setState({recipients: r})
            }}/>,
            "; "
          ]
        })}
        <input className="title-input" type="text" value={this.state.recipientText} onChange={(e)=>{
          let text = e.target.value
          this.setState({recipientText: text})
          this.searchForMatchingUsers(text)
        }}/>
        {this.state.numFetchedUsers > 0 && <div className="matching-user-list">
          {this.state.fetchedUsers && this.state.fetchedUsers.map(u=>{
            return <div key={u.id}>
              <Username user={u} href="#" onClick={(e)=>{
                e.preventDefault()
                let r = this.state.recipients.slice()
                r.push(u)
                this.setState({recipients: r, recipientText: ""})
                this.searchForMatchingUsers("")
              }}/>
            </div>
          })}
        </div>}
      </div>
      <hr/>
      <div className="quick-reply">
        <PostEditor 
          key="ConversationReplyEditor"
          text={this.state.text}
          onChange={this.onTextChange}
          appState={this.props.appState}
          appSetStattEditor
          e={this.props.appSetState}
          doingReply={this.state.doingReply}
          error={this.state.postError}
          onSubmit={text=>{
            if (this.state.recipients.length > 0) {
              this.setState({doingReply: true})
              let i = 0
              fetch(this.props.appState.api + "/messages", {
                "credentials": "include",
                "headers": {
                  "content-format-version": "1",
                  "Content-Type": "application/json;charset=utf-8",
                },
                "body": JSON.stringify({
                  receivingUserId: this.state.recipients.length > 1 ? this.state.recipients.map(u=>u.id) : this.state.recipients[0].id,
                  content: text,
                  appName: "kopunch",
                  body: JSON.stringify({
                    content: text,
                  })
                }),
                "method": "POST",
              }).then(r=>r.json()).then(r=>{
                if ((r.error || r.errors)) {
                  this.setState({postError: (r.error || r.errors), doingReply: false})
                } else {
                  console.log("created conversation!", r) // r.conversationId
                  this.setState({doingReply: false})
                  this.onTextChange("")
                  this.fetchContent()
                  this.props.history.push("usercp.php?do=inbox&conversation=" + r.conversationId)
                }
              })
            } else {
              this.setState({postError: "Please select 1 or more recipients."})
            }
          }}
        />
      </div>
    </div>
  }
}

//await fetch("https://api.knockout.chat/messages/583", {
//    "credentials": "include",
//    "headers": {
//        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0",
//        "Accept": "application/json, text/plain, */*",
//        "Accept-Language": "en-US,en;q=0.5",
//        "content-format-version": "1",
//        "Pragma": "no-cache",
//        "Cache-Control": "no-cache"
//    },
//    "referrer": "https://knockout.chat/messages/138",
//    "method": "PUT",
//    "mode": "cors"
//});