export function markNotificationsAsRead(props, ids) {
    window.fetch(`${props.appState.api}/v2/notifications`, {
        credentials: 'include',
        method: 'PUT',
        headers: {
            'content-format-version': 1,
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify({
            notificationIds: ids,
        })
    }).then(r => r.json()).then(r => {
        if (r.message) {
            console.log(r.message)
        } else {
            let notifs = props.appState.notifications.slice()
            for (let i = notifs.length - 1; i >= 0; i--) {
                if (ids.includes(notifs[i].id)) {
                    notifs[i].read = true
                    notifs.splice(i, 1)
                }
            }
            props.appSetState({ notifications: notifs })
        }
    })
}
