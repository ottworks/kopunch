let cachedSubforum
let cachedSubforumListeners = {}

export function getSubforum(api, id, callback) {
    console.log("getSubforum", api, id)
    if (cachedSubforum) {
        callback(cachedSubforum.list.find((s) => s.id == id))
        return
    }
    if (cachedSubforum === undefined) {
        cachedSubforum = false
        window.fetch(api + "/subforum", {
            method: 'GET',
        }).then(r => r.json()).then(r => {
            console.log("Subforum received", r)
            cachedSubforum = r
            callback(cachedSubforum.list.find((s) => s.id == id))
            console.log("cachedSubforumListeners", cachedSubforumListeners)
            if (cachedSubforumListeners) {
                for (const id in cachedSubforumListeners) {
                    let found = cachedSubforum.list.find((s) => s.id == id)
                    for (const listener of cachedSubforumListeners[id]) {
                        listener(found)
                    }
                }
            }
        })
    } else {
        cachedSubforumListeners[id] = cachedSubforumListeners[id] || []
        cachedSubforumListeners[id].push(callback)
        console.log("cachedSubforumListeners", cachedSubforumListeners)
    }
}
