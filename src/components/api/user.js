let cachedProfiles = {}
let cachedProfileListeners = {}

export function getUserProfile(api, id, callback) {
    if (cachedProfiles[id]) {
        callback(cachedProfiles[id])
        return
    }
    if (cachedProfiles[id] === undefined) {
        cachedProfiles[id] = false
        window.fetch(api + "/v2/users/" + id + "/profile", {
            method: 'GET',
        }).then(r => r.json()).then(r => {
            console.log("profile received", id, r)
            cachedProfiles[id] = r
            callback(r)
            if (cachedProfileListeners[id]) {
                for (const listener of cachedProfileListeners[id]) {
                    listener(r)
                }
            }
        })
    } else {
        cachedProfileListeners[id] = cachedProfileListeners[id] || []
        cachedProfileListeners[id].push(callback)
    }
}

let cachedUsers = {}
let cachedUserListeners = {}

export function getUser(api, id, callback) {
    if (cachedUsers[id]) {
        callback(cachedUsers[id])
        return
    }
    if (cachedUsers[id] === undefined) {
        cachedUsers[id] = false
        window.fetch(api + "/user/" + id, {
            method: 'GET',
        }).then(r => r.json()).then(r => {
            console.log("User received", id, r)
            cachedUsers[id] = r
            callback(r)
            if (cachedUserListeners[id]) {
                for (const listener of cachedUserListeners[id]) {
                    listener(r)
                }
            }
        })
    } else {
        cachedUserListeners[id] = cachedUserListeners[id] || []
        cachedUserListeners[id].push(callback)
    }
}


let nextSearch
let lastSearch
let lastResult
let isSearching
export function searchUser(api, text, callback) {
    console.log("searchUser", text)
    if (lastSearch === text || (text.startsWith(lastSearch) && lastResult.totalUsers === 0)) {
        callback(lastResult)
        return
    }
    nextSearch = text
    if (!text) {
        callback({
            totalUsers: 0,
            fetchedUsers: [],
        })
    } else if (!isSearching) {
        isSearching = true
        lastSearch = text
        fetch(api + "/users/?filter=" + text, {
            "credentials": "include",
            "headers": {
                "content-format-version": "1",
            },
            "method": "GET",
        }).then(r => r.json()).then(r => {
            isSearching = false
            lastResult = r
            callback(r)
            if (nextSearch !== text) {
                searchUser(api, nextSearch, callback)
            }
        }).catch(e => {
            isSearching = false
            if (nextSearch !== text) {
                searchUser(api, nextSearch, callback)
            }
        })
    } else {
    }
}
