import React, { Component } from "react";

export default class ImgWrapper extends React.Component {
  constructor(props) {
    super(props)
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {

  }
  render() {
    let src = this.props.src
    if (src && src.includes("knockout.chat/") && !src.includes("hotlink-ok")) {
      src = "/hotlink/" + src
    }
    return <picture>
      <source srcSet={this.props.src}/>
      <img {...this.props} ref={this.props.innerRef} onLoad={this.props.onLoad} src={src}/>
    </picture>
  }
}
