import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import Post, { emotify } from './Post'
import LinkWrapper from './LinkWrapper'
import { POSTS_PER_PAGE } from './views/showthread'
import { searchUser } from "./api/user"
import Username from "./Username"

const tag_buttons = [{
  title: "Bold",
  tag: "b",
  img: "/static/silkicons/text_bold.png",
}, {
  title: "Italic",
  tag: "i",
  img: "/static/silkicons/text_italic.png",
}, {
  title: "Underline",
  tag: "u",
  img: "/static/silkicons/text_underline.png",
}, {
  title: "Strikethrough",
  tag: "s",
  img: "/static/silkicons/text_strikethrough.png",
}, {
  title: "Spoiler",
  tag: "spoiler",
  img: "/static/silkicons/eye.png",
}, {
  title: "Heading 1",
  tag: "h1",
  img: "/static/silkicons/text_heading_1.png",
}, {
  title: "Heading 2",
  tag: "h2",
  img: "/static/silkicons/text_heading_2.png",
}, {
  title: "List Item",
  tag: "li",
  img: "/static/silkicons/bullet_add.png",
}, {
  title: "Ordered List",
  tag: "ol",
  img: "/static/silkicons/text_list_numbers.png",
  fun: (before, selected, after)=>{
    let lines = selected.split("\n")
    return before + "[ol]" + lines.map(l=>"[li]" + l + "[/li]").join("\n") + "[/ol]" + after
  }
}, {
  title: "Unordered List",
  tag: "ul",
  img: "/static/silkicons/text_list_bullets.png",
  fun: (before, selected, after)=>{
    let lines = selected.split("\n")
    return before + "[ul]" + lines.map(l=>"[li]" + l + "[/li]").join("\n") + "[/ul]" + after
  }
}, {
  title: "Link",
  tag: "url",
  img: "/static/silkicons/link.png",
}, {
  title: "Smart Link",
  tag: "url",
  img: "/static/silkicons/link_go.png",
  fun: (before, selected, after)=>{
    return before + "[url smart]" + selected + "[/url]" + after
  }
}, {
  title: "Link With Text",
  tag: "url",
  img: "/static/silkicons/link_edit.png",
  fun: (before, selected, after)=>{
    return before + '[url href="' + selected + '"][/url]' + after
  }
}, {
  title: "No Parse",
  tag: "noparse",
  img: "/static/silkicons/tag.png",
}, {
  title: "Quote",
  tag: "blockquote",
  img: "/static/silkicons/comment.png",
}, {
  title: "Code",
  tag: "code",
  img: "/static/silkicons/application_xp_terminal.png",
}, {
  title: "Image",
  tag: "img",
  img: "/static/silkicons/picture_link.png",
}, {
  title: "Image Thumbnail",
  tag: "img",
  img: "/static/silkicons/bullet_picture.png",
  fun: (before, selected, after)=>{
    return before + "[img thumbnail]" + selected + "[/img]" + after
  },
}, {
  title: "Video",
  tag: "video",
  img: "/static/silkicons/film_link.png",
}, {
  title: "YouTube",
  tag: "youtube",
  img: "/static/silkicons/film_link.png",
}, {
  title: "Vimeo",
  tag: "vimeo",
  img: "/static/silkicons/film_link.png",
}, {
  title: "Strawpoll",
  tag: "strawpoll",
  img: "/static/silkicons/chart_pie.png",
}, {
  title: "Twitter",
  tag: "twitter",
  img: "/static/silkicons/cancel.png",
}, {
  title: "Spotify",
  tag: "spotify",
  img: "/static/silkicons/feed.png",
}, {
  title: "Soundcloud",
  tag: "soundcloud",
  img: "/static/silkicons/control_play_blue.png",
}, {
  title: "Twitch",
  tag: "twitch",
  img: "/static/silkicons/television.png",
}, {
  title: "Upload Image",
  tag: "img",
  img: "/static/silkicons/attach.png",
  render: ({tag, text, editor, onChange}) => {
    const ref = React.useRef()
    const uploadFile = (file) => {
      if (file) {
        let formdata = new FormData()
        formdata.append("image", file)
        console.log("formdata", formdata)

        let start = editor?.current?.selectionStart ?? 0
        let end = editor?.current?.selectionEnd ?? 0

        let before = text.substring(0, start)
        let selected = text.substring(start, end)
        let after = text.substring(end)

        text = `${before}[img]${file.name}${file.lastModified}[/img]${after}`
        onChange(text)

        window.fetch("https://api.knockout.chat/postImages", {
          method: 'POST',
          headers: {
            "content-format-version": 1,
          },
          body: formdata,
          credentials: 'include',
        }).then(r => r.json()).then(r => {
          let url = r.message || `https://cdn.knockout.chat/image/${r.fileName}`
          text = editor?.current?.value ?? text
          onChange(text.replace(`${file.name}${file.lastModified}`, url))
        }).catch(e => {
          text = editor?.current?.value ?? text
          onChange(text.replace(`${file.name}${file.lastModified}`, e.message))
        })
        return file
      }
    }
    const paste = (e)=>{
      let file = e.clipboardData.files[0]
      uploadFile(file)
    }
    const drop = (e)=>{
      let file = e.dataTransfer.files[0]
      uploadFile(file)
    }
    const dragover = (e)=>{
      e.preventDefault()
    }
    React.useEffect(()=>{
      editor?.current?.addEventListener('drop', drop)
      editor?.current?.addEventListener('paste', paste)
      editor?.current?.addEventListener('dragover', dragover)
      return ()=>{
        editor?.current?.removeEventListener('dragover', dragover)
        editor?.current?.removeEventListener('paste', paste)
        editor?.current?.removeEventListener('drop', drop)
      }
    })
    return <>
      <LinkWrapper href="" key={tag.title} onClick={(e)=>{
        e.preventDefault()
        console.log("LinkWrapper click", ref.current)
        ref.current.click()
      }}>
        <img className={"icon icon-" + tag.tag} src={tag.img} title={tag.title}/>
      </LinkWrapper>
      <input ref={ref} style={{position: 'absolute', pointerEvents: 'none', opacity: 0}} type="file" onChange={(e)=>{
        console.log("onchange")
        const elem = e.target
        let file = elem.files[0]
        uploadFile(file)
      }}/>
    </>
  }
}]

class PostEditor extends Component {
  constructor(props) {
    super(props)

    this.state = {
      preview: false,
      text: this.props.text || "",
      error: "",
      doingReply: false,
      emotesDisabled: false,
    }

    this.ref = React.createRef()
    this.onChange = this.onChange.bind(this)
    this.doReply = this.doReply.bind(this)
    console.log("PostEditor constructor")
  }

  componentDidMount() {
    console.log("PostEditorDidMount")
  }

  componentWillUnmount() {
    console.log("PostEditorWillUnmount")
  }
  
  doReply(text) {
    this.setState({error: "", doingReply: true})
    let content = text
    console.log("TEXT: ", text)
    window.fetch(this.props.appState.api + "/v2/posts" + (this.props.edit ? "/" + this.props.edit : ""), {
      method: this.props.edit ? 'PUT' : 'POST', 
      credentials: 'include',
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "content-format-version": "1",
      },
      body: JSON.stringify({
        content: content,
        thread_id: Number(this.props.thread),
        display_country: this.props.appState.flagdog,
        app_name: "kopunch",
      }),
    }).then(r=>r.text()).then((r)=>{
      let parsed = false
      try {
        r = JSON.parse(r)
        parsed = true
      } catch (e) {
        this.setState({error: r, doingReply: false})
      }
      if (parsed) {
        if ((r.error || r.errors)) {
          console.log((r.error || r.errors))
          this.setState({error: (r.error || r.errors), doingReply: false})
        } else {
          this.setState({doingReply: false})
          this.onChange("")
          let page = this.props.page || 1
          let url = "/showthread.php?t=" + this.props.thread + "&page=" + page + "&p=" + (r.id || this.props.edit || "") + (this.props.appState.autosub !== "never" ? "&do=subscribe" : "")
          console.log("url:", url)
          this.props.history.push(url)
          //window.location.href = window.location.href = "/showthread.php?t=" + this.props.thread + "&page=" + page + "&p=" + (r.id || this.props.edit || "") + (this.props.appState.autosub !== "never" ? "&do=subscribe" : "")
        }
      }
    }).catch(e=>{
      console.log("POSTING EXCEPTION", e)
      this.setState({error: e.message, doingReply: false})
    })
  }

  onChange(text) {
    if (this.props.text !== undefined) {
      this.props.onChange(text)
    } else {
      this.setState({text: text})
    }
    let start = this.ref.current?.selectionStart
    let namestart
    for (let i = start - 1; i >= 0; --i) {
      if (text[i] === " ") {
        break;
      }
      if (text[i] === "@") {
        namestart = i + 1
      }
    }
    if (namestart) {
      searchUser(this.props.appState.api, text.substring(namestart, start), (r) => {
        this.setState({ numFetchedUsers: r.totalUsers, fetchedUsers: r.users })
      })
    } else if (this.state.numFetchedUsers !== 0) {
      this.setState({ numFetchedUsers: 0, fetchedUsers: [] })
    }
  }

  render() {
    const GoAdvancedButton = withRouter(({ history }) => <button onClick={()=>{
      history.push(this.props.goAdvanced)
    }}>Go Advanced</button>)
    let text = this.props.text
    if (text === undefined) {
      text = this.state.text
    }
    return <div className="post-editor">
      {!this.state.preview &&
        <div className="reply-box">
          <textarea ref={this.ref} value={text} autoComplete="on" id="post" onChange={e=>this.onChange(e.target.value)}/>
          {this.state.numFetchedUsers > 0 && <div className="user-search">
            <ul>
              {this.state.fetchedUsers.map(u => {
                return <li key={u.id}>
                  <Username href="" user={u} onClick={(e)=>{
                    e.preventDefault()
                    let start = this.ref.current?.selectionStart
                    let namestart
                    for (let i = start - 1; i >= 0; --i) {
                      if (text[i] === " ") {
                        break;
                      }
                      if (text[i] === "@") {
                        namestart = i + 1
                      }
                    }
                    if (namestart) {
                      this.onChange(text.substring(0, namestart) + "<" + u.id + ">" + text.substring(start))
                      this.ref.current?.focus()
                    }
                  }}/>
                </li>
              })}
            </ul>
          </div>}
          {tag_buttons.map((v, k)=>{
            const TagButton = v.render
            if (TagButton) {
              return <TagButton tag={v} text={text} editor={this.ref} onChange={this.onChange}/>
            }
            return <LinkWrapper href="" key={v.title} onClick={(e)=>{
              e.preventDefault()
              let start = this.ref.current.selectionStart
              let end = this.ref.current.selectionEnd

              let before = (this.props.text || this.state.text).substring(0, start)
              let selected = (this.props.text || this.state.text).substring(start, end) 
              let after = (this.props.text || this.state.text).substring(end)

              if (v.fun) {
                this.onChange(v.fun(before, selected, after))
              } else {
                this.onChange(before + "[" + v.tag + "]" + selected + "[/" + v.tag + "]" + after)
              }
            }}>
              <img className={"icon icon-" + v.tag} src={v.img} title={v.title}/>
            </LinkWrapper>
          })}
        </div>
      }
      {this.state.preview &&
        <Post data={{
          content: this.state.emotesDisabled ? text : emotify(text),
          user: this.props.appState.user,
        }} number={1} thread={1} threadRead locked={false} page={1} noRatings noButtons noReport subforum={1} appState={this.props.appState} appSetState={this.props.appSetState}/>
      }
      <div className="reply-buttons">
        <button accessKey="s" disabled={this.props.doingReply || this.state.doingReply} onClick={()=>{
          if (this.props.onSubmit) {
            this.props.onSubmit(this.state.emotesDisabled ? text : emotify(text))
          } else {
            this.doReply(this.state.emotesDisabled ? text : emotify(text))
          }
        }}>Reply</button>
        <button onClick={()=>{this.setState({preview: !this.state.preview})}}>Toggle Preview</button>
        {this.props.goAdvanced && <GoAdvancedButton/>}
        <button onClick={()=>{
          if (this.props.onCancel) {
            this.props.onCancel()
          } else {
            this.onChange("")
            window.history.back()
          }
        }}>Cancel</button>
      </div>
      {(this.props.error || this.state.error) && <div className="error">{this.props.error || this.state.error}</div>}
      <input type="checkbox" id="emotes" checked={this.state.emotesDisabled} onChange={(e)=>{
        this.setState({emotesDisabled: e.target.checked})
      }}/>
      <label htmlFor="emotes">Disable <LinkWrapper href="/misc.php">emoticons</LinkWrapper> for this post</label>
    </div>
  }
}
export default withRouter(PostEditor)

