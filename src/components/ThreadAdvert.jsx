import React, { Component } from "react";
import LinkWrapper from './LinkWrapper'
import ImgWrapper from './ImgWrapper'

const regexquery = /query/g
const regeximage = /image/g

export default class ThreadAdvert extends Component {
  constructor(props) {
    super(props)
    this.state = {
      image: "/static/threadad.png",
      query: "search query",
      title: "Random Thread Ad",
    }
  }
  componentDidMount() {
    window.fetch(this.props.appState.api + "/threadAds/random").then(r=>r.json()).then((r)=>{
      console.log(r)
      this.setState({image: r.imageUrl, query: r.query, title: r.description})
    })
  }
  render() {
    return <div className="thread-advert">
      <LinkWrapper href={"search.php?q=" + this.state.query} title={this.state.title}>
        <ImgWrapper src={this.state.image}/>
      </LinkWrapper>
    </div>
  }
}
