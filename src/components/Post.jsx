import React, { Component } from "react"
import TimeElapsed, { elapsedString, TimeDuration } from './TimeElapsed'
import LinkWrapper from './LinkWrapper'
import ImgWrapper from './ImgWrapper'
import YouTube from './YouTube'
import Username, { getUserTitleOrGroupName } from './Username'
import { BanEntry } from './views/member'
import Urls from 'my-name-is-url';
import SyntaxHighlighter from 'react-syntax-highlighter'
import { monokai } from 'react-syntax-highlighter/dist/esm/styles/hljs'
import Rating, { ratings } from './Rating'
import { getUserProfile } from "./api/user"

const { getCode, getName } = require('country-list');
const slugfinder = /\w+$/
const appIcons = {}
appIcons["kopunch"] = "favicon.ico"
// appIcons["knockout.chat"] = "knockout.chat.ico",
appIcons["lite.knockout.chat"] = "lite.knockout.chat.gif"
appIcons["knocky"] = "knocky.png"

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

var jsonp = require('jsonp')

function ClickToLoad(props) {
  let [hidden, setHidden] = React.useState(props.options && props.options.datasaver)

  console.log("ClickToLoad", props, hidden)
  if (hidden) {
    return <a href="#" onClick={(e)=>{
      e.preventDefault()
      setHidden(false)
    }}>Click to load {props.what}</a>
  }
  return props.children
}

class TwitterEmbed extends Component {
  constructor(props) {
    super(props)
    this.render = this.render.bind(this)
    this.componentDidMount = this.componentDidMount.bind(this)
    this.ref = React.createRef()
    this.state = {
      loaded: false
    }
    let match = props.src && props.src.match(/https:\/\/([^\.]+\.|)?twitter.com\/[^\/]+\/status\/\d+/g)
    if (match)
      this.src = match[0]
    console.log("TWITTER", props.src, match)
    //this.src = match[0]
  }
  componentDidMount() {
    let dark = window.getComputedStyle(document.body).content === '"dark"'
    jsonp("https://publish.twitter.com/oembed?omit_script=true&dnt=true&theme=" + (dark ? "dark" : "light") + "&url=" + this.src, null, (err, data)=>{
      if (err)
        return
      this.data = data
      this.setState({loaded: true})
      console.log(data)
      twttr.widgets.load(this.ref.current)
    })
  }
  render() {
    if (this.state.loaded)
      return <div ref={this.ref} className="twitter" dangerouslySetInnerHTML={{ __html: this.data.html}}/>
    else
      return <div className="twitter"><LinkWrapper external href={this.props.src}>{this.props.src}</LinkWrapper></div>
  }
}

function Thumb(props) {
  let [active, setActive] = React.useState(false)
  let [isThumb, setThumb] = React.useState(props.isThumb)
  let ref = React.useRef(null)

  function keydown(e) {
    if (e.code == "Escape") {
      setActive(false)
    }
  }

  function toggleActive() {
    if (active) {
      document.removeEventListener("keydown", keydown)
    } else {
      document.addEventListener("keydown", keydown)
    }
    setActive(!active)
  }

  function checkHeight() {
    if (ref.current && !isThumb) {
      console.log("checkHeight", ref.current, ref.current.offsetHeight, window.screen.height)
      if (ref.current.offsetHeight) {
        if (ref.current.offsetHeight > window.screen.height) {
          setThumb(true)
        }
      } else {
        //window.setTimeout(checkHeight, 1000)
      } 
      if (ref.current.closest('.quote')) {
        setThumb(true)
      }
    }
  }

  React.useLayoutEffect(checkHeight)
  if (isThumb) {
    return <div ref={ref} className="thumb">
      <a href={props.src} className="no-outline" onClick={(e)=>{e.preventDefault()}}>
        <ImgWrapper className="thumb" src={props.src} title={props.title} alt={props.alt} onLoad={checkHeight} onClick={(e)=>{
          e.preventDefault()
          toggleActive()
        }}/>
      </a>
      {active && <div className="thumb-overlay" onClick={(e)=>{
        if (e.currentTarget == e.target)
          toggleActive()
      }}>
        <a href={props.src} className="no-outline" onClick={(e)=>{e.preventDefault()}}>
          <ImgWrapper src={props.src} title={props.title} alt={props.alt} onLoad={checkHeight} onClick={(e)=>{
            e.preventDefault()
            toggleActive()
          }}/>
        </a>
      </div>}
    </div>
  } else {
    return <ImgWrapper innerRef={ref} onLoad={checkHeight} src={props.src}/>
  }
}

export const emotes = [
  { "EmoteId": 1, "Name": "Smile", "Url": "https://kopunch.club/static/emotes/newpunch/smile.png", "Code": ":)", "Width": "17", "Height": "17" },
  { "EmoteId": 2, "Name": "Shock", "Url": "https://kopunch.club/static/emotes/newpunch/shock.gif", "Code": ":o", "Width": "17", "Height": "26" },
  { "EmoteId": 3, "Name": "Gross", "Url": "https://kopunch.club/static/emotes/newpunch/gross.png", "Code": ":gross:", "Width": "17", "Height": "17" },
  { "EmoteId": 5, "Name": "D-Smiley", "Url": "https://kopunch.club/static/emotes/newpunch/happy.png", "Code": ":D", "Width": "17", "Height": "17" },
  { "EmoteId": 6, "Name": "Sad", "Url": "https://kopunch.club/static/emotes/newpunch/sad.png", "Code": ":(", "Width": "17", "Height": "17" },
  { "EmoteId": 7, "Name": "Cool", "Url": "https://kopunch.club/static/emotes/newpunch/cool.gif", "Code": ":cool:", "Width": "17", "Height": "17" },
  { "EmoteId": 8, "Name": "Zipped", "Url": "https://kopunch.club/static/emotes/newpunch/zipped.png", "Code": ":x", "Width": "17", "Height": "17" },
  { "EmoteId": 9, "Name": "Cry", "Url": "https://kopunch.club/static/emotes/newpunch/cry.gif", "Code": ":'(", "Width": "17", "Height": "17" },
  { "EmoteId": 10, "Name": "Wow", "Url": "https://kopunch.club/static/emotes/newpunch/wow.gif", "Code": ":wow:", "Width": "28", "Height": "17" },
  { "EmoteId": 11, "Name": "Excited", "Url": "https://kopunch.club/static/emotes/newpunch/excited.gif", "Code": ":excited:", "Width": "25", "Height": "25" },
  { "EmoteId": 12, "Name": "V", "Url": "https://kopunch.club/static/emotes/newpunch/v.png", "Code": ":v:", "Width": "17", "Height": "17" },
  { "EmoteId": 14, "Name": "Fap", "Url": "https://kopunch.club/static/emotes/newpunch/fap.gif", "Code": ":fap:", "Width": "22", "Height": "17" },
  { "EmoteId": 15, "Name": "Huh", "Url": "https://kopunch.club/static/emotes/newpunch/huh.gif", "Code": ":huh:", "Width": "23", "Height": "26" },
  { "EmoteId": 16, "Name": "Rolleyes", "Url": "https://kopunch.club/static/emotes/newpunch/rolleyes.gif", "Code": ":rolleyes:", "Width": "17", "Height": "17" },
  { "EmoteId": 17, "Name": "Words", "Url": "https://kopunch.club/static/emotes/newpunch/words.gif", "Code": ":words:", "Width": "77", "Height": "17" },
  { "EmoteId": 18, "Name": "Quagmire", "Url": "https://kopunch.club/static/emotes/newpunch/quagmire.gif", "Code": ":quagmire:", "Width": "22", "Height": "24" },
  { "EmoteId": 19, "Name": "Dance", "Url": "https://kopunch.club/static/emotes/newpunch/dance.gif", "Code": ":dance:", "Width": "25", "Height": "19" },
  { "EmoteId": 20, "Name": "Postal's Ass", "Url": "https://kopunch.club/static/emotes/newpunch/ass.png", "Code": ":ass:", "Width": "25", "Height": "25" },
  { "EmoteId": 21, "Name": "Mushroom", "Url": "https://kopunch.club/static/emotes/newpunch/mushroom.gif", "Code": ":mushroom:", "Width": "16", "Height": "16" },
  { "EmoteId": 22, "Name": "Coin", "Url": "https://kopunch.club/static/emotes/newpunch/coin.gif", "Code": ":coin:", "Width": "12", "Height": "16" },
  { "EmoteId": 23, "Name": "Wok", "Url": "https://kopunch.club/static/emotes/newpunch/wok.png", "Code": ":wok:", "Width": "52", "Height": "23" },
  { "EmoteId": 24, "Name": "Saddowns", "Url": "https://kopunch.club/static/emotes/newpunch/saddowns.png", "Code": ":saddowns:", "Width": "17", "Height": "17" },
  { "EmoteId": 25, "Name": "Garry Spin", "Url": "https://kopunch.club/static/emotes/newpunch/garryspin.gif", "Code": ":garry:", "Width": "20", "Height": "25" },
  { "EmoteId": 26, "Name": "Todd Howard", "Url": "https://kopunch.club/static/emotes/todd.png", "Code": ":todd:", "Width": "48", "Height": "17" },
  { "EmoteId": 27, "Name": "Smug", "Url": "https://kopunch.club/static/emotes/oldpunch/smug.png", "Code": ":smug:", "Width": "17", "Height": "17" },
  { "EmoteId": 28, "Name": "Speechless", "Url": "https://kopunch.club/static/emotes/oldpunch/speechless.gif", "Code": ":speechless:", "Width": "30", "Height": "19" },
  { "EmoteId": 29, "Name": "Dead", "Url": "https://kopunch.club/static/emotes/oldpunch/dead.png", "Code": ":dead:", "Width": "17", "Height": "17" },
  { "EmoteId": 30, "Name": "Ohno", "Url": "https://kopunch.club/static/emotes/oldpunch/ohno.png", "Code": ":ohno:", "Width": "17", "Height": "17" },
  { "EmoteId": 31, "Name": "Quotes", "Url": "https://kopunch.club/static/emotes/oldpunch/quotes.gif", "Code": ":quotes:", "Width": "33", "Height": "17" },
  { "EmoteId": 32, "Name": "What", "Url": "https://kopunch.club/static/emotes/oldpunch/what.gif", "Code": ":what:", "Width": "50", "Height": "17" },
  { "EmoteId": 33, "Name": "Smile", "Url": "https://kopunch.club/static/emotes/oldpunch/smile.png", "Code": ":smile:", "Width": "17", "Height": "17" },
  { "EmoteId": 34, "Name": "Goodjob", "Url": "https://kopunch.club/static/emotes/oldpunch/goodjob.gif", "Code": ":goodjob:", "Width": "49", "Height": "18" },
  { "EmoteId": 35, "Name": "Sick", "Url": "https://kopunch.club/static/emotes/oldpunch/sick.png", "Code": ":sick:", "Width": "17", "Height": "17" },
  { "EmoteId": 36, "Name": "Cry", "Url": "https://kopunch.club/static/emotes/oldpunch/cry.gif", "Code": ":cry:", "Width": "17", "Height": "17" },
  { "EmoteId": 37, "Name": "Nope", "Url": "https://kopunch.club/static/emotes/oldpunch/nope.gif", "Code": ":nope:", "Width": "42", "Height": "20" },
  { "EmoteId": 38, "Name": "Pudge", "Url": "https://kopunch.club/static/emotes/oldpunch/pudge.png", "Code": ":pudge:", "Width": "17", "Height": "17" },
  { "EmoteId": 39, "Name": "Weeb", "Url": "https://kopunch.club/static/emotes/oldpunch/weeb.png", "Code": ":weeb:", "Width": "17", "Height": "17" },
  { "EmoteId": 40, "Name": "Funny", "Url": "https://kopunch.club/static/emotes/oldpunch/v.png", "Code": ":v:", "Width": "17", "Height": "17" },
  { "EmoteId": 41, "Name": "God Zing", "Url": "https://kopunch.club/static/emotes/oldpunch/godzing.gif", "Code": ":godzing:", "Width": "42", "Height": "17" },
  { "EmoteId": 42, "Name": "Shock", "Url": "https://kopunch.club/static/emotes/oldpunch/shock.png", "Code": ":shock:", "Width": "17", "Height": "17" },
  { "EmoteId": 43, "Name": "Yarr", "Url": "https://kopunch.club/static/emotes/oldpunch/yarr.gif", "Code": ":yarr:", "Width": "46", "Height": "28" },
  { "EmoteId": 44, "Name": "Cool", "Url": "https://kopunch.club/static/emotes/oldpunch/cool.gif", "Code": ":cool:", "Width": "17", "Height": "17" },
  { "EmoteId": 45, "Name": "Payne", "Url": "https://kopunch.club/static/emotes/oldpunch/payne.gif", "Code": ":payne:", "Width": "32", "Height": "20" },
  { "EmoteId": 46, "Name": "Mindblown", "Url": "https://kopunch.club/static/emotes/oldpunch/mindblown.gif", "Code": ":mindblown:", "Width": "63", "Height": "48" },
  { "EmoteId": 47, "Name": "Wavey", "Url": "https://kopunch.club/static/emotes/oldpunch/wavey.gif", "Code": ":wavey:", "Width": "17", "Height": "17" },
  { "EmoteId": 48, "Name": "Shh", "Url": "https://kopunch.club/static/emotes/oldpunch/shh.gif", "Code": ":shh:", "Width": "23", "Height": "22" },
  { "EmoteId": 49, "Name": "Suicide", "Url": "https://kopunch.club/static/emotes/oldpunch/suicide.gif", "Code": ":suicide:", "Width": "48", "Height": "34" },
  { "EmoteId": 50, "Name": "Glare", "Url": "https://kopunch.club/static/emotes/oldpunch/glare.png", "Code": ":glare:", "Width": "17", "Height": "17" },
  { "EmoteId": 51, "Name": "Scream", "Url": "https://kopunch.club/static/emotes/oldpunch/scream.png", "Code": ":scream:", "Width": "17", "Height": "17" },
  { "EmoteId": 52, "Name": "Words", "Url": "https://kopunch.club/static/emotes/oldpunch/words.gif", "Code": ":words:", "Width": "77", "Height": "17" },
  { "EmoteId": 53, "Name": "Clown", "Url": "https://kopunch.club/static/emotes/oldpunch/clown.png", "Code": ":clown:", "Width": "27", "Height": "18" },
  { "EmoteId": 54, "Name": "Ninja", "Url": "https://kopunch.club/static/emotes/oldpunch/ninja.gif", "Code": ":ninja:", "Width": "21", "Height": "21" },
  { "EmoteId": 55, "Name": "Joy", "Url": "https://kopunch.club/static/emotes/oldpunch/joy.gif", "Code": ":joy:", "Width": "41", "Height": "26" },
  { "EmoteId": 56, "Name": "VR react", "Url": "https://kopunch.club/static/emotes/oldpunch/vrreact.gif", "Code": ":vrreact:", "Width": "35", "Height": "24" },
  { "EmoteId": 57, "Name": "Tinfoil", "Url": "https://kopunch.club/static/emotes/oldpunch/tinfoil.gif", "Code": ":tinfoil:", "Width": "36", "Height": "23" },
  { "EmoteId": 58, "Name": "Dance", "Url": "https://kopunch.club/static/emotes/oldpunch/dance.gif", "Code": ":dance:", "Width": "25", "Height": "19" },
  { "EmoteId": 59, "Name": "Frown", "Url": "https://kopunch.club/static/emotes/oldpunch/frown.png", "Code": ":frown:", "Width": "17", "Height": "17" },
  { "EmoteId": 60, "Name": "Saxout", "Url": "https://kopunch.club/static/emotes/oldpunch/saxout.gif", "Code": ":saxout:", "Width": "37", "Height": "17" },
  { "EmoteId": 61, "Name": "Why", "Url": "https://kopunch.club/static/emotes/oldpunch/why.png", "Code": ":why:", "Width": "36", "Height": "17" },
  { "EmoteId": 62, "Name": "Class", "Url": "https://kopunch.club/static/emotes/oldpunch/class.png", "Code": ":class:", "Width": "26", "Height": "25" },
  { "EmoteId": 63, "Name": "Magic 101", "Url": "https://kopunch.club/static/emotes/oldpunch/magic101.gif", "Code": ":magic101:", "Width": "44", "Height": "25" },
  { "EmoteId": 64, "Name": "JohnnyMo1", "Url": "https://kopunch.club/static/emotes/oldpunch/johnnymo1.gif", "Code": ":johnnymo1:", "Width": "90", "Height": "27" },
  { "EmoteId": 65, "Name": "Unimpressed", "Url": "https://kopunch.club/static/emotes/oldpunch/unimpressed.png", "Code": ":unimpressed:", "Width": "17", "Height": "17" },
  { "EmoteId": 66, "Name": "Surrender", "Url": "https://kopunch.club/static/emotes/oldpunch/surrender.gif", "Code": ":surrender:", "Width": "29", "Height": "21" },
  { "EmoteId": 67, "Name": "Rock", "Url": "https://kopunch.club/static/emotes/oldpunch/rock.gif", "Code": ":rock:", "Width": "45", "Height": "18" },
  { "EmoteId": 68, "Name": "Flex", "Url": "https://kopunch.club/static/emotes/oldpunch/flex.gif", "Code": ":flex:", "Width": "31", "Height": "22" },
  { "EmoteId": 69, "Name": "Sax", "Url": "https://kopunch.club/static/emotes/oldpunch/sax.png", "Code": ":sax:", "Width": "27", "Height": "17" },
  { "EmoteId": 70, "Name": "Vomit", "Url": "https://kopunch.club/static/emotes/oldpunch/vomit.gif", "Code": ":vomit:", "Width": "40", "Height": "17" },
  { "EmoteId": 71, "Name": "Cat", "Url": "https://kopunch.club/static/emotes/oldpunch/cat.gif", "Code": ":cat:", "Width": "17", "Height": "17" },
  { "EmoteId": 72, "Name": "Idiot Cull", "Url": "https://kopunch.club/static/emotes/oldpunch/idiotcull.gif", "Code": ":idiotcull:", "Width": "64", "Height": "24" },
  { "EmoteId": 73, "Name": "Evil", "Url": "https://kopunch.club/static/emotes/oldpunch/evil.png", "Code": ":evil:", "Width": "18", "Height": "17" },
  { "EmoteId": 74, "Name": "Jawdrop", "Url": "https://kopunch.club/static/emotes/oldpunch/jawdrop.gif", "Code": ":jawdrop:", "Width": "17", "Height": "26" },
  { "EmoteId": 75, "Name": "Trumpet", "Url": "https://kopunch.club/static/emotes/oldpunch/trumpet.gif", "Code": ":trumpet:", "Width": "36", "Height": "20" },
  { "EmoteId": 76, "Name": "Worried", "Url": "https://kopunch.club/static/emotes/oldpunch/worried.gif", "Code": ":worried:", "Width": "17", "Height": "17" },
  { "EmoteId": 77, "Name": "Fap", "Url": "https://kopunch.club/static/emotes/oldpunch/fap.gif", "Code": ":fap:", "Width": "22", "Height": "17" },
  { "EmoteId": 78, "Name": "Confused", "Url": "https://kopunch.club/static/emotes/oldpunch/s.png", "Code": ":s:", "Width": "17", "Height": "17" },
  { "EmoteId": 79, "Name": "This Thread", "Url": "https://kopunch.club/static/emotes/oldpunch/thisthread.gif", "Code": ":thisthread:", "Width": "86", "Height": "27" },
  { "EmoteId": 80, "Name": "Buckteeth", "Url": "https://kopunch.club/static/emotes/oldpunch/buckteeth.png", "Code": ":buckteeth:", "Width": "17", "Height": "17" },
  { "EmoteId": 81, "Name": "Hidden Downs", "Url": "https://kopunch.club/static/emotes/oldpunch/hiddendowns.gif", "Code": ":hiddendowns:", "Width": "17", "Height": "17" },
  { "EmoteId": 82, "Name": "Buddy", "Url": "https://kopunch.club/static/emotes/oldpunch/buddy.png", "Code": ":buddy:", "Width": "17", "Height": "17" },
  { "EmoteId": 83, "Name": "English 101", "Url": "https://kopunch.club/static/emotes/oldpunch/eng101.png", "Code": ":eng101:", "Width": "33", "Height": "22" },
  { "EmoteId": 84, "Name": "Hurr", "Url": "https://kopunch.club/static/emotes/oldpunch/hurr.gif", "Code": ":hurr:", "Width": "49", "Height": "23" },
  { "EmoteId": 85, "Name": "Toot", "Url": "https://kopunch.club/static/emotes/oldpunch/toot.gif", "Code": ":toot:", "Width": "29", "Height": "25" },
  { "EmoteId": 86, "Name": "Rolleyes", "Url": "https://kopunch.club/static/emotes/oldpunch/rolleyes.gif", "Code": ":rolleyes:", "Width": "17", "Height": "17" },
  { "EmoteId": 87, "Name": "Facemelt", "Url": "https://kopunch.club/static/emotes/oldpunch/facemelt.gif", "Code": ":facemelt:", "Width": "17", "Height": "17" },
  { "EmoteId": 88, "Name": "Rollout", "Url": "https://kopunch.club/static/emotes/oldpunch/rollout.gif", "Code": ":rollout:", "Width": "17", "Height": "17" },
  { "EmoteId": 89, "Name": "Tableflip", "Url": "https://kopunch.club/static/emotes/oldpunch/tableflip.gif", "Code": ":tableflip:", "Width": "60", "Height": "23" },
  { "EmoteId": 90, "Name": "Boxhide", "Url": "https://kopunch.club/static/emotes/oldpunch/boxhide.gif", "Code": ":boxhide:", "Width": "21", "Height": "21" },
  { "EmoteId": 91, "Name": "Freakout", "Url": "https://kopunch.club/static/emotes/oldpunch/freakout.gif", "Code": ":freakout:", "Width": "20", "Height": "19" },
  { "EmoteId": 92, "Name": "Chemistry 101", "Url": "https://kopunch.club/static/emotes/oldpunch/chem101.gif", "Code": ":chem101:", "Width": "28", "Height": "22" },
  { "EmoteId": 93, "Name": "Huh", "Url": "https://kopunch.club/static/emotes/oldpunch/huh.gif", "Code": ":huh:", "Width": "23", "Height": "26" },
  { "EmoteId": 94, "Name": "Tired", "Url": "https://kopunch.club/static/emotes/oldpunch/tired.gif", "Code": ":tired:", "Width": "17", "Height": "17" },
  { "EmoteId": 95, "Name": "Excited", "Url": "https://kopunch.club/static/emotes/oldpunch/excited.gif", "Code": ":excited:", "Width": "25", "Height": "25" },
  { "EmoteId": 96, "Name": "Poot", "Url": "https://kopunch.club/static/emotes/oldpunch/poot.gif", "Code": ":poot:", "Width": "26", "Height": "25" },
  { "EmoteId": 97, "Name": "Sex 101", "Url": "https://kopunch.club/static/emotes/oldpunch/sex101.gif", "Code": ":sex101:", "Width": "34", "Height": "22" },
  { "EmoteId": 98, "Name": "Blaze", "Url": "https://kopunch.club/static/emotes/oldpunch/blaze.gif", "Code": ":blaze:", "Width": "23", "Height": "26" },
  { "EmoteId": 99, "Name": "Zing", "Url": "https://kopunch.club/static/emotes/oldpunch/zing.gif", "Code": ":zing:", "Width": "42", "Height": "17" },
  { "EmoteId": 100, "Name": "Wink", "Url": "https://kopunch.club/static/emotes/oldpunch/wink.png", "Code": ":wink:", "Width": "17", "Height": "17" },
  { "EmoteId": 101, "Name": "Happy", "Url": "https://kopunch.club/static/emotes/oldpunch/happy.png", "Code": ":happy:", "Width": "17", "Height": "17" },
  { "EmoteId": 102, "Name": "That was funny right?", "Url": "https://kopunch.club/static/emotes/oldpunch/thatwasfunnyright.gif", "Code": ":thatwasfunnyright:", "Width": "26", "Height": "20" },
  { "EmoteId": 103, "Name": "Disgust", "Url": "https://kopunch.club/static/emotes/oldpunch/disgust.gif", "Code": ":disgust:", "Width": "17", "Height": "17" },
  { "EmoteId": 104, "Name": "Drool", "Url": "https://kopunch.club/static/emotes/oldpunch/drool.gif", "Code": ":drool:", "Width": "23", "Height": "17" },
  { "EmoteId": 105, "Name": "PC Repair", "Url": "https://kopunch.club/static/emotes/oldpunch/pcrepair.gif", "Code": ":pcrepair:", "Width": "38", "Height": "17" },
  { "EmoteId": 106, "Name": "Science 101", "Url": "https://kopunch.club/static/emotes/oldpunch/science101.gif", "Code": ":science101:", "Width": "48", "Height": "26" },
  { "EmoteId": 107, "Name": "Bad Zing", "Url": "https://kopunch.club/static/emotes/oldpunch/badzing.gif", "Code": ":badzing:", "Width": "48", "Height": "17" },
  { "EmoteId": 108, "Name": "Wow", "Url": "https://kopunch.club/static/emotes/oldpunch/wow.gif", "Code": ":wow:", "Width": "28", "Height": "17" },
  { "EmoteId": 109, "Name": "PWN", "Url": "https://kopunch.club/static/emotes/oldpunch/pwn.png", "Code": ":pwn:", "Width": "17", "Height": "17" },
  { "EmoteId": 110, "Name": "Hammered", "Url": "https://kopunch.club/static/emotes/oldpunch/hammered.gif", "Code": ":hammered:", "Width": "28", "Height": "19" },
  { "EmoteId": 111, "Name": "Speedfap", "Url": "https://kopunch.club/static/emotes/oldpunch/speedfap.gif", "Code": ":speedfap:", "Width": "22", "Height": "17" },
  { "EmoteId": 112, "Name": "Downs", "Url": "https://kopunch.club/static/emotes/oldpunch/downs.png", "Code": ":downs:", "Width": "17", "Height": "17" },
  { "EmoteId": 113, "Name": "Disappointed", "Url": "https://kopunch.club/static/emotes/oldpunch/disappoint.gif", "Code": ":disappoint:", "Width": "17", "Height": "17" },
  { "EmoteId": 114, "Name": "Over Your Head", "Url": "https://kopunch.club/static/emotes/oldpunch/overyourhead.gif", "Code": ":overyourhead:", "Width": "29", "Height": "25" },
  { "EmoteId": 115, "Name": "Sad Downs", "Url": "https://kopunch.club/static/emotes/oldpunch/saddowns.png", "Code": ":saddowns:", "Width": "17", "Height": "17" },
  { "EmoteId": 116, "Name": "Angry", "Url": "https://kopunch.club/static/emotes/oldpunch/angry.png", "Code": ":angry:", "Width": "17", "Height": "17" },
  { "EmoteId": 117, "Name": "Wideeye", "Url": "https://kopunch.club/static/emotes/oldpunch/wideeye.png", "Code": ":wideeye:", "Width": "17", "Height": "17" },
  { "EmoteId": 118, "Name": "Neat", "Url": "https://kopunch.club/static/emotes/oldpunch/neat.gif", "Code": ":neat:", "Width": "56", "Height": "25" },
  { "EmoteId": 119, "Name": "Hairpull", "Url": "https://kopunch.club/static/emotes/oldpunch/hairpull.gif", "Code": ":hairpull:", "Width": "27", "Height": "26" },
  { "EmoteId": 120, "Name": "Incredible", "Url": "https://kopunch.club/static/emotes/oldpunch/incredible.gif", "Code": ":incredible:", "Width": "110", "Height": "25" },
  { "EmoteId": 121, "Name": "Toad Leave", "Url": "https://kopunch.club/static/emotes/oldpunch/toadleave.gif", "Code": ":toadleave:", "Width": "116", "Height": "25" },
  { "EmoteId": 122, "Name": "Garry Spin", "Url": "https://kopunch.club/static/emotes/oldpunch/garryspin.gif", "Code": ":garryspin:", "Width": "20", "Height": "25" },
  { "EmoteId": 123, "Name": "Stop", "Url": "https://kopunch.club/static/emotes/oldpunch/stop.png", "Code": ":stop:", "Width": "25", "Height": "25" },
  { "EmoteId": 124, "Name": "FP", "Url": "https://kopunch.club/static/emotes/oldpunch/fp.gif", "Code": ":fp:", "Width": "23", "Height": "23" },
  { "EmoteId": 125, "Name": "rrerr", "Url": "https://kopunch.club/static/emotes/oldpunch/rrerr.gif", "Code": ":rrerr:", "Width": "35", "Height": "10" },
  { "EmoteId": 126, "Name": "Sarcastic Informative", "Url": "https://kopunch.club/static/emotes/oldpunch/eng101s.png", "Code": ":eng101s:", "Width": "23", "Height": "19" },
  { "EmoteId": 127, "Name": "Clickbait", "Url": "https://kopunch.club/static/emotes/oldpunch/clickbait.gif", "Code": ":clickbait:", "Width": "110", "Height": "23" },
  { "EmoteId": 128, "Name": "Postal's Ass", "Url": "https://kopunch.club/static/emotes/oldpunch/ass.png", "Code": ":ass:", "Width": "25", "Height": "25" },
  { "EmoteId": 129, "Name": "Bullshit", "Url": "https://kopunch.club/static/emotes/oldpunch/bullshit.gif", "Code": ":bullshit:", "Width": "90", "Height": "14" },
  { "EmoteId": 130, "Name": "-snip-", "Url": "https://kopunch.club/static/emotes/oldpunch/snip.png", "Code": ":snip:", "Width": "64", "Height": "16" },
  { "EmoteId": 131, "Name": "Badage", "Url": "https://kopunch.club/static/emotes/oldpunch/badage.png", "Code": ":badage:", "Width": "26", "Height": "25" },
  { "EmoteId": 132, "Name": "sbhj", "Url": "https://kopunch.club/static/emotes/oldpunch/sbhj.png", "Code": ":sbhj:", "Width": "26", "Height": "23" },
  { "EmoteId": 133, "Name": "XFiles", "Url": "https://kopunch.club/static/emotes/oldpunch/xfiles.gif", "Code": ":xfiles:", "Width": "61", "Height": "11" },
  { "EmoteId": 134, "Name": "Q", "Url": "https://kopunch.club/static/emotes/oldpunch/q.png", "Code": ":q:", "Width": "23", "Height": "25" },
  { "EmoteId": 135, "Name": "Wok", "Url": "https://kopunch.club/static/emotes/oldpunch/wok.png", "Code": ":wok:", "Width": "52", "Height": "23" },
  { "EmoteId": 136, "Name": "OK", "Url": "https://kopunch.club/static/emotes/oldpunch/ok.png", "Code": ":ok:", "Width": "15", "Height": "17" },
  { "EmoteId": 137, "Name": "Sarcastic Funny", "Url": "https://kopunch.club/static/emotes/oldpunch/vs.png", "Code": ":vs:", "Width": "20", "Height": "18" },
  { "EmoteId": 138, "Name": "Lick", "Url": "https://kopunch.club/static/emotes/oldpunch/lick.gif", "Code": ":lick:", "Width": "31", "Height": "21" },
  { "EmoteId": 139, "Name": "You Tried", "Url": "https://kopunch.club/static/emotes/oldpunch/tried.gif", "Code": ":tried:", "Width": "30", "Height": "25" },
  { "EmoteId": 140, "Name": "asdfghjkl;'", "Url": "https://kopunch.club/static/emotes/oldpunch/asdfghjkl.gif", "Code": ":asdfghjkl;':", "Width": "65", "Height": "25" },
  { "EmoteId": 141, "Name": "VR", "Url": "https://kopunch.club/static/emotes/oldpunch/vr.gif", "Code": ":vr:", "Width": "35", "Height": "24" },
  { "EmoteId": 142, "Name": "Doubt", "Url": "https://kopunch.club/static/emotes/oldpunch/doubt.png", "Code": ":doubt:", "Width": "25", "Height": "24" },
  { "EmoteId": 143, "Name": "Terrists", "Url": "https://kopunch.club/static/emotes/oldpunch/terrists.gif", "Code": ":terrists:", "Width": "63", "Height": "35" },
  { "EmoteId": 144, "Name": "Dig", "Url": "https://kopunch.club/static/emotes/oldpunch/dig.gif", "Code": ":dig:", "Width": "35", "Height": "18" },
  { "EmoteId": 145, "Name": "Shitposting", "Url": "https://kopunch.club/static/emotes/oldpunch/shitposting.gif", "Code": ":shitposting:", "Width": "29", "Height": "22" },
  { "EmoteId": 146, "Name": "Dewritos", "Url": "https://kopunch.club/static/emotes/oldpunch/dewritos.gif", "Code": ":dewritos:", "Width": "60", "Height": "30" },
  { "EmoteId": 147, "Name": "Privilege", "Url": "https://kopunch.club/static/emotes/oldpunch/privilege.gif", "Code": ":privilege:", "Width": "100", "Height": "25" },
  { "EmoteId": 148, "Name": "Conspiratard", "Url": "https://kopunch.club/static/emotes/oldpunch/conspiratard.gif", "Code": ":conspiratard:", "Width": "70", "Height": "37" },
  { "EmoteId": 149, "Name": "Pride", "Url": "https://kopunch.club/static/emotes/oldpunch/pride.gif", "Code": ":pride:", "Width": "32", "Height": "32" },
  { "EmoteId": 150, "Name": "Chill Out", "Url": "https://kopunch.club/static/emotes/oldpunch/chillout.gif", "Code": ":chillout:", "Width": "120", "Height": "25" },
  { "EmoteId": 151, "Name": "Pls Understand", "Url": "https://kopunch.club/static/emotes/oldpunch/plsunderstand.png", "Code": ":plsunderstand:", "Width": "21", "Height": "25" },
  { "EmoteId": 152, "Name": "Mystery Solved", "Url": "https://kopunch.club/static/emotes/oldpunch/mysterysolved.png", "Code": ":mysterysolved:", "Width": "58", "Height": "23" },
  { "EmoteId": 153, "Name": "Mystery", "Url": "https://kopunch.club/static/emotes/oldpunch/mystery.gif", "Code": ":mystery:", "Width": "58", "Height": "23" },
  { "EmoteId": 154, "Name": "Hype is real", "Url": "https://kopunch.club/static/emotes/oldpunch/hypeisreal.gif", "Code": ":hypeisreal:", "Width": "80", "Height": "17" },
  { "EmoteId": 155, "Name": "Hype is not real", "Url": "https://kopunch.club/static/emotes/oldpunch/hypeisnotreal.gif", "Code": ":hypeisnotreal:", "Width": "120", "Height": "25" },
  { "EmoteId": 156, "Name": "Hoff", "Url": "https://kopunch.club/static/emotes/oldpunch/hoff.png", "Code": ":hoff:", "Width": "44", "Height": "23" },
  { "EmoteId": 157, "Name": "Pyramid", "Url": "https://kopunch.club/static/emotes/oldpunch/pyramid.png", "Code": ":pyramid:", "Width": "100", "Height": "25" },
  { "EmoteId": 158, "Name": "Dogcited", "Url": "https://kopunch.club/static/emotes/oldpunch/dogcited.gif", "Code": ":dogcited:", "Width": "25", "Height": "25" },
  { "EmoteId": 159, "Name": "Alien", "Url": "https://kopunch.club/static/emotes/oldpunch/alien.gif", "Code": ":alien:", "Width": "15", "Height": "17" },
  { "EmoteId": 160, "Name": "Dog", "Url": "https://kopunch.club/static/emotes/oldpunch/dog.png", "Code": ":dog:", "Width": "20", "Height": "19" },
  { "EmoteId": 161, "Name": "Toxx", "Url": "https://kopunch.club/static/emotes/oldpunch/toxx.gif", "Code": ":toxx:", "Width": "90", "Height": "26" },
  { "EmoteId": 162, "Name": "Dogsleep", "Url": "https://kopunch.club/static/emotes/oldpunch/dogsleep.gif", "Code": ":dogsleep:", "Width": "27", "Height": "12" },
  { "EmoteId": 163, "Name": "The Best", "Url": "https://kopunch.club/static/emotes/oldpunch/thebest.gif", "Code": ":thebest:", "Width": "100", "Height": "25" },
  { "EmoteId": 164, "Name": "NSFW", "Url": "https://kopunch.club/static/emotes/oldpunch/nsfw.gif", "Code": ":nsfw:", "Width": "96", "Height": "26" },
  { "EmoteId": 165, "Name": "Dogchill", "Url": "https://kopunch.club/static/emotes/oldpunch/dogchill.gif", "Code": ":dogchill:", "Width": "50", "Height": "37" },
  { "EmoteId": 166, "Name": "Russia", "Url": "https://kopunch.club/static/emotes/oldpunch/russia.gif", "Code": ":russia:", "Width": "63", "Height": "35" },
  { "EmoteId": 167, "Name": "Doghidden", "Url": "https://kopunch.club/static/emotes/oldpunch/doghidden.gif", "Code": ":doghidden:", "Width": "22", "Height": "22" },
  { "EmoteId": 168, "Name": "CRAZY ASS Sex 101", "Url": "https://kopunch.club/static/emotes/oldpunch/qsex101.gif", "Code": ":qsex101:", "Width": "40", "Height": "29" },
  { "EmoteId": 169, "Name": "Dogwow", "Url": "https://kopunch.club/static/emotes/oldpunch/dogwow.gif", "Code": ":dogwow:", "Width": "22", "Height": "21" },
  { "EmoteId": 170, "Name": "Zoid", "Url": "https://kopunch.club/static/emotes/oldpunch/zoid.gif", "Code": ":zoid:", "Width": "24", "Height": "25" },
  { "EmoteId": 171, "Name": "Quagmire", "Url": "https://kopunch.club/static/emotes/oldpunch/quagmire.gif", "Code": ":quagmire:", "Width": "22", "Height": "24" },
  { "EmoteId": 172, "Name": "What The Christ", "Url": "https://kopunch.club/static/emotes/sa/wtc.gif", "Code": ":wtc:", "Width": "58", "Height": "25" },
  { "EmoteId": 173, "Name": "Siren", "Url": "https://kopunch.club/static/emotes/sa/siren.gif", "Code": ":siren:", "Width": "20", "Height": "20" },
]

emotes.sort((a, b) => {
  if (a.Code.length > b.Code.length) {
    return -1
  } else if (a.Code.length < b.Code.length) {
    return 1
  }
  if (a.EmoteId < b.EmoteId) {
    return -1
  } else if (a.EmoteId > b.EmoteId) {
    return 1
  }
  return 0
})

export function emotify(text) {
  emotes.forEach((emote)=>{
    text = text.replaceAll(emote.Code, "[img]" + emote.Url + "[/img]")
  })
  return text
}

const tags = {}
const block_tags = {}
const noparse_tags = {}
tags.b = (obj, options)=>{
  const small = obj.content && obj.content[0] === "Edited:"
  return <b className={small ? "small" : ""}>{obj.content}</b>
}
tags.i = (obj, options)=>{
  return <i>{obj.content}</i>
}
tags.u = (obj, options)=>{
  return <u>{obj.content}</u>
}
tags.s = (obj, options)=>{
  return <s>{obj.content}</s>
}
block_tags.blockquote = true
tags.blockquote = (obj, options)=>{
  return <div className="block-quote quote">
    <div className="message">
      {obj.content}
    </div>
  </div>
}
block_tags.q = true
tags.q = tags.blockquote
tags.video = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0]
  return <ClickToLoad options={options} what={"video: " + src}>
    <video src={src} controls/>
  </ClickToLoad>
}
block_tags.quote = true
tags.quote = (obj, options)=>{
  if (!obj.attrs.username)
    return tags.blockquote(obj, options)
  return <div className="quote">
    <div className="info">
      <LinkWrapper href={"/showthread.php?t=" + obj.attrs.threadId + "&page=" + obj.attrs.threadPage + "&p=" + obj.attrs.postId}>{obj.attrs.username} posted:</LinkWrapper>
    </div>
    <div className="message">
      {obj.content}
    </div>
  </div>
}
block_tags.code = true
noparse_tags.code = true
tags.code = (obj, options)=>{
  let inline = obj.attrs.inline
  if (inline === undefined) {
    inline = Array.isArray(obj.content) ? !obj.content.find(e=>("" + e).includes('\n')) : !obj.content.includes('\n')
  }
  return <SyntaxHighlighter language={obj.attrs.language || "plaintext"} inline={inline} style={monokai} showLineNumbers={!inline} customStyle={{maxHeight: '400px', display: inline ? 'inline' : null, padding: inline ? '2px 4px' : null}} codeTagProps={{style: {textShadow: 'none'}}}>{obj.content.join ? obj.content.join("") : obj.content}</SyntaxHighlighter>
}
tags.spoiler = (obj, options)=>{
  return <span className="spoiler">{obj.content}</span>
}
block_tags.h1 = true
tags.h1 = (obj, options)=>{
  return <h1>{obj.content}</h1>
}
block_tags.h2 = true
tags.h2 = (obj, options)=>{
  return <h2>{obj.content}</h2>
}
tags.img = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0]
  return <ClickToLoad options={options} what={"image: " + src}>
    <Thumb isThumb={obj.attrs.thumbnail !== undefined || obj.attrs.thumb !== undefined} src={src}/>
  </ClickToLoad>
}
block_tags.youtube = true
tags.youtube = (obj, options)=>{
  return <ClickToLoad options={options} what="YouTube video">
    <YouTube src={obj.attrs.href || obj.content[0]}/>
  </ClickToLoad>
}
block_tags.vimeo = true
tags.vimeo = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  src = src.replace("vimeo.com", "player.vimeo.com/video").replace("http://", "https://")
  if (!src.startsWith("https://player.vimeo.com")) {
    return src
  }
  return <ClickToLoad options={options} what="Vimeo video">
    <iframe className="vimeo" src={src}/>
  </ClickToLoad>
}
block_tags.twitter = true
tags.twitter = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0]
  return <ClickToLoad options={options} what={"Twitter: " + src}>
    <TwitterEmbed src={src}/>
  </ClickToLoad>
}
tags.strawpoll = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  src = src.replace("strawpoll.me/", "strawpoll.me/embed_1/").replace("http://", "https://")
  src = src.replace("strawpoll.com/", "strawpoll.com/embed/").replace("http://", "https://")
  console.log("STRAWPOLL", src)
  if (!src.startsWith("https://www.strawpoll.me") && !src.startsWith("https://strawpoll.com")) {
    return src
  }
  return <ClickToLoad options={options} what="Strawpoll">
    <iframe className="strawpoll" src={src}/>
  </ClickToLoad>
}
tags.vocaroo = (obj, options)=>{
  let src = "" + (obj.attrs.href || obj.content[0] || "")
  console.log("vocasrc:", src)
  let id = src.match(slugfinder)
  if (id === null) {
    return src
  }
  return <ClickToLoad options={options} what="Vocaroo">
    <iframe width="300" height="60" src={"https://vocaroo.com/embed/" + id[0]} frameborder="0"></iframe>
  </ClickToLoad>
}
block_tags.streamable = true
tags.streamable = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  let id = src.match(slugfinder)
  if (id === null) {
    return src
  }
  return <ClickToLoad options={options} what="Streamable video">
    <div style={{width: '100%', height: '0px', position: 'relative', paddingBottom: '56.250%'}}>
      <iframe
        src={"https://streamable.com/e/" + id} 
        frameBorder="0" 
        width="100%" 
        height="100%" 
        allowFullScreen 
        style={{width: '100%', height: '100%', position: 'absolute'}}
      />
    </div>
  </ClickToLoad>
}
block_tags.ol = true
tags.ol = (obj, options)=>{
  return <ol>{obj.content}</ol>
}
block_tags.ul = true
tags.ul = (obj, options)=>{
  return <ul>{obj.content}</ul>
}
block_tags.li = true
tags.li = (obj, options)=>{
  return <li>{obj.content}</li>
}
tags.url = (obj, options)=>{
  console.log("url", obj)
  let src = obj.attrs.href || obj.content[0] || ""
  let href = src
  if (options.convertlinks) {
    if (!href.replace) {
      console.log("wtf", href)
    } else if (!href.includes("api.knockout.chat") && !href.includes("lite.knockout.chat")) {
      href = href.replace("knockout.chat/thread/", "kopunch.club/thread/")
    }
  }
  return <LinkWrapper external href={href} smart={obj.attrs.smart}>{obj.content.length !== 0 ? obj.content : src}</LinkWrapper>
}
noparse_tags.noparse = true
tags.noparse = (obj, options)=>{
  return obj.inner
}
tags.spotify = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  if (!src.startsWith("https://open.spotify.com/")) {
    return src
  }
  src = src.replace("https://open.spotify.com/", "https://open.spotify.com/embed/")
  return <ClickToLoad options={options} what="Spotify">
    <iframe className="spotify" src={src} width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
  </ClickToLoad>
}
tags.soundcloud = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  if (!src.startsWith("https://soundcloud.com/")) {
    return src
  }
  let and = src.indexOf("&")
  let what = src.indexOf("?")
  if (and !== -1) {
    src = src.substring(0, and)
  }
  if (what !== -1) {
    src = src.substring(0, what)
  }
  return <ClickToLoad options={options} what={"Soundcloud: " + src}>
    <iframe 
      width="100%" 
      height="300" 
      scrolling="no" 
      frameborder="no" 
      allow="autoplay" 
      src={"https://w.soundcloud.com/player/?url=" + src + "&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"}
    />
  </ClickToLoad>
}
tags.twitch = (obj, options)=>{
  let src = obj.attrs.href || obj.content[0] || ""
  src = src.replace("https://m.twitch.tv/", "https://twitch.tv/")
  let mode
  if (src.startsWith("https://twitch.tv/collections/")) {
    mode = "collection"
    src = src.substring("https://twitch.tv/collections/".length)
  } else if (src.startsWith("https://www.twitch.tv/collections/")) {
    mode = "collection"
    src = src.substring("https://www.twitch.tv/collections/".length)
  } else if (src.startsWith("https://clips.twitch.tv/")) {
    mode = "clip"
    src = src.substring("https://clips.twitch.tv/".length) 
  } else if (src.startsWith("https://twitch.tv/videos/")) {
    mode = "video"
    src = src.substring("https://twitch.tv/videos/".length) 
  } else if (src.startsWith("https://www.twitch.tv/videos/")) {
    mode = "video"
    src = src.substring("https://www.twitch.tv/videos/".length)
  } else if (src.includes("/clip/")) {
    mode = "clip"
    src = src.substring(src.indexOf("/clip/") + "/clip/".length)
  } else if (src.startsWith("https://twitch.tv/")) {
    mode = "channel"
    src = src.substring("https://twitch.tv/".length)
  } else if ("https://www.twitch.tv/") {
    mode = "channel"
    src = src.substring("https://www.twitch.tv/".length)
  } else {
    console.log("twitch?", src)
    return src
  }
  let and = src.indexOf("&")
  let what = src.indexOf("?")
  if (and !== -1) {
    src = src.substring(0, and)
  }
  if (what !== -1) {
    src = src.substring(0, what)
  }
  let url = "https://player.twitch.tv/?" + mode + "=" + src + "&parent=kopunch.club&parent=localhost&autoplay=false"
  if (mode === "clip") {
    url = "https://clips.twitch.tv/embed?clip=" + src + "&parent=kopunch.club&parent=localhost"
  }
  console.log("TWITCH", mode, src, url)
  return <ClickToLoad options={options} what={"Twitch: " + (obj.attrs.href || obj.content[0])}>
    <div className="twitch">
      <iframe
        className="twitch"
        src={url}
        height="720"
        width="1280"
        frameBorder="no"
        scrolling="no"
        allowFullScreen
      />
    </div>
  </ClickToLoad>
}

function find_first_of(text, items, start) {
  let min = -1
  for (const item of items) {
    let i = text.indexOf(item, start)
    if (min === -1 || (i !== -1 && i < min)) {
      min = i
    }
  }
  return min
}

function log(...args) {
  //console.log(...args)
}

function parse_attributes(text) {
  log("parsing attributes:", text)
  let tokens = []

  let start = 0
  let inquote = false
  for (let cursor = 0; cursor < text.length; ++cursor) {
    if (text[cursor] === ' ' && !inquote) {
      if (cursor === start) {
        ++start
      } else {
        tokens.push(text.substring(start, cursor))
        start = cursor + 1
      }
    } else if (text[cursor] === '"') {
      if (!inquote) {
        inquote = true
      } else {
        inquote = false
        tokens.push(text.substring(start, cursor + 1))
        start = cursor + 1
      }
    }
  }
  if (start < text.length) {
    tokens.push(text.substring(start))
  }

  let attributes = {}
  for (const token of tokens) {
    let eq = token.indexOf('=')
    if (eq === -1) {
      attributes[token] = true
    } else {
      if (token[eq + 1] === '"') {
        attributes[token.substring(0, eq)] = token.substring(eq + 2, token.length - 1)
      } else {
        attributes[token.substring(0, eq)] = token.substring(eq + 1)
      }
    }
  }
  log("attributes", attributes)
  return attributes
}

function parse_bbcode(text, tags, depth) {
  if (depth === undefined) {
    depth = 1
  }
  log("|___|".repeat(depth))
  log("parsing bbcode:", text)
  let intag
  let start = 0
  let children = []
  let noparse_attrs
  for (let cursor = 0; cursor < text.length; ++cursor) {
    if (text[cursor] === '[' && text[cursor + 1] !== '/') {
      let tagstart = cursor + 1
      let tagend = find_first_of(text, [' ', ']'], tagstart)
      let tag = text.substring(tagstart, tagend).toLowerCase()
      log("new tag", tag)

      if (tags[tag]) {
        intag = tag

        if (start < cursor) {
          children.push(text.substring(start, cursor))
        }

        let tagclose = tagend
        let attrs = {}
        if (text[tagend] === ' ') {
          tagclose = text.indexOf(']', tagstart)
          console.assert(tagclose !== -1, "tagclose")
          if (tagclose === -1) {
            if (start < text.length) {
              children.push(text.substring(start))
            }
            return {
              end: text.length,
              content: children,
              inner: text,
            }
          }

          attrs = parse_attributes(text.substring(tagend + 1, tagclose))
        } else {
          console.assert(text[tagend] === ']', "text[tagend]")
        }

        if (noparse_tags[tag]) {
          noparse_attrs = attrs
          let noparsedepth = 1
          cursor = tagend
          while (noparsedepth > 0) {
            let nextstart = text.indexOf('[' + tag, cursor)
            let nextend = text.indexOf('[/' + tag, cursor)
            console.log("find noparses", noparsedepth, nextstart, nextend)
            if (nextstart === -1 && nextend === -1) break;
            if (nextstart === -1) {
              cursor = Math.max(cursor, nextend + 1)
              --noparsedepth
            }
            if (nextend == -1) {
              cursor = Math.max(cursor, nextstart + 1)
              ++noparsedepth
            }
            if (nextstart < nextend) {
              cursor = Math.max(cursor, nextstart + 1)
              ++noparsedepth
            }
            if (nextend < nextstart) {
              cursor = Math.max(cursor, nextend + 1)
              --noparsedepth
            }
          }
          console.log("done with noparses!")

          start = text.indexOf(']', tagstart) + 1
          cursor -= 2
        } else {
          // There's not really a good way to tell where the ending tag is at this point,
          // so rely on recursive calls to tell us where our end tag is

          log("|>>>|".repeat(depth))
          let ret = parse_bbcode(text.substring(tagclose + 1), tags, depth + 1)
          log("|<<<|".repeat(depth))
          ret.tag = tag
          ret.attrs = attrs

          let endtag = ret.end + tagclose + 1
          log("ret.end:", ret.end, "tagclose", tagclose, "text.length", text.length)
          let endtagend = text.indexOf(']', endtag)
          console.assert(endtagend !== -1, "endtagend")

          if (endtagend === -1) {
            endtagend = endtag
          }
          log("endtag:", text.substring(endtag))

          ret.outer = text.substring(cursor, endtagend + 1)
          ret.tag_open = text.substring(cursor, tagclose + 1)
          ret.tag_close = text.substring(endtag, endtagend + 1)

          cursor = endtagend
          start = cursor + 1

          log("new cursor:", text.substring(cursor))

          children.push(ret)
        }
      }
    } else if (text[cursor] === '[' && text[cursor + 1] === '/') {
      let endend = text.indexOf(']', cursor + 1)
      console.assert(endend !== -1, "endend")
      let tag = text.substring(cursor + 2, endend).toLowerCase()
      log("found end", tag)
      if (noparse_tags[tag]) {
        let content = text.substring(start, cursor)
        start = endend + 1
        cursor = endend
        children.push({
          end: cursor,
          content: [content],
          inner: content,
          tag: tag,
          attrs: noparse_attrs || {},
        })
      } else if (tags[tag]) {
        // abort! not our tag!
        log("aborting!")
        if (depth === 1) {
          log("tried to abort on depth 1! breaking instead.")
          break
        }
        if (start < cursor) {
          children.push(text.substring(start, cursor))
        }
        return {
          end: cursor,
          content: children,
          inner: text.substring(0, cursor)
        }
      }
    }
  }
  log("end of function, leftover:", text.substring(start))
  if (start < text.length) {
    children.push(text.substring(start))
  }
  return {
    end: text.length,
    content: children,
    inner: text,
  }
}

function render_bbcode(tree, options, depth) {
  if (depth === undefined) {
    depth = 1
  }
  if (tree.content) {
    if (tree.tag && tags[tree.tag] && block_tags[tree.tag] && tree.content.length > 0 && !tree.content[0].tag && tree.content[0][0] === "\n") {
      log("tag", tree.tag, "was block, opened with a newline")
      if (tree.content[0].length === 1) {
        tree.content.splice(0, 1)
        log("removed line")
      } else {
        tree.content[0] = tree.content[0].substring(1)
        log("removed newline")
      }
    }
    for (let i = 0; i < tree.content.length; ++i) {
      if (tree.content[i].tag && block_tags[tree.content[i].tag]) {
        log("tag", tree.content[i].tag, "was block, followed by [" + tree.content[i + 1] + "]")
        if (tree.content[i + 1] && !tree.content[i + 1].tag && tree.content[i + 1][0] === "\n") {
          if (tree.content[i + 1].length === 1) {
            tree.content.splice(i + 1, 1)
            log("removed line")
          } else {
            tree.content[i + 1] = tree.content[i + 1].substring(1)
            log("removed newline")
          }
        }
      } else if (typeof tree.content[i] === 'string') {
        let str = tree.content[i]
        let at
        let numstart
        for (let j = 0; j < str.length; ++j) {
          if (str[j] === '@') {
            at = j
          }
          if (at === j - 1 && str[j] === '<') {
            numstart = j + 1
          }
          if (str[j] === ' ') {
            at = undefined
            numstart = undefined
          }
          if (at !== undefined && numstart !== undefined && str[j] === '>') {
            let before = str.substring(0, at)
            let after = str.substring(j + 1)
            let num = str.substring(numstart, j)
            tree.content.splice(i, 1, before, <Username at id={num}/>, after)
          }
        }
        if (depth === 1) {
          try {
            for (const get of Urls(str).get()) {
              if (get.indexOf("[/") !== -1) {
                get = get.substr(0, get.indexOf("[/"))
              }
              let start = str.indexOf(get)
              let before = str.substr(0, start)
              let after = str.substr(start + get.length)
              console.log(get, start, before, after)
              let url = tags.url({
                attrs: {
                  href: get.includes("://") ? get : `//${get}`
                },
                content: get
              }, options)
              tree.content.splice(i, 1, before, url, after)
              str = after
              i += 2
            }
          } catch (e) {

          }
        }
      }
      tree.content[i] = render_bbcode(tree.content[i], options, depth + 1)
    }
  }
  if (tree.tag && tags[tree.tag]) {
    tree = tags[tree.tag](tree, options)
  } else if (tree.content) {
    tree = tree.content
  }
  return tree
}

export function parse_and_render_bbcode(content) {
  return render_bbcode(parse_bbcode(this.props.data.content, tags), {})
}

let socialIcons = {
  steam: (data)=><LinkWrapper title="Steam" href={data.url}><ImgWrapper src="/static/social/steam.png"/></LinkWrapper>,
  website: (data)=>data == "null" ? null : <LinkWrapper external title="Website" href={data}><ImgWrapper src="/static/social/homepage.png"/></LinkWrapper>,
  twitter: (data)=><LinkWrapper title="Twitter" href={"https://twitter.com/" + data}><ImgWrapper src="/static/social/twitter.png"/></LinkWrapper>,
  github: (data)=><LinkWrapper title="GitHub" href={"https://github.com/" + data}><ImgWrapper src="/static/social/github.png"/></LinkWrapper>,
  gitlab: (data)=><LinkWrapper title="GitLab" href={"https://gitlab.com/" + data}><ImgWrapper src="/static/social/gitlab.png"/></LinkWrapper>,
  tumblr: (data)=><LinkWrapper title="Tumblr" href={`https://${data}.tumblr.com`}><ImgWrapper src="/static/social/tumblr.png"/></LinkWrapper>,
  twitch: (data)=><LinkWrapper title="Twitch" href={"https://twitch.tv/" + data}><ImgWrapper src="/static/social/twitch.png"/></LinkWrapper>,
  discord: (data)=><ImgWrapper title={data} src="/static/social/discord.png"/>,
  default: (data)=><ImgWrapper title={data} src="/static/silkicons/help.png"/>,
}


export function SocialIcons({id, api}) {
  let [profile, setProfile] = React.useState()
  React.useEffect(()=>{
    getUserProfile(api, id, setProfile)
  }, [id])

  return <>
    <div className="social-icons">
      {profile && profile.social && Object.keys(profile.social).map(key=>{
        return socialIcons[key] ? socialIcons[key](profile.social[key]) : socialIcons.default(profile.social[key])
      })}
    </div>
  </>
}

var rateMessages = ["Rated!", "your paypal has been successfully charged", "thanks dickhead", "farmatyr says hi", "yum, pixels", "thanks babe", "rrerr", "don't post about this"]
rateMessages[-1] = "only losers self-rate"
export default class PostEntry extends Component {
  constructor(props) {
    super(props)
    let author = props.data.author ?? props.data.user
    this.state = {
      value: this.props.value || render_bbcode(parse_bbcode(this.props.data.content, tags), {
        convertlinks: this.props.appState.convertlinks,
        datasaver: this.props.appState.datasaver,
      }),
      ratings: this.props.data.ratings,
      ratingsList: false,
      rateMessage: false,
      reportMenu: false,
      reportText: "",
      reportMessage: undefined,
      showEvents: false,
      bansLoaded: false,
      avatarToggled: false,
      collapsed: this.props.appState.ignorelist[author.id],
      hasError: undefined,
    }

    this.doRating = this.doRating.bind(this)

    
    //parse_bbcode('[hi]hello[/hi] [url href=google.com thumbnail blah="what the fuck"]world[/url] [b]how [i]are[/i] you[/b]?', tags)
    //              |_______________________________________________________________________________________________________|
    //                  |___|                                                          |___|          |________________|
    //                                                                                                       |_|   
  }
  static getDerivedStateFromError(err) {
    return {hasError: err}
  }
  doRating(post, rating) {
    let author = this.props.data.author ?? this.props.data.user
    let own = this.props.appState.userid == author.id
    console.log(own, this.props.appState.userid, author.id)
    if (own) {
      console.log("loser")
      this.setState({rateMessage: -1})
    } else if (this.props.appState.authenticated && !this.props.appState.userbanned) {
      console.log("rated post", post, "with", rating)
      let rare = Math.random() < 0.4
      this.setState({rateMessage: rare && Math.floor(Math.random() * rateMessages.length) || 0})
      window.fetch(this.props.appState.api + "/v2/posts/" + post + "/ratings", {
        method: 'PUT', 
        credentials: 'include',
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          "content-format-version": "1",
        },
        body: JSON.stringify({
          rating: rating,
        }),
      }).then((result)=>{
        if (result.ok) {
          window.fetch(this.props.appState.api + "/v2/posts/" + post, {
            method: 'GET',
            credentials: 'include',
          }).then(r=>r.json()).then((r)=>{
            this.setState({ratings: r.ratings})
          })
        }
      })
    }
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    let author = this.props.data.author ?? this.props.data.user
    if (prevstate.showEvents != this.state.showEvents && this.state.showEvents && !this.state.bansLoaded) {
      window.fetch(this.props.appState.api + "/user/" + author.id + "/bans", {credentials: 'include'}).then(res => res.json()).then((result)=>{
        console.log(result)
        this.bans = result
        this.setState({bansLoaded: true})
      })
    }
    if (prevprops.appState.ignorelist != this.props.appState.ignorelist) {
      this.setState({collapsed: this.props.appState.ignorelist[author.id]})
    }
    if (prevprops.data.content != this.props.data.content) {
      this.setState({value: this.props.value || render_bbcode(parse_bbcode(this.props.data.content, tags), {
        convertlinks: this.props.appState.convertlinks,
        datasaver: this.props.appState.datasaver,
      })})
    }
  }
  render() {
    //console.log("AST:", parse_bbcode(this.props.data.content, tags))
    //console.log("rendered:", render_bbcode(parse_bbcode(this.props.data.content, tags)))

    let unread = false
    if (typeof this.props.threadRead === "boolean") {
      unread = !this.props.threadRead
    } else if (this.props.threadRead) {
      let createdAt = new Date(this.props.data.createdAt)
      let lastRead = new Date(this.props.threadRead)
      if (lastRead < createdAt)
        unread = true
    } else if (this.props.appState.authenticated) {
      unread = true
    }

    let author = this.props.data.author ?? this.props.data.user
    let group = getUserTitleOrGroupName(author)

    let own = this.props.appState.userid == author.id
    let lastTime
    let date = new Date(author.createdAt)
    let createdate = new Date(this.props.data.createdAt)
    let editdate = new Date(this.props.data.updatedAt)
    let appName = this.props.data.appName
    let postNumber = this.props.data.threadPostNumber || this.props.number
    let posts = Number(author && author.posts || 0).toLocaleString()
    let threads = Number(author && author.threads || 0).toLocaleString()
    let usernamebit = <><Username shadow={this.props.appState.layout == "newpunch"} user={author}/> {author.pronouns ? <span className="user-pronouns small">({author.pronouns})</span> : ''}</>
    if (appName === null && postNumber !== 1 && createdate > new Date("19 February 2020") && createdate < new Date("20 March 2021")) {
      appName = "knocky"
    }
    let permalink = "/showthread.php?t=" + this.props.thread + "&page=" + this.props.page + "&p=" + this.props.data.id
    let noAvatar = ("" + author.backgroundUrl).length === 0 && author.avatarUrl === "none.webp"
    if (author.isBanned) {
      noAvatar = true
    }
    if (this.props.appState.noavatar) {
      noAvatar = true
    }
    return <li className="post-li" id={"post-" + this.props.data.id}>
      <div className="post-top small">
        <img className="icon" src="/static/silkicons/time.png" title={new Date(this.props.data.createdAt).toString()}/>
        <span className="header-text">
          &nbsp;<TimeElapsed date={this.props.data.createdAt}/>
          {editdate - createdate >= 300000 && <span>
            ;&nbsp;Last edited <TimeElapsed date={this.props.data.updatedAt}/>
          </span>}
        </span>
        <span className="post-number">
          <LinkWrapper href={this.props.permalink || permalink} className="header-text">
            <img className="icon" src="/static/silkicons/link.png"/>
            &nbsp;Post #{postNumber}
          </LinkWrapper>
        </span>
      </div>
      <div className={["post-bottom", (unread ? "new" : ""), (own ? "own" : ""), this.props.appState.layout, (this.state.collapsed ? "collapsed" : ""), "user-group-" + author.usergroup, "user-role-" + author.role.code, (noAvatar ? "no-avatar" : "")].join(" ")}>
        <div className="user-pane">
          {this.props.appState.layout != "newpunch" && usernamebit}
          {this.props.appState.layout != "newpunch" && <div className="user-title small">{group}</div>}
          <div className="user-info">
            {!noAvatar && <div className="user-images">
              {!author.isBanned && !this.state.collapsed &&
                <picture className="user-avatar" alt={author.username + "'s avatar"} title={author.username + "'s avatar"} onClick={()=>{this.setState({avatarToggled: !this.state.avatarToggled})}} style={{opacity: this.state.avatarToggled ? 0 : 1}}>
                  <source srcSet={`https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/${(author.avatar_url || author.avatarUrl)}?${author.updatedAt}`} type="image/webp"/>
                  <img srcSet="/static/defaultavatar.png"/>
                </picture>
              }
              {author.isBanned && 
                <div className="user-avatar banned">
                  BAN
                </div>
              }
              {!this.state.collapsed &&
                <div className="background-container">
                  <div className="background-gradient"></div>
                  <img className="background-image" src={`https://knockout-production-assets.nyc3.digitaloceanspaces.com/image/${author.backgroundUrl || 'none.webp'}?${author.updatedAt}`}/>
                </div>
              }
            </div>}
            {this.props.appState.layout != "newpunch" && author.isBanned && 
              <div className="user-avatar banned">
                BAN
              </div>
            }
            {this.props.appState.layout == "newpunch" && usernamebit}
            {this.props.appState.layout == "newpunch" && <div className="user-title small">{group}</div>}
            <span title={"Joined " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear()} className="user-joined">
              {months[date.getMonth()]} {date.getFullYear()}
              {date.getDate() === (new Date()).getDate() && date.getMonth() === (new Date()).getMonth() && <img className="birthday" src="/static/emotes/oldpunch/toot.gif"/>}
            </span>
            
            <span title={threads + " Threads"} className="user-posts">{posts} Posts</span>
            {!this.props.appState.nosocial && <SocialIcons id={author.id} api={this.props.appState.api}/>}
          </div>
          <div className="user-pane-bottom">
            {this.props.appState.authenticated && !this.props.noReport && <LinkWrapper href="" title="Report this post" className="report-button" onClick={(e)=>{e.preventDefault(); this.setState({reportMenu: !this.state.reportMenu, reportMessage: undefined})}}>
              <img src="/static/silkicons/report_edit.png"/>
            </LinkWrapper>}
            <LinkWrapper href="" title="Show Events" className="events-button" onClick={(e)=>{e.preventDefault(); this.setState({showEvents: !this.state.showEvents})}}>
              <img src="/static/silkicons/calendar.png"/>
            </LinkWrapper>
            {this.props.customButtons && this.props.customButtons.map(({src, ...rest}, k)=>{
              return <LinkWrapper key={k} href="" {...rest}>
                <img src={src}/>
              </LinkWrapper> 
            })}
            {this.props.data.countryCode && <img className="flagdog" title={this.props.data.countryName} src={`/static/flagdog/${this.props.data.countryCode.toLowerCase()}.png`}/>}
            {appIcons[appName] && <img className="appname" title={appName} src={"/static/" + appIcons[appName]} width="16" height="16"/>}
          </div>
        </div>
        <span className="post-body">
          <span className="content" key={"content-" + this.props.data.id} >
            {!this.state.collapsed && !this.state.hasError && this.state.value}
            {!this.state.collapsed && this.state.hasError && this.props.data.content}
            {!this.state.collapsed && this.state.hasError && <div className="error">{"" + this.state.hasError}</div>}
            {this.state.collapsed && <div className="post-hidden">
              <b>This message is hidden because {author.username} is on your ignore list.</b>
              <hr/>
              <LinkWrapper href="#" onClick={(e)=>{e.preventDefault(); this.setState({collapsed: false})}}>View Post</LinkWrapper>
              <div className="small"><LinkWrapper href="#" onClick={(e)=>{
                e.preventDefault()
                let list = Object.assign({}, this.props.appState.ignorelist)
                list[author.id] = false
                this.props.appSetState({ignorelist: list})
              }}>Remove user from ignore list</LinkWrapper></div>
            </div>}
            {this.props.data.bans && this.props.data.bans.map((ban)=>{
              return <div key={ban.banReason} className="ban-text">(User was banned for this post ("{ban.banReason}" - {ban.bannedBy.username}))</div>
            })}
          </span>
          <div className="ratings">
            {this.state.ratings && this.state.ratings.map((o)=>{
              return <Rating rating={o.rating} count={o.count} showName={this.state.ratings.length < 5} showCount doRating={this.doRating} postId={this.props.data.id}/>
            })}
            {this.state.ratings && this.state.ratings.length > 0 && this.state.ratings[0].users && <LinkWrapper href="" className="ratings-list-button" onClick={(e)=>{e.preventDefault(); this.setState({ratingsList: !this.state.ratingsList})}}>(list)</LinkWrapper>}
            {!this.props.appState.userbanned && !this.props.noButtons && this.props.appState.authenticated && !this.props.locked && <LinkWrapper href={"/newreply.php?t=" + this.props.thread + "&page=" + this.props.page + "&p=" + this.props.data.id} onClick={this.props.onReplyButtonClicked} className="reply-button link-button">Reply</LinkWrapper>}
            {!this.props.appState.userbanned && !this.props.noButtons && this.props.appState.authenticated && !this.props.locked && own && <LinkWrapper href={"/newreply.php?t=" + this.props.thread + "&page=" + this.props.page + "&edit=" + this.props.data.id} className="edit-button link-button">Edit</LinkWrapper>}
            <span className="rating-buttons">
              {this.state.rateMessage === false && !this.props.appState.userbanned && this.props.appState.authenticated && !this.props.noRatings && Object.keys(ratings).filter(r => ratings[r].subforum & (1 << this.props.subforum)).map((rating)=>{
                return <Rating rating={rating} doRating={this.doRating} postId={this.props.data.id}/>
              })}
              {this.state.rateMessage !== false && <span className="rate-message small" onClick={()=>{this.setState({rateMessage: false})}}>{rateMessages[this.state.rateMessage]}</span>}
            </span>
            {this.state.ratingsList && <div className="popover panel ratings-list">
              {this.state.ratings.map((o)=>{
                return <div key={o.rating} className="ratings-list-column">
                  <Rating rating={o.rating} count={o.count} showName={this.state.ratings.length < 5} showCount doRating={this.doRating} postId={this.props.data.id}/>
                  {o.users && o.users.map(u=>{
                    if (typeof u === "string") {
                      return <div key={u}>{u}</div>
                    }
                    return <Username key={u} user={u}/>
                  })}
                </div>
              })}
            </div>}
          </div>
        </span>
      </div>
      {this.state.reportMenu && <div className="popover panel report-menu">
        {this.state.reportMessage || null}
        {!this.state.reportMessage && <div>
          Which of the <LinkWrapper href="https://knockout.chat/rules">site's rules</LinkWrapper> did this post break?
          <form onSubmit={(e)=>{
            e.preventDefault()
            window.fetch(this.props.appState.api + "/reports", {
              method: 'POST', 
              credentials: 'include',
              headers: {
                "Content-Type": "application/json;charset=utf-8",
                "content-format-version": "1",
              },
              body: JSON.stringify({
                postId: this.props.data.id,
                reportReason: this.state.reportText,
              }),
            }).then(r=>r.json()).then((r)=>{
              console.log(r)
              this.setState({reportText: "", reportMessage: r.message})
            })
            this.setState({reportMessage: "Sending report..."})
          }}>
            <input type="text" value={this.state.reportText} onChange={(e)=>{this.setState({reportText: e.target.value})}}/>
            <input type="submit" value="Submit"/>
          </form>
        </div>}
      </div>}
      {this.state.showEvents && <div className="popover panel events-menu">
        {this.state.bansLoaded && this.bans.length &&
          this.bans.map((v, k)=>{
            let changed = lastTime != elapsedString(v.createdAt)
            if (changed)
              lastTime = elapsedString(v.createdAt)
            return <BanEntry key={v.id} data={v} username={author.username} showTime={changed}/>
          }) || "Nothing to show here!"
        }
      </div>}
    </li>
  }
}
