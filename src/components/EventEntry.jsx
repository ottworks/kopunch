import React, { Component } from "react"
import { Link } from 'react-router-dom'
import TimeElapsed, { elapsedString } from './TimeElapsed'
import LinkWrapper from './LinkWrapper'
import Username from "./Username"
import ThreadIcon from "./ThreadIcon"
import Rating from './Rating'

function ThreadLink({id, title, iconId, post}) {
  return <>
    <ThreadIcon id={iconId}/>{" "}
    <LinkWrapper href={`/showthread.php?t=${id}${post ? `&page=${post.page}&p=${post.id}` : ''}`} title={post?.content}>
      {title}
    </LinkWrapper>
  </>
}

function SubforumLink({id, name}) {
  return <LinkWrapper href={`/forumdisplay.php?f=${id}`}>
    {name}
  </LinkWrapper>
}

function LockEvent({creator, data}) {
  return <>
    <img src="/static/silkicons/lock.png" className="icon"/>
    <Username user={creator}/>{" "}
    locked{" "}
    <ThreadLink {...data} post={data.lastPost}/>
  </>
}

function UnlockEvent({creator, data}) {
  return <>
    <img src="/static/silkicons/lock_open.png" className="icon"/>
    <Username user={creator}/>{" "}
    unlocked{" "}
    <ThreadLink {...data} post={data.lastPost}/>
  </>
}

function PinEvent({creator, data}) {
  return <>
    <img src="/static/silkicons/note.png" className="icon"/>
    <Username user={creator}/>{" "}
    stickied{" "}
    <ThreadLink {...data}/>
  </>
}

function UnpinEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/note_delete.png" className="icon" />
    <Username user={creator} />{" "}
    un-stickied{" "}
    <ThreadLink {...data} />
  </>
}

function DeleteEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/bin_closed.png" className="icon" />
    <Username user={creator} />{" "}
    deleted{" "}
    <ThreadLink {...data} post={data.lastPost} />
  </>
}

function RestoreEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/bin_empty.png" className="icon" />
    <Username user={creator} />{" "}
    restored{" "}
    <ThreadLink {...data} post={data.lastPost} />
  </>
}

function MoveEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/arrow_turn_right.png" className="icon" />
    <Username user={creator} />{" "}
    moved{" "}
    <ThreadLink {...data}/>{" "}
    to{" "}
    <SubforumLink {...data.subforum}/>
  </>
}

function BanEvent({creator, data}) {
  return <>
    <img src="/static/silkicons/world_delete.png" className="icon"/>
    <Username user={creator}/>
    {" "}banned{" "}
    <Username user={data.user}/>
    {" "}for "{data.banReason}"{" "}
    {data.thread && <>in <ThreadLink {...data.thread} post={data.post}/></>}
  </>
}

function WipeEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/delete.png" className="icon" />
    <Username user={creator} />
    {" "}wiped the account of{" "}
    <Username user={data.user} />
  </>
}

function GoldEvent({ creator }) {
  return <>
    <img src="/static/silkicons/star.png" className="icon" />
    <Username user={creator} />
    {" "}was golded
  </>
}

function UngoldEvent({ creator }) {
  return <>
    <img src="/static/utried.png" className="icon" />
    <Username user={creator} />
    {" "}was ungolded
  </>
}

function UnbanEvent({creator, data}) {
  return <>
    <img src="/static/silkicons/heart.png" className="icon"/>
    <Username user={creator}/>
    {" "}unbanned{" "}
    <Username user={data}/>
  </>
}

function RenameEvent({creator, content, data}) {
  return <>
    <img src="/static/silkicons/pencil.png" className="icon" />
    <Username user={creator} />
    {" "}renamed{" "}
    <ThreadLink {...data} title={content.oldTitle} />
    {" "}to{" "}
    <ThreadLink {...data} />
  </>
}

function PostLimitEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/lorry.png" className="icon" />
    <ThreadLink {...data} />
    {" "}has reached the maximum post count{" "}
  </>
}

function BgUpdateEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/picture.png" className="icon" />
    <Username user={creator} />
    {" "}updated the background of{" "}
    <ThreadLink {...data} />
    {" "}to{" "}
    <LinkWrapper href={data.backgroundUrl}>this image</LinkWrapper>
  </>
}

function PostCreatedEvent({ creator, data }) {
  return <div className="d-flex-justify">
    <div>
      <div style={{ width: '140px', display: 'inline-block' }}>
        <Username user={creator} />
      </div>
      <ThreadLink post={data} {...data.thread} />
    </div>
    <div>
      <b><LinkWrapper href={`/forumdisplay.php?f=${data.thread.subforum.id}`}>{data.thread.subforum.name}</LinkWrapper></b>
    </div>
  </div>
}

function CommentCreatedEvent({ creator, data }) {
  return <div className="d-flex-justify">
    <div>
      <Username user={creator} />
    </div>
    <div>
      <b><Username user={data} /></b>
    </div>
  </div>
}

function ThreadCreatedEvent({ creator, data }) {
  return <div className="d-flex-justify">
    <div>
      <div style={{ width: '140px', display: 'inline-block' }}>
        <Username user={creator} />
      </div>
      <b><ThreadLink {...data} /></b>
    </div>
    <div>
      <b><LinkWrapper href={`/forumdisplay.php?f=${data.subforum.id}`}>{data.subforum.name}</LinkWrapper></b>
    </div>
  </div>
}

function RatingCreatedEvent({ creator, data, content }) {
  return <div className="d-flex-justify">
    <div>
      <div style={{ width: '140px', display: 'inline-block' }}>
        <Rating rating={content.rating} doRating={console.log} /> <Username user={creator} />
      </div>
      <ThreadLink post={data} {...data.thread} />
    </div>
    <div>
      <b><LinkWrapper href={`/forumdisplay.php?f=${data.thread.subforum.id}`}>{data.thread.subforum.name}</LinkWrapper></b>
    </div>
  </div>
}

function AvatarRemoveEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/world_delete.png" className="icon" />
    <Username user={creator} />
    {" "}removed the avatar{" "}
    <Username user={data.user} />
    {" "}due to their bad taste{" "}
  </>
}

function BackgroundRemoveEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/world_delete.png" className="icon" />
    <Username user={creator} />
    {" "}removed the background{" "}
    <Username user={data.user} />
    {" "}due to their bad taste{" "}
  </>
}

function ProfileRemoveEvent({ creator, data }) {
  return <>
    <img src="/static/silkicons/world_delete.png" className="icon" />
    <Username user={creator} />
    {" "}removed the profile{" "}
    <Username user={data.user} />
    {" "}due to their bad taste{" "}
  </>
}

export default function EventEntry(props) {
  let content = props.data.content
  let src = "/static/silkicons/magnifier.png"

  if (content.content) {
    if (content.content[0].includes("unbanned")) {
      src = "/static/silkicons/heart.png"
    } else if (content.content[0].includes("🔨")) {
      src = "/static/silkicons/world_delete.png"
    } else if (content.content[0].includes("📝")) {
      src = "/static/silkicons/pencil.png"
    } else if (content.content[0].includes("unlocked")) {
      src = "/static/silkicons/lock_open.png"
    } else if (content.content[0].includes("🔒")) {
      src = "/static/silkicons/lock.png"
    } else if (content.content[0].includes("restored")) {
      src = "/static/silkicons/bin_empty.png"
    } else if (content.content[0].includes("🗑️")) {
      src = "/static/silkicons/bin.png"
    } else if (content.content[0].includes("↪️")) {
      src = "/static/silkicons/arrow_turn_right.png"
    } else if (content.content[0].includes("📌")) {
      src = "/static/silkicons/note.png"
    } else if (content.content[0].includes("👑")) {
      src = "/static/silkicons/star.png"
    } else if (content.content[0].includes("🍃")) {
      src = "/static/silkicons/lorry.png"
    } else if (content.content[0].includes("🤢")) {
      src = "/static/utried.png"
    }
  }
  
  let Component = props.data.type === "thread-locked" ? <LockEvent {...props.data} /> :
    props.data.type === "thread-unlocked" ? <UnlockEvent {...props.data} /> :
    props.data.type === "thread-deleted" ? <DeleteEvent {...props.data} /> :
    props.data.type === "thread-restored" ? <RestoreEvent {...props.data} /> :
    props.data.type === "thread-moved" ? <MoveEvent {...props.data} /> :
    props.data.type === "thread-pinned" ? <PinEvent {...props.data} /> :
    props.data.type === "thread-unpinned" ? <UnpinEvent {...props.data} /> :
    props.data.type === "thread-renamed" ? <RenameEvent {...props.data} /> :
    props.data.type === "thread-post-limit-reached" ? <PostLimitEvent {...props.data} /> :
    props.data.type === "thread-background-updated" ? <BgUpdateEvent {...props.data} /> :
    props.data.type === "user-banned" ? <BanEvent {...props.data} /> :
    props.data.type === "user-unbanned" ? <UnbanEvent {...props.data} /> :
    props.data.type === "user-wiped" ? <WipeEvent {...props.data} /> :
    props.data.type === "user-avatar-removed" ? <AvatarRemoveEvent {...props.data} />:
    props.data.type === "user-background-removed" ? <BackgroundRemoveEvent {...props.data} /> :
    props.data.type === "user-profile-removed" ? <ProfileRemoveEvent {...props.data} /> :
    props.data.type === "gold-earned" ? <GoldEvent {...props.data} /> :
    props.data.type === "gold-lost" ? <UngoldEvent {...props.data} /> :
    props.data.type === "post-created" ? <PostCreatedEvent {...props.data} /> :
    props.data.type === "thread-created" ? <ThreadCreatedEvent {...props.data} /> :
    props.data.type === "rating-created" ? <RatingCreatedEvent {...props.data} /> :
    props.data.type === "profile-comment-created" ? <CommentCreatedEvent {...props.data} /> : "?"
  
  let Tag = props.td ? "td" : "div"

  if (typeof Component === "string") {
    console.log("unknown event", props.data)
  }

  return <Tag className="event-entry">
    {props.showTime && 
      <p className="event-time small">
        <TimeElapsed date={props.data.createdAt || props.data.created_at}/>
      </p>
    }
    {props.p && <p className="event">
      {Component}
    </p>}
    {!props.p && Component}
  </Tag>
}
