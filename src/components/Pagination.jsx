import React, { Component } from "react"
import LinkWrapper from './LinkWrapper'

export default function Pagination(props) {
  let page = props.page
  let pages = Math.floor((props.totalItems - 1) / props.itemsPerPage) + 1

  if (pages < 2)
    return false

  let query = Object.assign({}, props.query)
  query.page = Number(page) + 1
  delete query.p
  let paramsnext = Object.keys(query).map(key => key + "=" + query[key]).join("&")

  query = Object.assign({}, props.query)
  query.page = pages
  delete query.p
  let paramslast = Object.keys(query).map(key => key + "=" + query[key]).join("&")

  query = Object.assign({}, props.query)
  query.page = page - 1
  delete query.p
  let paramsprev = Object.keys(query).map(key => key + "=" + query[key]).join("&")

  query = Object.assign({}, props.query)
  query.page = 1
  delete query.p
  let paramsfirst = Object.keys(query).map(key => key + "=" + query[key]).join("&")

  let pagelinks = []
  for (let i = Math.max(1, page - 10); i <= Math.min(pages, Number(page) + 10); i++) {
    let query = Object.assign({}, props.query)
    query.page = i
    delete query.p
    let params = Object.keys(query).map(key => key + "=" + query[key]).join("&")
    pagelinks.push(<span key={i} className={page == i ? "selected" : ""}>
      <LinkWrapper href={page == i ? undefined : props.location.pathname + "?" + params} title={"Show results " + ((i - 1) * props.itemsPerPage + 1) + " to " + Math.min(i * props.itemsPerPage, props.totalItems) + " of " + props.totalItems}>{i}</LinkWrapper>
    </span>)
  }
  return <div className="pagination">
    <span><LinkWrapper>Page {page} of {pages}</LinkWrapper></span>
    {page != 1 &&
      <span>
        <LinkWrapper href={props.location.pathname + "?" + paramsfirst} title={"First page - Results 1 to " + Math.min(props.totalItems, props.itemsPerPage) + " of " + props.totalItems}>First «</LinkWrapper>
      </span>
    }
    {page != 1 &&
      <span>
        <LinkWrapper href={props.location.pathname + "?" + paramsprev} title={"Previous page - Results " + ((page - 2) * props.itemsPerPage + 1) + " to " + Math.min((page - 1) * props.itemsPerPage, props.totalItems) + " of " + props.totalItems}>&lt;</LinkWrapper>
      </span>
    }
    {pagelinks}
    {page != pages &&
      <span>
        <LinkWrapper href={props.location.pathname + "?" + paramsnext} title={"Next page - Results " + (page * props.itemsPerPage + 1) + " to " + Math.min((page + 2) * props.itemsPerPage, props.totalItems) + " of " + props.totalItems}>&gt;</LinkWrapper>
      </span>
    }
    {page != pages &&
      <span>
        <LinkWrapper href={props.location.pathname + "?" + paramslast} title={"Last page - Results " + ((pages - 1) * props.itemsPerPage + 1) + " to " + props.totalItems + " of " + props.totalItems}>Last »</LinkWrapper>
      </span>
    }
    
  </div>
}