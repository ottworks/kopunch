import React, { Component } from "react";
import LinkWrapper from './LinkWrapper'

import { io } from "socket.io-client"

export const socketClient = io("https://api.knockout.chat/", {
  withCredentials: true,
  transports: ['websocket'],
})

export default class ViewerCount extends Component {
  constructor(props) {
    super(props)
    this.state = {
      members: props.members || 0,
      guests: props.guests || 0,
    }
  }
  componentDidMount() {
    console.log("WEBSADIBDUWBDUIAWBDAS", socketClient.emit('thread:join', this.props.id))
    socketClient.on('thread:members', v=>{
      console.log("on members", v)
      this.setState({members: v})
    })
    socketClient.on('thread:guests', v=>{
      this.setState({guests: v})
    })
  }
  componentWillUnmount() {
    socketClient.emit('thread:leave', this.props.id)
    socketClient.off('thread:members')
    socketClient.off('thread:guests')
  }
  render() {
    let m = "members"
    let g = "guests"
    if (this.state.members === 1) {
      m = "member"
    }
    if (this.state.guests === 1) {
      g = "guest"
    }
    return <div className="stats">
      <b>{this.state.members.toLocaleString()}</b> {m}, <b>{this.state.guests.toLocaleString()}</b> {g}
    </div>
  }
}