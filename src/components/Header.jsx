import React, { Component } from "react"
import { Helmet } from "react-helmet"
import LinkWrapper from './LinkWrapper'
import TimeElapsed, { elapsedString } from './TimeElapsed'
import { parse_and_render_bbcode } from "./Post"
import { markNotificationsAsRead } from "./api/notifications"

export default class Header extends Component {
  constructor(props) {
    super(props)
    this.loadNotifications = this.loadNotifications.bind(this)
    this.loadMOTD = this.loadMOTD.bind(this)
    this.state = {
      notificationMenu: false,
    }
  }
  loadNotifications() {
    if (!this.props.appState.lastCheckedNotifications || new Date() - this.props.appState.lastCheckedNotifications > 60 * 1000) {
      console.log("FETCHING NOTIFICATIONS")
      this.props.appSetState({lastCheckedNotifications: new Date()})
      window.fetch(this.props.appState.api + "/v2/notifications", {
        credentials: 'include',
        method: "GET",
        mode: "cors"
      }).then(r=>r.json()).then((result)=>{
        console.log("FETCHED NOTIFICATIONS", result)
        if (!result.message) {
          this.props.appSetState({notifications: result})
        }
      })
    }
  }
  loadMOTD() {
    if (!this.props.appState.lastCheckedMOTD || new Date() - this.props.appState.lastCheckedMOTD > 300 * 1000) {
      console.log("FETCHING MOTD")
      this.props.appSetState({lastCheckedMOTD: new Date()})
      window.fetch(this.props.appState.api + "/motd", {
        credentials: 'include',
        method: "GET",
        headers: {
          "content-format-version": "1",
        },
        mode: "cors"
      }).then(r=>r.json()).then((result)=>{
        console.log("FETCHED MOTD", result)
        if (!result.message) {
          let motdlist = []
          if (this.props.appState.motdlist) {
            for (const v of result) {
              if (this.props.appState.motdlist.includes(v.id)) {
                motdlist.push(v.id) // Don't accumulate old motds
              }
            }
          }
          this.props.appSetState({motd: result, motdlist: motdlist})
        }
      })
    }
  }
  messageCallback(message) {
    if (message.data === 'reload') {
      window.location.reload()
    }
  }
  componentDidMount() {
    this.loadNotifications()
    this.loadMOTD()
    window.addEventListener('message', this.messageCallback)
  }
  componentWillUnmount() {
    window.removeEventListener('message', this.messageCallback)
  }
  componentDidUpdate(prevprops) {
    if (prevprops.appState.authenticated !== this.state.authenticated) {
      this.loadNotifications()
    }
  }
  render() {
    let notifications = []//this.props.appState.notifications ? this.props.appState.notifications.map(i=>{return {what: "notification", item: i}}) : []
    
    if (this.props.appState.notifications && this.props.appState.user) {
      let n = 0
      let c = 0
      for (const notification of this.props.appState.notifications) {
        if (!notification.read) {
          notifications.push(notification)
        }
      }
    }
    
    let apr1 = (new Date().getMonth() === 3 && new Date().getDate() === 1)
    let lastTime
    return <div>
      <div className="header" id="top">
        <div className="gradient"/>
        <div className={apr1 ? "logo dont-hassel-the-hoff" : "logo"}>
          <LinkWrapper href={apr1 ? "https://www.youtube.com/watch?v=c8OgmZTtuXk" : "/forum.php"}>
            {apr1 ? <img src="/static/header2.png" title="David Hasselhoff"/> : <img src="/static/header.png" title="Knockout"/>}
          </LinkWrapper>
        </div>
        <div className="breadcrumb">
          <div className="authenticated-box">
            {!notifications.length > 0 && <LinkWrapper href="usercp.php?do=inbox"><img className="notification-icon" src="/static/silkicons/email.png" title="No mentions"/></LinkWrapper>}
            {notifications.length > 0 && <LinkWrapper href="" onClick={(e)=>{e.preventDefault(); this.setState({notificationMenu: !this.state.notificationMenu})}}>
              <img className="notification-icon new" src="/static/silkicons/email_open_image.png" title={notifications.length + " mention" + (notifications.length != 1 ? "s" : "")}/>
            </LinkWrapper>}
            <LinkWrapper href="/usercp.php?do=settings">
              <img className="notification-icon" src="/static/silkicons/wrench_orange.png"/>
            </LinkWrapper>
            {this.props.appState.authenticated && this.props.appState.username && <LinkWrapper href={"/member.php?u=" + this.props.appState.userid} className="header-username">{this.props.appState.username}</LinkWrapper>}
            {this.props.appState.authenticated === false && <LinkWrapper href={`https://knockout.chat/login?redirect=${window.location.origin}/auth`} rel="opener" className="header-username">Log in / Sign up</LinkWrapper>}
            {this.props.appState.authenticated === undefined && <span className="header-username">Authenticating...</span>}
            {notifications && this.state.notificationMenu && <div className="popover panel notification-menu">
              {notifications && notifications.map((n,i)=>{
                if (n.type === "POST_REPLY") {
                  let changed = lastTime != elapsedString(n.createdAt)
                  if (changed) {
                    lastTime = elapsedString(n.createdAt)
                  }
                  let mid = n.mentionId || n.id
                  let pid = n.postId || n.data?.id
                  let tid = n.threadId || n.data?.thread?.id
                  let page = n.threadPage || n.data?.page
                  let content = n.content || n.data?.content
                  console.log("notification", n)
                  return <div className="notification" key={mid}>
                    {changed && <div><TimeElapsed date={n?.createdAt}/></div>}
                    <LinkWrapper href={"/showthread.php?t=" + tid + "&page=" + page + "&p=" + pid}>
                      {n.data?.user?.username} mentioned you in "{n.data?.thread?.title}"
                    </LinkWrapper>
                  </div>
                } else if (n.type === "MESSAGE") {
                  let m = n?.data?.messages && n.data.messages[0]
                  let changed = lastTime != elapsedString(m?.updatedAt)
                  if (changed) {
                    lastTime = elapsedString(m?.updatedAt)
                  }
                  return <div className="notification" key={n.id}>
                    {changed && <div><TimeElapsed date={m?.updatedAt}/></div>}
                    <LinkWrapper href={"/usercp.php?do=inbox&conversation=" + m?.conversationId + "&p=" + m?.id}>
                      {n?.data?.messages.map(m=>m?.user?.username + ": " + m?.content).join(" ")}
                    </LinkWrapper>
                  </div>
                } else if (n.type === "PROFILE_COMMENT") {
                  return <div key={n.id}>
                    <LinkWrapper href={`/member.php?u=${n.data.userProfile}&page=1#post-${n.data.id}`}>
                      Comment from {n.data.author.username}: {n.data.content}
                    </LinkWrapper>
                  </div>
                } else if (n.type === "REPORT_RESOLUTION") {
                  return <div key={n.id}>
                    <LinkWrapper onClick={()=>{
                      markNotificationsAsRead(this.props, [n.id])
                    }}>
                      Action has been taken on one of your reports.
                    </LinkWrapper>
                  </div>
                } else {
                  console.log("Unknown notification", n)
                }
              })}
            </div>}
          </div>
          {this.props.title && 
            <div>
              <LinkWrapper href="/forum.php">Home</LinkWrapper>
              <span className="navbit">
                {' > '}
                <LinkWrapper href="/forum.php">Forum</LinkWrapper>
              </span>
              <span className="navbit">
                {' > '}
                <LinkWrapper href="/forum.php">Knockout</LinkWrapper>
              </span>
              {this.props.subforum && <span className="navbit">
                {' > '}
                <LinkWrapper href={"/forumdisplay.php?f=" + this.props.subforumid}>{this.props.subforum}</LinkWrapper>
              </span>}
              {this.props.thread && <span className="navbit">
                {' > '}
                <LinkWrapper href={"/showthread.php?t=" + this.props.threadid}>{this.props.thread}</LinkWrapper>
              </span>}
            </div>
          }
          <div className="title">
            <span>{this.props.title || "Knockout"}</span>
            <Helmet>
              <title>{this.props.title || "Knockout"}</title>
            </Helmet>
          </div>
        </div>
        <div className="header-links">
          <div className="header-link events">
            <LinkWrapper href="/fp_events.php">
              <img className="header-icon" src="/static/silkicons/calendar.png"/>
              &nbsp;Event Log&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link ticker">
            <LinkWrapper href="/fp_ticker.php">
              <img className="header-icon" src="/static/silkicons/application_view_list.png" />
              &nbsp;Ticker&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link popular">
            <LinkWrapper href="/fp_popular.php">
              <img className="header-icon" src="/static/silkicons/table_lightning.png"/>
              &nbsp;Popular&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link read">
            <LinkWrapper href="/fp_read.php">
              <img className="header-icon" src="/static/silkicons/book_previous.png"/>
              &nbsp;Read&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link calendar">
            <LinkWrapper href="/calendar.php">
              <img className="header-icon" src="/static/silkicons/date.png" />
              &nbsp;Calendar&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link search">
            <LinkWrapper href="/search.php">
              <img className="header-icon" src="/static/silkicons/magnifier.png"/>
              &nbsp;Search&nbsp;
            </LinkWrapper>
          </div>
          <div className="header-link chat">
            <LinkWrapper external href="steam://friends/joinchat/103582791464489035">
              <img className="header-icon" src="/static/silkicons/comments.png"/>
              &nbsp;Chat&nbsp;
            </LinkWrapper>
          </div>
        </div>
      </div>
      {this.props.appState.motd && this.props.appState.motd.map && this.props.appState.motd.map(v=>{
        if (this.props.appState.motdlist && this.props.appState.motdlist.includes(v.id)) {
          return null
        }
        let link = v.buttonLink
        if (link.startsWith("https://knockout.chat/thread")) {
          link = "/thread" + link.substring("https://knockout.chat/thread".length)
        }
        if (!v.message) return null
        return <div className="motd panel" key={v.id}>
          {v.message} <LinkWrapper external href={link} className="link-button">{v.buttonName}</LinkWrapper>
          <LinkWrapper href="" className="xbutton" title="Dismiss" onClick={e=>{
            e.preventDefault()
            let motdlist = this.props.appState.motdlist.slice()
            motdlist.push(v.id)
            this.props.appSetState({motdlist: motdlist})
          }}><img src="/static/xbutton.png"/></LinkWrapper>
        </div>
      })}
      {this.props.appState.userbanned && <div className="ban-box">
        You have been banned for the following reason:<br/>
        <b>{this.props.appState.banmessage}</b>
        <br/><br/>
        Read the rules <LinkWrapper href="https://knockout.chat/rules">here</LinkWrapper>.<br/>
        Appeal for ban <LinkWrapper href="https://discord.gg/ce8pVKH">here</LinkWrapper>.<br/>
        Make sure to provide a reason why you should be unbanned.<br/>
        You only get <b>one</b> chance for an appeal.
      </div>}
    </div>
  }
}
